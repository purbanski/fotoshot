# FotoShot #

iOS and Android application which allows you to take photo of you friends, and destroy it in many funny ways. 
App is available in [iTunes][itunes-fs] and in [Google Play][google-play-fs].

![fotoshot.png](https://bitbucket.org/repo/LxLa4K/images/3570771012-fotoshot.png)

[itunes-fs]: https://itunes.apple.com/pa/app/fotoshot/id845512070?l=en&mt=8
[google-play-fs]: https://play.google.com/store/apps/details?id=com.blackted.fotoshot