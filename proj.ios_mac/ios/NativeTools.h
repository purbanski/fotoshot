//
//  NativeTools.h
//  FotoShot
//
//  Created by Przemek Urbanski on 15/04/15.
//
//

#import <Foundation/Foundation.h>
#import "FotoShotTypes.h"

@interface NativeTools : NSObject

+(UIImage*) scaleImage:(UIImage*)image bubbleCount:(int)bubbleCount;
+(UIImage*) scaleImage:(UIImage*)image width:(int)w height:(int)h;
+(UIImage*) normalizeImage:(UIImage*)image;

+(void) saveImage:(UIImage*)image;

+(Colors) getRGBAsFromImage:(UIImage*)image;

@end
