#import "RootViewController.h"
#import "cocos2d.h"
#import "platform/ios/CCEAGLView-ios.h"
#import "Platform.h"
#import "Brain.h"
#import "NativeTools.h"
//----------------------------------------------------
@interface RootViewController() {
    UIPopoverController *popoverController;
}

@property (nonatomic) UIImagePickerController *imagePickerController;
@property (nonatomic) IBOutlet UIView *overlayView;
@property (nonatomic,strong) UIImage* lastImage;
@property (nonatomic,strong) UIImage* scaledImage;

@end
//----------------------------------------------------
@implementation RootViewController


 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
  
        [super viewDidLoad];
        self.imagePickerController = nil;
        
        _lastImage = [UIImage imageNamed:@"pngs/welcome_shot_big.png"];
        _lastImage = [NativeTools normalizeImage:_lastImage];
        [_lastImage retain];

    }
    return self;
}


// Implement loadView to create a view hierarchy programmatically, without using a nib.
//- (void)loadView {
//}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
  
}


// Override to allow orientations other than the default portrait orientation.
// This method is deprecated on ios6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsLandscape( interfaceOrientation );
}

// For ios6, use supportedInterfaceOrientations & shouldAutorotate instead
- (NSUInteger) supportedInterfaceOrientations{
#ifdef __IPHONE_6_0
    return UIInterfaceOrientationMaskAllButUpsideDown;
#endif
}

- (BOOL) shouldAutorotate {
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];

    auto glview = cocos2d::Director::getInstance()->getOpenGLView();

    if (glview)
    {
        CCEAGLView *eaglview = (CCEAGLView*) glview->getEAGLView();

        if (eaglview)
        {
            CGSize s = CGSizeMake([eaglview getWidth], [eaglview getHeight]);
            cocos2d::Application::getInstance()->applicationScreenSizeChanged((int) s.width, (int) s.height);
        }
    }
}

//fix not hide status on ios7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Camer routines
//----------------------------------------------------
-(void)showImagePicker:(UIImagePickerControllerSourceType)pickerType
{
    @try {
        if ( self.imagePickerController )
            [self.imagePickerController release];
        
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        
        imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
        imagePickerController.sourceType = pickerType;
        imagePickerController.delegate = self;
        self.imagePickerController = imagePickerController;
        
        if (pickerType == UIImagePickerControllerSourceTypeCamera)
        {
            
            //    imagePickerController.showsCameraControls = NO;
            /*
             Load the overlay view from the OverlayView nib file. Self is the File's Owner for the nib file, so the overlayView outlet is set to the main view in the nib. Pass that view to the image picker controller to use as its overlay view, and set self's reference to the view to nil.
             */
            // [[NSBundle mainBundle] loadNibNamed:@"OverlayView" owner:self options:nil];
            
            self.overlayView.frame = imagePickerController.cameraOverlayView.frame;
            imagePickerController.cameraOverlayView = self.overlayView;
            self.overlayView = nil;
            [self.overlayView release];
        }
        
        if ( [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad &&
            pickerType == UIImagePickerControllerSourceTypePhotoLibrary)
        {
            popoverController = [[UIPopoverController alloc] initWithContentViewController:_imagePickerController];
            
            [popoverController presentPopoverFromRect:self.overlayView.frame
                                               inView:self.view
                             permittedArrowDirections:UIPopoverArrowDirectionAny
                                             animated:YES];
            
            popoverController.delegate = self;
            popoverController.popoverContentSize = [self getPoperSize];
            self.preferredContentSize = [self getPoperSize];
        }
        else
        {
            [self presentViewController:self.imagePickerController animated:YES completion:nil];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %s", [[exception name]UTF8String], [[exception reason]UTF8String]);
    }
}
//----------------------------------------------------
-(CGSize) getPoperSize
{
    cocos2d::Size size;
    CGSize poperSize;
    
    size = cocos2d::CCDirector::getInstance()->getWinSize();
    
    poperSize.width = size.width;
    poperSize.height = size.height;
    
    return poperSize;
}

#pragma mark - Image Picker Controller delgate
//-------------------------------------------------------------------------
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    Brain::Get()->ShowProcessing();
    
    if (_lastImage)
        [_lastImage release];
    
    _lastImage = [info valueForKey:UIImagePickerControllerOriginalImage];

    [self dismissViewControllerAnimated:YES completion:nil];
    [popoverController dismissPopoverAnimated:true];

    _lastImage = [NativeTools normalizeImage:_lastImage];
    [_lastImage retain];
    [self onStart];
}
//-------------------------------------------------------------------------
-(void) onStart {
    _scaledImage = [NativeTools scaleImage:_lastImage bubbleCount:Brain::Get()->GetBallCount()];
    
    cocos2d::Size size;
    CGSize s = [_scaledImage size];
    
    size.width = s.width;
    size.height = s.height;
    
    Brain::Get()->LoadColors(size, [NativeTools getRGBAsFromImage:_scaledImage]);
}
//--------------------------------------------------------------------------
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.imagePickerController release];
    self.imagePickerController = nil;
    _imagePickerController = nil;
    
//    DestroyScene::Get()->SetForProcessing();
//    DestroyScene::Get()->Menu_SetFresh( NULL );
}

- (void)dealloc {
    [super dealloc];
}


@end
