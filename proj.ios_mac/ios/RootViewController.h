#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
//#import "GAITrackedViewController.h"
#import <MessageUI/MessageUI.h>


@interface RootViewController :
    UIViewController<UINavigationControllerDelegate,
    UIImagePickerControllerDelegate,
    UIPopoverControllerDelegate> {

}
- (BOOL) prefersStatusBarHidden;

-(void) showImagePicker:(UIImagePickerControllerSourceType) pickerType;
-(void) onStart;

@end
