//
//  NativeTools.m
//  FotoShot
//
//  Created by Przemek Urbanski on 15/04/15.
//
//

#import "NativeTools.h"
#import "Config.h"
#import "cocos2d.h"
#import "FotoShotTypes.h"

@implementation NativeTools
//------------------------------------------------------------------------------
+(UIImage*) scaleImage:(UIImage*)image bubbleCount:(int)bubbleCount {
    CGSize orgSize = [ image size ];
    unsigned int size = orgSize.width * orgSize.height;
    float scale = (float)size / (float)bubbleCount;
    
    int nw = (round(orgSize.width / sqrt(scale)));
    int nh = (round(orgSize.height / sqrt(scale)));
    
    UIImage *ret = [self scaleImage:image width:nw height:nh];
    return ret;
}
//------------------------------------------------------------------------------
+(UIImage*) scaleImage:(UIImage*)image width:(int)w height:(int)h {
    CGRect thumbRect;
    thumbRect.origin.x = 0;
    thumbRect.origin.y = 0;
    thumbRect.size.width = w;
    thumbRect.size.height = h;
    
    CGImageRef          imageRef = [image CGImage];
    CGImageAlphaInfo    alphaInfo = CGImageGetAlphaInfo(imageRef);
    
    // There's a wierdness with kCGImageAlphaNone and CGBitmapContextCreate
    // see Supported Pixel Formats in the Quartz 2D Programming Guide
    // Creating a Bitmap Graphics Context section
    // only RGB 8 bit images with alpha of kCGImageAlphaNoneSkipFirst, kCGImageAlphaNoneSkipLast, kCGImageAlphaPremultipliedFirst,
    // and kCGImageAlphaPremultipliedLast, with a few other oddball image kinds are supported
    // The images on input here are likely to be png or jpeg files
    if (alphaInfo == kCGImageAlphaNone)
        alphaInfo = kCGImageAlphaNoneSkipLast;
    
    // Build a bitmap context that's the size of the thumbRect
    CGContextRef bitmap = CGBitmapContextCreate(
                                                NULL,
                                                thumbRect.size.width,       // width
                                                thumbRect.size.height,      // height
                                                CGImageGetBitsPerComponent(imageRef),   // really needs to always be 8
                                                4 * thumbRect.size.width,   // rowbytes
                                                CGImageGetColorSpace(imageRef),
                                                alphaInfo
                                                );
    
    // Draw into the context, this scales the image
    CGContextDrawImage(bitmap, thumbRect, imageRef);
    
    // Get an image from the context and a UIImage
    CGImageRef  ref = CGBitmapContextCreateImage(bitmap);
    UIImage*    result = [UIImage imageWithCGImage:ref];
    
    CGContextRelease(bitmap);   // ok if NULL
    CGImageRelease(ref);
    
    return result;
}
//------------------------------------------------------------------------------
+(UIImage*) normalizeImage:(UIImage*) image {
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    [image drawInRect:(CGRect){0, 0, image.size}];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
//------------------------------------------------------------------------------
+(void) saveImage:(UIImage*) img {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *filePath;
    
    filePath = [NSString stringWithFormat:@"%@/%s", documentsDirectory, Config::imageDefaultFilename];
    cocos2d::log("Save file: %s", [filePath UTF8String]);
    
    // Save image.
    [UIImagePNGRepresentation(img) writeToFile:filePath atomically:YES];
}
//------------------------------------------------------------------------------
+(Colors) getRGBAsFromImage:(UIImage*)image
{
    // First get the image into your data buffer
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = (unsigned char*) calloc(height * width * 4, sizeof(unsigned char));
    
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    Colors cols;
    
    CGSize size = [image size];
    unsigned int count = size.width * size.height;
    
    //    Colors cols;
    // Now your rawData contains the image data in the RGBA8888 pixel format.
    int byteIndex = 0; //(bytesPerRow * yy) + xx * bytesPerPixel;
    for (int ii = 0 ; ii < count ; ++ii)
    {
        int red   = rawData[byteIndex] ;
        int green = rawData[byteIndex + 1];
        int blue  = rawData[byteIndex + 2];
        int alpha = rawData[byteIndex + 3];
        byteIndex += 4;
        
        cocos2d::Color4B c;
        c=cocos2d::Color4B(red, green, blue, alpha);
        
        cols.push_back(c);
    }
    
    free(rawData);
    return cols;
}
@end
