package com.blackted.fotoshot;


import android.os.Bundle;
import android.widget.ImageView;
import android.view.View;
import android.animation.Animator;
import android.os.Handler;
import android.content.Intent;
import android.util.Log;
import android.media.AudioManager;
import android.view.WindowManager;




import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

//import android.util.Log;
import android.hardware.Camera;
//import android.content.pm.PackageManager;
//
//import android.view.SurfaceView;
//import android.view.SurfaceHolder;
//
import android.widget.FrameLayout;
//import java.io.IOException;

import com.blackted.fotoshot.AppActivity;
import com.blackted.fotoshot.CameraMy;
import com.blackted.fotoshot.CameraPreview;

public class CameraActivity extends Activity {
    
    private android.hardware.Camera mCamera;
    private CameraPreview mPreview;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_preview);
        
        // Create an instance of Camera
        mCamera = CameraMy.getCameraInstance();
        
        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();
    }
    
    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }
    
}



