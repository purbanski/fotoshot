package com.blackted.fotoshot;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxHelper;

import android.provider.MediaStore;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Handler;
import android.hardware.Camera;
import android.view.Surface;
import android.media.AudioManager;

import java.io.IOException;
import java.io.FileOutputStream;

import com.blackted.fotoshot.FsCamera;
//import com.blackted.fotoshot.CameraActivity;
//import com.blackted.fotoshot.camera.CameraPreview;

public class AppActivity extends Cocos2dxActivity {
	static public AppActivity me;
	static final String TAG = "przurb fotoshot";	
    private Billing mBilling;

    private FsCamera mCamera;
    
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        mBilling = new Billing(this);
        mCamera = new FsCamera(this);
		me = this;
	}

	static public void takePhoto() {
        me.mCamera.takePicture();

//        Intent intent = new Intent(me, CameraActivity.class);
//        EditText editText = (EditText) findViewById(R.id.edit_message);
//        String message = editText.getText().toString();
//        intent.putExtra(EXTRA_MESSAGE, message);
//        me.startActivity(intent);
	}

    static public void appStartUp() {
        Log.e(TAG, "appStartUp");
        me.mCamera.reloadBubbleImage();
    }
    
    //--------------------
    // Billing
    public static void buyWeaponPack() {
        me.mBilling.buyWeaponPack();
    }
    
    public static void setBuyTestPurchased() {
        me.mBilling.setBuyTestPurchased();
    }
    
    public static void setBuyTestCanceled() {
        me.mBilling.setBuyTestCanceled();
    }
    
    public static void setBuyTestRefunded() {
        me.mBilling.setBuyTestRefunded();
    }
    
    public static void setBuyTestUnavailable() {
        me.mBilling.setBuyTestUnavailable();
    }
    
    public static void setBuyDefault() {
        me.mBilling.setBuyDefault();
    }
    
    public static void getBuyItemId() {
//        showMessageBox("Buy Type", me.mBilling.getBuyItemId());
    }
    
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        
        if (requestCode == FsCamera.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK)
        {
            mCamera.onResult(data);
            return;
        }

        if ( mBilling.handleActivityResult(requestCode, resultCode, data))
        {
            return;
        }
        
        super.onActivityResult(requestCode, resultCode, data);
    }
    
	@Override
    protected void onPause() {
        super.onPause();
			Log.e(TAG, "pause");
    }   
    
    @Override
    protected void onResume() {
        super.onResume();
			Log.e(TAG, "Resume");
    }   

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "on Start");
        onResumeRefresh();
    }
    
    private void onResumeRefresh() {
        //-----------------
        // Music
        Runnable rMusic = new Runnable() {
            public void run () {
                setVolumeControlStream(AudioManager.STREAM_MUSIC);
            }
        };
        Handler hMusic = new Handler();
        hMusic.postDelayed(rMusic, 1100);

        //-----------------
        // Graph
        Runnable rGraph = new Runnable() {
            public void run () {
                Log.d(TAG, "setContentView postponed");
                setContentView(mFrameLayout);

            }
        };
        
        Handler hGraph = new Handler();
        hGraph.postDelayed(rGraph, 1000);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "on Stop");
    }

    public static native int nativeGetBallCount();

	static public void test() {
	}
}
