package com.blackted.fotoshot;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxHelper;

import android.provider.MediaStore;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Handler;
import android.hardware.Camera;
import android.view.Surface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.content.res.AssetManager;

import java.io.IOException;
import java.io.FileOutputStream;
import java.io.InputStream;

import com.blackted.fotoshot.AppActivity;

public class FsCamera {
    static final String TAG = "przurb FsCamera";
    static final String DEFAULT_IMAGE = "last.png";
    static final int    REQUEST_IMAGE_CAPTURE = 13;

    private Activity mActivity;
    private Bitmap mPhoto;
    
    public FsCamera(Activity activity) {
        mActivity = activity;
        mPhoto = getBitmapFromAsset(mActivity,"pngs/welcome_shot_big.png");
    }
    
    public static Bitmap getBitmapFromAsset(Context context, String filePath) {
        AssetManager assetManager = context.getAssets();
        
        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open(filePath);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            // handle exception
        }
        
        return bitmap;
    }

    
    public void takePicture() {
        
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            mActivity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
    
    public void onResult(Intent data) {
        Bundle extras = data.getExtras();
        mPhoto = (Bitmap) extras.get("data");

        reloadBubbleImage();
    }
    
    public void reloadBubbleImage()
    {
        if (mPhoto==null)
        {
            Log.d(TAG, "null photo");
            return;
        }
        
        Log.d(TAG, "Reloading bubble image");
        int size;
        float scale;
        int ballCount = AppActivity.nativeGetBallCount();
        int w = mPhoto.getWidth();
        int h = mPhoto.getHeight();
        
        size = w * h;
        scale = size / (float)ballCount;
        
        int width = (int)((double) w / Math.sqrt(scale));
        int height = (int)((double)h / Math.sqrt(scale));
        
        Log.d(TAG,"widht: "+width+" height:"+height);

        Bitmap scaleImg = Bitmap.createScaledBitmap(mPhoto, width, height, true);
        nativeDefaultColorString(width, height, getBitmapRGBA(scaleImg));
        
//        if (saveBitmap(scaleImg)) {
//            nativeDefaultPhotoSaved();
//        }
    }

    public int[] getBitmapRGBA(Bitmap bitmap) {
        int picw = bitmap.getWidth();
        int pich = bitmap.getHeight();
        Log.d(TAG,"Size:"+ picw +"/"+pich);

        int[] pix = new int[picw * pich];

        bitmap.getPixels(pix, 0, picw, 0, 0, picw, pich);
        return pix;
    }

    private boolean saveBitmap(Bitmap bitmap) {
        String filename = Cocos2dxHelper.getCocos2dxWritablePath()+"/"+DEFAULT_IMAGE;
        Log.d(TAG,"Save to file " + filename);
        
        FileOutputStream out = null;
        
        try {
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        }
        catch (Exception e) {
            Log.d(TAG,"Saving png problem"+e.getMessage());
            e.printStackTrace();
            return false;
        }
        finally {
            Log.d(TAG,"PNG Saved");
            try {
                if (out != null) {
                    out.close();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }
    
    public static native void nativeDefaultPhotoSaved();
    public static native void nativeDefaultColorString(final int w, final int h, final int[] colors);

}
