//package com.blackted.piggybiggy;
//
//import org.cocos2dx.lib.Cocos2dxActivity;
//import org.cocos2dx.lib.Cocos2dxEditText;
//import org.cocos2dx.lib.Cocos2dxGLSurfaceView;
////import org.cocos2dx.lib.Cocos2dxMusic;
//
//import android.os.Bundle;
//import android.widget.ImageView;
//import android.view.View;
//import android.animation.Animator;
//import android.os.Handler;
//import android.content.Intent;
//import android.util.Log;
//import android.media.AudioManager;
//import android.view.WindowManager;
//
//public class PiggyBiggy extends Cocos2dxActivity{
//	private Cocos2dxGLSurfaceView mGLView;
//    private Billing mBilling;
//    private GoogleAnal mTracker;
//    
//    private static PiggyBiggy me = null;
//    static final String TAG = "przurb PiggyBiggyActivity";
//    static boolean sReloadContextEnable = true;
//
//    protected void onCreate(Bundle savedInstanceState){
//		super.onCreate(savedInstanceState);
//        me = this;
//        mTracker = new GoogleAnal(this);
//        
//        setContentView(R.layout.piggybiggy);
//        mBilling = new Billing(me);
//        
//		String packageName = getApplication().getPackageName();
//		super.setPackageName(packageName);
//
//        mGLView = (Cocos2dxGLSurfaceView) findViewById(R.id.piggybiggy_gl_surfaceview);
//        mGLView.setTextField((Cocos2dxEditText)findViewById(R.id.textField));
//		mGLView.setAlpha(0);
//        
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//    }
//    
//	 @Override
//	 protected void onPause() {
//         super.onPause();
//         Log.d(TAG, "on Pause.");
//	     mGLView.onPause();
//	 }
//
//	 @Override
//	 protected void onResume() {
//         super.onResume();
//         Log.d(TAG, "on Resume");
//         mGLView.onResume();
//     }
//    
//    @Override
//    protected void onStart() {
//        super.onStart();
//        Log.d(TAG, "on Start");
//        onResumeRefresh();
//        mTracker.onStart();
//        resumeBackgroundMusic();
//    }
//    
//    @Override
//    protected void onStop() {
//        super.onStop();
//        Log.d(TAG, "on Stop");
//        mTracker.onStop();
//        pauseBackgroundMusic();
//    }
//    
//    private void onResumeRefresh() {
//        //-----------------
//        // Music
//        Runnable rMusic = new Runnable() {
//            public void run () {
//                setVolumeControlStream(AudioManager.STREAM_MUSIC);
//            }
//        };
//        Handler hMusic = new Handler();
//        hMusic.postDelayed(rMusic, 4*1000);
//        
////        //-----------------
////        // Graph
////        if (sReloadContextEnable) {
//            Runnable rGraph = new Runnable() {
//                public void run () {
//                    Log.d(TAG, "setContentView postponed");
//                    setContentView(R.layout.piggybiggy);
//                }
//            };
//            
//            Handler hGraph = new Handler();
//            hGraph.postDelayed(rGraph, 1000);
////        }
//    }
//    
//    public static void resumeContextDisable() {
//        Log.d(TAG, "resume Context Disable");
//        sReloadContextEnable = false;
//    }
//    
//    public static void resumeContextEnable() {
//        Log.d(TAG, "resume Context Enable");
//        sReloadContextEnable = true;
//    }
//    
//    public static void buyLevelPack() {
//        me.mBilling.buyLevelPack();
//    }
//
//    public static void setBuyTestPurchased() {
//        me.mBilling.setBuyTestPurchased();
//    }
//    
//    public static void setBuyTestCanceled() {
//        me.mBilling.setBuyTestCanceled();
//    }
//    
//    public static void setBuyTestRefunded() {
//        me.mBilling.setBuyTestRefunded();
//    }
//    
//    public static void setBuyTestUnavailable() {
//        me.mBilling.setBuyTestUnavailable();
//    }
//    
//    public static void setBuyDefault() {
//        me.mBilling.setBuyDefault();
//    }
//    
//    public static void getBuyItemId() {
//        showMessageBox("Buy Type", me.mBilling.getBuyItemId());
//    }
//
//     static {
//    	 System.loadLibrary("cocosdenshion");
//    	 System.loadLibrary("box2d");
//         System.loadLibrary("tests");
//     }
//    
//    
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        if ( !mBilling.handleActivityResult(requestCode, resultCode, data)) {
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//        else
//        {
//            System.out.println("Handle activity result");
//        }
//    }
//}
//
//class DialogMessage {
//    public String title;
//    public String message;
//    
//    public DialogMessage(String title, String message){
//        this.message = message;
//        this.title = title;
//    }
//}