package com.blackted.fotoshot;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.content.Context;

import com.blackted.fotoshot.util.IabHelper;
import com.blackted.fotoshot.util.IabResult;
import com.blackted.fotoshot.util.Inventory;
import com.blackted.fotoshot.util.Purchase;

public class Billing extends Activity {
    //---- In sync with BuyScene::BillingResponeType
    static final int BillingRespone_Error = -1;
    static final int BillingRespone_OK    = 1;
    //-----
    
    static final int    REQUEST_BILLING = 103;
	static final String TAG = "przurb Billing";
    static final String SKU_DEFAULT = "fotoshot.weaponpack";
    static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgEWhQRDs45RFv+tKXub2AbYkpF17YHrmaNww7K2mDzVm6gcJVO9y1oDlWShtxgaN1BRPOiW6pV09fqDqIwVANOtjgpUjz4J4tfOsIN5jTPKunZwCZtwULoAVmtgZtDSz7mj9ueZMLRJGiOtg5BQwqvewQdSvWEbuHs3kZw+oNcOm+OhvsfYDqzRurvPdCbUvUD6zTbLnEHP2UiHSS513J/NSIQBbRtFJysBYXgFTvVM8aAzBK9PWeSP+BXcivKK6lKsbt6ugezUT7jfbDyqibl/LWUn/Y0bcyEYyq8sjlDYPzjEKndztNqQD1LMDM/v0GQU4MgYJZbloJDIM1lOMKQIDAQAB";
    
    static private String SKU_ITEM;
    private Activity mActivity;
    private IabHelper mHelper;
    
    public Billing(Activity act) {
        mActivity = act;
        
        SKU_ITEM = SKU_DEFAULT;
        mHelper = new IabHelper(mActivity, base64EncodedPublicKey);
        
        
        // enable debug logging (for a production application, you should set
        // this to false).
        mHelper.enableDebugLogging(true);
        
        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        Log.d(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");
                
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    complain("Problem setting up in-app billing: " + result);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                Log.d(TAG, "Setup successful. Querying inventory.");
                mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
    }

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                complain("Failed to query inventory: " + result);
                return;
            }

            Log.d(TAG, "Query inventory was successful.");

			/*
			 * Check for items we own. Notice that for each purchase, we check
			 * the developer payload to see if it's correct! See
			 * verifyDeveloperPayload().
			 */

			// Check for gas delivery -- if we own gas, we should fill up the
			// tank immediately
			Purchase removeAdsPurchase = inventory.getPurchase(SKU_ITEM);
			if (removeAdsPurchase != null
					&& verifyDeveloperPayload(removeAdsPurchase)) {
				Log.d(TAG, "User has already purchased this item for removing ads. Write the Logic for removign Ads.");
				mHelper.consumeAsync(inventory.getPurchase(SKU_ITEM),
						mConsumeFinishedListener);
				return;
			}

			Log.d(TAG, "Initial inventory query finished; enabling main UI.");
		}

	};

	// User clicked the "Buy Gas" button
	public void buyWeaponPack() {
		Log.d(TAG, "Buy gas button clicked.");

		/*
		 * TODO: for security, generate your payload here for verification. See
		 * the comments on verifyDeveloperPayload() for more info. Since this is
		 * a SAMPLE, we just use an empty string, but on a production app you
		 * should carefully generate this.
		 */
		String payload = "";

//                mHelper.queryInventoryAsync(mGotInventoryListener);

		mHelper.launchPurchaseFlow(mActivity, SKU_ITEM, REQUEST_BILLING,
				mPurchaseFinishedListener, payload);
	}

	public boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "handleActivityResult");
        return mHelper.handleActivityResult(requestCode, resultCode, data);
    }

    /** Verifies the developer payload of a purchase. */
    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();
        String t= "payload: " + payload;
        Log.d(TAG, t);
        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        return true;
    }

	// Callback for when a purchase is finished
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.getResponse() == IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED ) {
                Log.d(TAG, "Purchase already owned - successful.");
                nativeBillingCallback( BillingRespone_OK );
                return;
            }

			if (result.isFailure()) {
				complain("Error purchasing: " + result);
                nativeBillingCallback( BillingRespone_Error );
				return;
			}

			if (!verifyDeveloperPayload(purchase)) {
				complain("Error purchasing. Authenticity verification failed.");
                nativeBillingCallback( BillingRespone_Error );
				return;
			}

			Log.d(TAG, "Purchase successful.");

			if (purchase.getSku().equals(SKU_ITEM)) {
//                nativeBillingCallback();
                nativeBillingCallback( BillingRespone_OK );
				Log.d(TAG,
						"removeAdsPurchase was succesful.. starting consumption.");
				mHelper.consumeAsync(purchase, mConsumeFinishedListener);
			}
		}
	};

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

			// We know this is the "gas" sku because it's the only one we
			// consume,
			// so we don't check which sku was consumed. If you have more than
			// one
			// sku, you probably should check...
			if (result.isSuccess()) {
				// successfully consumed, so we apply the effects of the item in
				// our
				// game world's logic, which in our case means filling the gas
				// tank a bit
				Log.d(TAG, "Consumption successful. Provisioning.");
				alert("You have purchased for removing ads from your app.");

                nativeBillingCallback( BillingRespone_OK );

			} else {
				complain("Error while consuming: " + result);
			}
			Log.d(TAG, "End consumption flow.");
		}
	};


	void complain(String message) {
		Log.e(TAG, "**** IN APP Purchase Error: " + message);
		alert(message);
	}

	void alert(String message) {
		Log.d(TAG, "Showing alert dialog: " + message);
//		TextView resultTv = (TextView) findViewById(R.id.textView_result);
//		resultTv.setText("Result : " + message);
	}

    public void setBuyTestPurchased() {
        SKU_ITEM = "android.test.purchased";
        Log.d(TAG, "set buy: " + SKU_ITEM);
    }

    public void setBuyTestCanceled() {
        SKU_ITEM = "android.test.canceled";
        Log.d(TAG, "set buy: " + SKU_ITEM);
    }

    public void setBuyTestRefunded() {
        SKU_ITEM = "android.test.refunded";
        Log.d(TAG, "set buy: " + SKU_ITEM);
    }

    public void setBuyTestUnavailable() {
        SKU_ITEM = "android.test.item_unavailable";
        Log.d(TAG, "set buy: " + SKU_ITEM);
    }

    public void setBuyDefault() {
        SKU_ITEM = SKU_DEFAULT;
        Log.d(TAG, "set buy: " + SKU_ITEM);
    }

    public String getBuyItemId() {
        Log.d(TAG, "get IAP item id: "  + SKU_ITEM);
        return SKU_ITEM;
    }

    private static native void nativeBillingCallback( int status );

}
