#!/bin/sh

CONFIG=cfg.$$

sed /LOCAL_SRC_FILES/,/DONE_LOCAL_SRC_FILES/d test.mk >> $CONFIG


files=`find ../../Classes fotoshot | grep -e \\\\.cpp$ -e \\\\.c$`

echo "LOCAL_SRC_FILES := \\" >> $CONFIG
for f in $files; do
	echo "$f \\" >> $CONFIG
	#echo "$f \\"
done
echo >> $CONFIG
echo "#DONE_LOCAL_SRC_FILES" >> $CONFIG

mv $CONFIG Android.mk
