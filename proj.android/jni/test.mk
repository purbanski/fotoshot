LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)

LOCAL_MODULE := fotoshotcpp_shared
LOCAL_MODULE_FILENAME := libfotoshotcpp

LOCAL_SRC_FILES := \
../../Classes/ui/ShareMenu.cpp 
#DONE_LOCAL_SRC_FILES

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes \
$(LOCAL_PATH)/../../Classes/platform \
$(LOCAL_PATH)/../../Classes/platform/android \
$(LOCAL_PATH)/../../Classes/debug \
$(LOCAL_PATH)/../../Classes/ui \
$(LOCAL_PATH)/../../Classes/ui/effects \
$(LOCAL_PATH)/../../Classes/ui/settings \
$(LOCAL_PATH)/../../Classes/game \
$(LOCAL_PATH)/../../Classes/game/weapons \
$(LOCAL_PATH)/../../Classes/tools \
$(LOCAL_PATH)/../../Classes/thirdparty \
$(LOCAL_PATH)/../../Classes/thirdparty/libpng \
$(LOCAL_PATH)/../../Classes/thirdparty/lodepng \
$(LOCAL_PATH)/../../Classes/physic \
$(LOCAL_PATH)/../../Classes/physic/blocks \
$(LOCAL_PATH)/../../cocos2d/cocos/platform/android/jni \
$(LOCAL_PATH)/fotoshot

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cocos2dx_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
