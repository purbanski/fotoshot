#ifdef BUILD_ANDROID
//------------------------------------------------------------
#include "JniHelper.h"
#include <jni.h>
#include "cocos2d.h"
#include "Brain.h"
//------------------------------------------------------------
using namespace cocos2d;
//------------------------------------------------------------
extern "C"
{
    //------------------------------------------------------------
    // Default photo saved
    JNIEXPORT void JNICALL
    Java_com_blackted_fotoshot_FsCamera_nativeDefaultPhotoSaved
    (JNIEnv*  env, jobject thiz/*, jstring apkPath*/) {
        log("przurb native Default Photo Saved");
        Brain::Get()->DefaultPhotoSaved();
    }

    //------------------------------------------------------------
    // Get Ball Count
    JNIEXPORT jint JNICALL
    Java_com_blackted_fotoshot_AppActivity_nativeGetBallCount
    (JNIEnv*  env, jobject thiz) {
        log("przurb native Get Ball Count");
        return Brain::Get()->GetBallCount();
    }

    
    //------------------------------------------------------------
    // Array colors loaded
    JNIEXPORT void JNICALL
    Java_com_blackted_fotoshot_FsCamera_nativeDefaultColorString
    (JNIEnv*  env, jobject thiz, jint w, jint h, jintArray colors)
    {
        log("przurb native array colors loaded");

        int size = env->GetArrayLength(colors);
        jint colorsCopy[size];

        env->GetIntArrayRegion(colors, 0, size, colorsCopy);

        intptr_t colorLong[size];

        for(int i = 0; i < size; i++)
            colorLong[i] = colorsCopy[i];
        
        Brain::Get()->LoadColors(w,h,colorLong);
    }

    
    //------------------------------------------------------------
    // Billing callback
    JNIEXPORT void JNICALL
    Java_com_blackted_fotoshot_Billing_nativeBillingCallback
    (JNIEnv*  env, jobject thiz, jint status)
    {
        log("przurb Billing c++ callback %d", (int)status);
        if ((int)status == 1 )
            Brain::Get()->WeaponPackEnable();
            
            //        BuyScene::BillingResponseStatic((BuyScene::BillingResponseType)status, NULL);
//        alog("Billing callback callled");
    }

    
    //JNIEXPORT void JNICALL Java_org_cocos2dx_lib_Cocos2dxHelper_nativeSetContext(JNIEnv*  env, jobject thiz, jobject context, jobject assetManager) {
    //      JniHelper::setClassLoaderFrom(context);
    //    FileUtilsAndroid::setassetmanager(AAssetManager_fromJava(env, assetManager));
    //}
    
    //JNIEXPORT void JNICALL Java_org_cocos2dx_lib_Cocos2dxHelper_nativeSetEditTextDialogResult(JNIEnv * env, jobject obj, jbyteArray text) {
    //        jsize  size = env->GetArrayLength(text);
    //
    //        if (size > 0) {
    //            jbyte * data = (jbyte*)env->GetByteArrayElements(text, 0);
    //            char* buffer = (char*)malloc(size+1);
    //            if (buffer != nullptr) {
    //                memcpy(buffer, data, size);
    //                buffer[size] = '\0';
    //                // pass data to edittext's delegate
    //                if (s_editTextCallback) s_editTextCallback(buffer, s_ctx);
    //                free(buffer);
    //            }
    //            env->ReleaseByteArrayElements(text, data, 0);
    //        } else {
    //            if (s_editTextCallback) s_editTextCallback("", s_ctx);
    //        }
    //}
}
//------------------------------------------------------------

#endif
