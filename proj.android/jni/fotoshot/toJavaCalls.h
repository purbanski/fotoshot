#ifdef BUILD_ANDROID

#ifndef __FOTOSHOT_TO_JAVA_CALLS_JNI_H__
#define __FOTOSHOT_TO_JAVA_CALLS_JNI_H__

#include <string>

void takePhotoJNI();
void appStartUpJNI();

void billing_buyWeaponPackJNI();
void billing_setBuyDefaultJNI();

void billing_setBuyTestCanceledJNI();
void billing_setBuyTestUnavailableJNI();
void billing_setBuyTestRefundedJNI();
void billing_setBuyTestPurchasedJNI();

void billing_getBuyItemIdJNI();


#endif

#endif