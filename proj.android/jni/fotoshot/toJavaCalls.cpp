#ifdef BUILD_ANDROID

#include "JniHelper.h"
#include <string.h>
#include "base/CCDirector.h"
#include "../CCApplication.h"
#include "platform/CCFileUtils.h"
#include <jni.h>
#include "cocos2d.h"
#include "Brain.h"

using namespace cocos2d;

//-----------------------------------------------------------------
// Take Photo
//-----------------------------------------------------------------
void takePhotoJNI() {
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       "com/blackted/fotoshot/AppActivity",
                                       "takePhoto",
                                       "()V")) {
        t.env->CallStaticIntMethod(t.classID, t.methodID );
        t.env->DeleteLocalRef(t.classID);
    }
}

//-----------------------------------------------------------------
// App StartUp
//-----------------------------------------------------------------
void appStartUpJNI() {
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       "com/blackted/fotoshot/AppActivity",
                                       "appStartUp",
                                       "()V")) {
        t.env->CallStaticIntMethod(t.classID, t.methodID );
        t.env->DeleteLocalRef(t.classID);
    }
}


//-----------------------------------------------------------------
// Billing
//-----------------------------------------------------------------
//---------------------------
// buy weapon pack
//---------------------------
void billing_buyWeaponPackJNI()
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       "com/blackted/fotoshot/AppActivity",
                                       "buyWeaponPack",
                                       "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}

//---------------------------
// set buy default (levelpack)
//---------------------------
void billing_setBuyDefaultJNI()
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       "com/blackted/fotoshot/AppActivity",
                                       "setBuyDefault",
                                       "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}

//---------------------------
// set buy test purchased
//---------------------------
void billing_setBuyTestPurchasedJNI()
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       "com/blackted/fotoshot/AppActivity",
                                       "setBuyTestPurchased",
                                       "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}

//---------------------------
// set buy test canceled
//---------------------------
void billing_setBuyTestCanceledJNI()
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       "com/blackted/fotoshot/AppActivity",
                                       "setBuyTestCanceled",
                                       "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}

//---------------------------
// set buy test refunded
//---------------------------
void billing_setBuyTestRefundedJNI()
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       "com/blackted/fotoshot/AppActivity",
                                       "setBuyTestRefunded",
                                       "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}

//---------------------------
// set buy test unavailable
//---------------------------
void billing_setBuyTestUnavailableJNI()
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       "com/blackted/fotoshot/AppActivity",
                                       "setBuyTestUnavailable",
                                       "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}

//---------------------------
// get buy item
//---------------------------
void billing_getBuyItemIdJNI()
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       "com/blackted/fotoshot/AppActivity",
                                       "getBuyItemId",
                                       "()V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}

//------------------------------------------------------------------
// billing finish
//------------------------------------------------------------------


#endif



