LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)

LOCAL_MODULE := fotoshotcpp_shared
LOCAL_MODULE_FILENAME := libfotoshotcpp

LOCAL_SRC_FILES := \
../../Classes/AppDelegate.cpp \
../../Classes/Config.cpp \
../../Classes/debug/TestNode.cpp \
../../Classes/DestroyScene.cpp \
../../Classes/FotoShotTypes.cpp \
../../Classes/game/BallCount.cpp \
../../Classes/game/Blocks.cpp \
../../Classes/game/Brain.cpp \
../../Classes/game/PhysicRenderNode.cpp \
../../Classes/game/RunningSettings.cpp \
../../Classes/game/Status.cpp \
../../Classes/game/weapons/Bomber.cpp \
../../Classes/game/weapons/Shooter.cpp \
../../Classes/game/weapons/ShotDef.cpp \
../../Classes/game/weapons/Slider.cpp \
../../Classes/game/weapons/WeaponManager.cpp \
../../Classes/physic/blocks/puBall.cpp \
../../Classes/physic/blocks/puBlock.cpp \
../../Classes/physic/blocks/puBlockDef.cpp \
../../Classes/physic/blocks/puBomb.cpp \
../../Classes/physic/blocks/puBorder.cpp \
../../Classes/physic/blocks/puBullet.cpp \
../../Classes/physic/blocks/puSlider.cpp \
../../Classes/physic/World.cpp \
../../Classes/platform/android/Platform_android.cpp \
../../Classes/SoundEngine.cpp \
../../Classes/thirdparty/lodepng/lodepng.cpp \
../../Classes/tools/PngTool.cpp \
../../Classes/tools/Tools.cpp \
../../Classes/tools/VisibleRect.cpp \
../../Classes/ui/Background.cpp \
../../Classes/ui/BaseMenu.cpp \
../../Classes/ui/effects/BombMenu.cpp \
../../Classes/ui/effects/ShotMenu.cpp \
../../Classes/ui/effects/SliderMenu.cpp \
../../Classes/ui/EffectsMenu.cpp \
../../Classes/ui/FxMenuItemImage.cpp \
../../Classes/ui/MainMenu.cpp \
../../Classes/ui/ProcessingLayer.cpp \
../../Classes/ui/settings/BubbleCountMenu.cpp \
../../Classes/ui/settings/BuyMenu.cpp \
../../Classes/ui/SettingsMenu.cpp \
../../Classes/ui/ShareMenu.cpp \
fotoshot/fromJavaCalls.cpp \
fotoshot/main.cpp \
fotoshot/toJavaCalls.cpp \

#DONE_LOCAL_SRC_FILES

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes \
$(LOCAL_PATH)/../../Classes/platform \
$(LOCAL_PATH)/../../Classes/platform/android \
$(LOCAL_PATH)/../../Classes/debug \
$(LOCAL_PATH)/../../Classes/ui \
$(LOCAL_PATH)/../../Classes/ui/effects \
$(LOCAL_PATH)/../../Classes/ui/settings \
$(LOCAL_PATH)/../../Classes/game \
$(LOCAL_PATH)/../../Classes/game/weapons \
$(LOCAL_PATH)/../../Classes/tools \
$(LOCAL_PATH)/../../Classes/thirdparty \
$(LOCAL_PATH)/../../Classes/thirdparty/libpng \
$(LOCAL_PATH)/../../Classes/thirdparty/lodepng \
$(LOCAL_PATH)/../../Classes/physic \
$(LOCAL_PATH)/../../Classes/physic/blocks \
$(LOCAL_PATH)/../../cocos2d/cocos/platform/android/jni \
$(LOCAL_PATH)/fotoshot

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cocos2dx_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
