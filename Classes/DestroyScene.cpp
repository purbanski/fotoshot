#include "DestroyScene.h"
#include "VisibleRect.h"
#include "Brain.h"
#include "World.h"
//-------------------------------------------------
DestroySceneLayer* DestroySceneLayer::_sInstance = NULL;
//-------------------------------------------------
cocos2d::Scene* DestroySceneLayer::createScene()
{
    auto scene = cocos2d::Scene::create();
    auto layer = DestroySceneLayer::create();
    scene->addChild(layer, layer->getLocalZOrder(), DestroySceneLayer::LayerTag );
//    layer->postInit();
    return scene;
}
//-------------------------------------------------
DestroySceneLayer::DestroySceneLayer()
{
    _sInstance = this;
}
//-------------------------------------------------
DestroySceneLayer::~DestroySceneLayer()
{
    
}
//-------------------------------------------------
bool DestroySceneLayer::init()
{
    if ( !Layer::init() )
        return false;

    _background = Background::create();
    addChild(_background, Brain::eLayerBackground);
    
    _processImg = ProcessingLayer::create();
    addChild(_processImg, Brain::eLayerProcessing);
    _weapon = NULL;

    auto mainMenu = DestroySceneMenu::Create();
    addChild(mainMenu, Brain::eLayerMenu);
    mainMenu->setPosition(cocos2d::Vec2(0,0));
    
//    setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(DestroySceneLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(DestroySceneLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(DestroySceneLayer::onTouchEnded, this);

    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    scheduleUpdate();
    
    return true;
}
//-------------------------------------------------
void DestroySceneLayer::AccelStart() {
    cocos2d::Device::setAccelerometerEnabled(true);
    _accelListener = cocos2d::EventListenerAcceleration::create(CC_CALLBACK_2(DestroySceneLayer::onAccel, this));
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_accelListener, this);
}
//-------------------------------------------------
void DestroySceneLayer::AccelStop() {
    cocos2d::Device::setAccelerometerEnabled(false);
    if (_accelListener)
        _eventDispatcher->removeEventListener(_accelListener);
    _accelListener = NULL;
    World::Get()->GravitySetZero();
}
//-------------------------------------------------
void DestroySceneLayer::update(float delta) {
    unscheduleUpdate();
    
    Brain::Get()->StartUp();
}
//-------------------------------------------------
void DestroySceneLayer::menuCloseCallback(Ref* pSender)
{
    cocos2d::Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
//-------------------------------------------------
void DestroySceneLayer::UpdateWeapon() {
    _weapon = Brain::Get()->GetWeapon();

    if (_weapon)
        addChild(_weapon,Brain::eLayerWeapon);
}
//-------------------------------------------------
bool DestroySceneLayer::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event) {
    cocos2d::log("touch began");
    
    if(!touch || !_weapon)
        return false;
    
    cocos2d::Point location = touch->getLocation();
    
    _weapon->SetStartPoint(location.x, location.y);
    _weapon->SetEndPoint(location.x, location.y);
    return true;
}
//-------------------------------------------------
void DestroySceneLayer::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event) {
    cocos2d::log("touch moved");
    
    if(!touch)
        return;
    
    cocos2d::Point location = touch->getLocation();
    _weapon->SetEndPoint(location.x, location.y);
    
}
//-------------------------------------------------
void DestroySceneLayer::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event) {
    cocos2d::log("touch ended");
    
    if(!touch)
        return;
    
    cocos2d::Point location = touch->getLocation();
    _weapon->SetEndPoint(location.x, location.y);
    _weapon->Shot();
}
//-------------------------------------------------
void DestroySceneLayer::onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    
}
//-------------------------------------------------
void DestroySceneLayer::onAccel(cocos2d::Acceleration* accel, cocos2d::Event* event)
{
    
    cpVect gravity;
    float scale;
    
    scale = 3000.0f;
    gravity.x = accel->x * scale;
    gravity.y = accel->y * scale;
    
    World::GetSpace()->gravity=gravity;
}
//-------------------------------------------------
void DestroySceneLayer::ShowProcessing()
{
    _processImg->Show();
}
//-------------------------------------------------
void DestroySceneLayer::HideProcessing()
{
    _processImg->Hide();
}
//-------------------------------------------------


