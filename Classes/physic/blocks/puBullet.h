#ifndef __Foto_Shot__puBullet__
#define __Foto_Shot__puBullet__
//------------------------------------------------------
#include "puBlock.h"
//------------------------------------------------------
// puBullet
//------------------------------------------------------
class puBullet : public puBlock
{
public:
    puBullet();
    puBullet(float radius, float x, float y);
    virtual ~puBullet();
    
    virtual void SetFresh();
    
private:
    void Const(float radius);
};
//------------------------------------------------------
#endif
