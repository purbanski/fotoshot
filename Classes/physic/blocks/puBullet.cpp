#include "puBullet.h"
#include "World.h"
#include "Tools.h"
#include "Blocks.h"

//------------------------------------------------------------
// puBullet
//------------------------------------------------------------
puBullet::puBullet()
{
    Const(101.95f);
}
//------------------------------------------------------------
puBullet::puBullet(float radius, float x, float y)
{
    Const(radius);
    _body->p = cpv(x,y);
    
    _blockDef._x = x;
    _blockDef._y = y;
}
//------------------------------------------------------------
void puBullet::Const(float radius)
{
    _body = cpBodyNew(INFINITY, INFINITY);
    
	_shape = cpCircleShapeNew(_body, radius, cpvzero);
	_shape->e = 0.0f;
    _shape->u = 0.0f;
	
    _shape->data = this;
    
	cpSpaceAddBody(World::GetSpace(), _body);
	cpSpaceAddShape(World::GetSpace(), _shape);
    
    float r = Tools::Rand(0, 1);
    float g = Tools::Rand(0, 1);
    float b = Tools::Rand(0, 1);
    float a = Tools::Rand(0, 1);
    
    _color = cocos2d::Color4F(r, g, b, a);
    _blockType = eBlock_Bullet;
}
//------------------------------------------------------------
puBullet::~puBullet()
{
//    cpSpaceRemoveBody(World::GetSpace(), _body);
//    cpSpaceRemoveShape(World::GetSpace(), _shape);
//    
//    cpBodyFree(_body);
//    cpShapeFree(_shape);
}
//------------------------------------------------------------
void puBullet::SetFresh()
{
    cpVect zeroVec;
    zeroVec.x = 0.0f;
    zeroVec.y = 0.0f;
    
    _body->v = zeroVec;
    _body->f = zeroVec;
    _body->p = cpv(9999999,9999999);
    
    Blocks::Get()->ScheduleDeleteBlock(this);
}

