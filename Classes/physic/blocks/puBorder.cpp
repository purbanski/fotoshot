#include "puBorder.h"
#include "World.h"
#include "Tools.h"
#include "VisibleRect.h"

//------------------------------------------------------------
// puBorder
//------------------------------------------------------------
puBorder::puBorder()
{
    Const();
}
//------------------------------------------------------------
puBorder::puBorder(float x, float y)
{
    Const();
    _body->p = cpv(x,y);
}
//------------------------------------------------------------
void puBorder::Const()
{
    _skipBaseDestructor = true;
    _color = cocos2d::Color4F(0,0,1,0.5f);

    _body = cpBodyNewStatic();
    cpVect a,b;
    
    cocos2d::Point rb;
    rb = VisibleRect::rightBottom();
    
    cpShape *shape;
    float e = 0.3f;
    float f = 0.3f; //friction
    float bottomY = 140.0f;
    float topDeltaY = 10.0f;
    float barWidth = 10.0f;
    
    cocos2d::log("rb %f %f", rb.x, rb.y);
    
    //------------------------------------
    a.x=0;
    a.y=rb.y+bottomY;
    b.x=rb.x;
    b.y=rb.y+bottomY;
    
    shape = cpSegmentShapeNew(_body, a, b, barWidth);
    shape->e = e;
    shape->u = f;
    shape->data = this;
    cpSpaceAddStaticShape(World::GetSpace(), shape);
    _shapes.insert(shape);
    
    //------------------------------------
    a.x=barWidth;
    a.y=VisibleRect::top().y - topDeltaY;
    b.x=barWidth;
    b.y=bottomY;

    shape = cpSegmentShapeNew(_body, a, b, barWidth);
    shape->e = e;
    shape->u = f;
    shape->data = this;
    cpSpaceAddStaticShape(World::GetSpace(), shape);
    _shapes.insert(shape);
    
    //------------------------------------
    a.x=rb.x-barWidth;
    a.y=VisibleRect::top().y - topDeltaY;
    b.x=rb.x-barWidth;
    b.y=bottomY;
    
    shape = cpSegmentShapeNew(_body, a, b, barWidth);
    shape->e = e;
    shape->u = f;
    shape->data = this;
    cpSpaceAddStaticShape(World::GetSpace(), shape);
    _shapes.insert(shape);
    
    //------------------------------------
    a.x=0;
    a.y=VisibleRect::top().y - topDeltaY;
    b.x=rb.x;
    b.y=VisibleRect::top().y - topDeltaY;
    
    shape = cpSegmentShapeNew(_body, a, b, barWidth);
    shape->e = e;
    shape->u = f;
    shape->data = this;
    cpSpaceAddStaticShape(World::GetSpace(), shape);
    _shapes.insert(shape);
    
    //------------------------------------

    _blockType = eBlock_Bottom;
}
//------------------------------------------------------------
puBorder::~puBorder()
{
    _shapes.DeleteAll();
    cpBodyFree(_body);
}





