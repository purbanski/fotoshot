#ifndef __Foto_Shot__puBlock__
#define __Foto_Shot__puBlock__
//------------------------------------------------------
#include "cocos2d.h"
#include "chipmunk.h"
#include "puBlockDef.h"
#include <set>
#include "World.h"
//------------------------------------------------------
typedef enum {
    eBlock_Unknown = 0,
    eBlock_Ball = 1,
    eBlock_Bullet,
    eBlock_Slider,
    eBlock_Bottom,
    eBlock_Bomb,
    eBlock_StaticBall,
} BlockType;


//------------------------------------------------------
// Shapes
//------------------------------------------------------
class Shapes : public std::set<cpShape*> {
public:
    void DeleteAll() {
        for (auto shape : *this)
        {
            cpSpaceRemoveShape(World::GetSpace(), shape);
            cpShapeFree(shape);
        }
        clear();
    }
};

//------------------------------------------------------
// puBlock
//------------------------------------------------------
class puBlock
{
public:
    virtual ~puBlock();
    
    cpShape* GetShape();
    cpBody*  GetBody();
    
    cocos2d::Color4F GetColor();
    void SetColor( cocos2d::Color4F color );
    void SetColor( unsigned char r, unsigned char g, unsigned char b, unsigned char a );
    
    void SetInitVelocity(float xVel, float yVel);
    
    virtual void SetFresh();
    BlockType GetBlockType();
    
protected:
    puBlock();
    puBlock( const cocos2d::Point& start );
    void CenterBody();
    
protected:
    BlockType   _blockType;
    
    Shapes       _shapes;
    cpShape*     _shape;
    cpBody*     _body;
    
    cocos2d::Color4F         _color;
    cocos2d::Point  _startPoint;
    puBlockDef  _blockDef;
    bool        _skipBaseDestructor;
};

#endif /* defined(__Foto_Shot__puBlock__) */
