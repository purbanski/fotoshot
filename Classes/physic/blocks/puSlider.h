#ifndef __FotoShot__puSlider__
#define __FotoShot__puSlider__
//------------------------------------------------------
#include "puBlock.h"
//------------------------------------------------------
// puSlider
//------------------------------------------------------
class puSlider : public puBlock
{
public:
    typedef enum {
        eSliderS = 1,
        eSliderM,
        eSliderL,
        eSliderReversM,
        eSliderReversL,
        eSliderLine,
        eSliderDotLines,
    } SliderType;
    
public:
    puSlider(SliderType type);
    virtual ~puSlider();
    
private:
    void Const(SliderType type);
    void SetSlider(float dx, float dy);
    void SetSliderLine(float len);
    void SetSliderDotLine(int count);
};
//------------------------------------------------------
#endif 
