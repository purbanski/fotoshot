#include "puBlock.h"
#include "VisibleRect.h"
//------------------------------------------------------------
// puBlock
//------------------------------------------------------------
puBlock::puBlock()
{
    _body = NULL;
    _shape = NULL;
    _skipBaseDestructor = false;
    
    _blockType = eBlock_Unknown;
    _startPoint.x = 0;
    _startPoint.y = 0;
}
//------------------------------------------------------------
puBlock::puBlock(const cocos2d::Point &point )
{
    _body = NULL;
    _shape = NULL;

    _startPoint = point;
    _blockType = eBlock_Unknown;
}
//------------------------------------------------------------
puBlock::~puBlock()
{
    if (_skipBaseDestructor)
        return;
    
    cpSpaceRemoveBody(World::GetSpace(), _body);
    
    if (_shape)
    {
        cpSpaceRemoveShape(World::GetSpace(), _shape);
        cpShapeFree(_shape);
    }
    else
        _shapes.DeleteAll();
    
    cpBodyFree(_body);
}
//------------------------------------------------------------
cocos2d::Color4F puBlock::GetColor()
{
    return _color;
}
//------------------------------------------------------------
BlockType puBlock::GetBlockType()
{
    return _blockType;
}
//------------------------------------------------------------
void puBlock::SetColor(cocos2d::Color4F color)
{
    _color = color;
}
//------------------------------------------------------------
void puBlock::SetColor( unsigned char r, unsigned char g, unsigned char b, unsigned char a )
{
    _color = cocos2d::Color4F((float)r/255.0f,
                   (float)g/255.0f,
                   (float)b/255.0f,
                   (float)a/255.0f);
}
//------------------------------------------------------------
void puBlock::CenterBody() {
    cocos2d::Point p = VisibleRect::center();
    _body->p = cpv(p.x,p.y);
}
//------------------------------------------------------------
cpShape* puBlock::GetShape()
{
    cpShape* shape;
    
    if (_shape)
        shape = _shape;
    else
        shape = *(_shapes.begin());
    
    return shape;
}
//------------------------------------------------------------
cpBody* puBlock::GetBody() {
    return _body;
}
//------------------------------------------------------------
void puBlock::SetFresh()
{
    _body->p = cpv(_startPoint.x, _startPoint.y);
    _body->v = cpv(0,0);
    _body->f = cpv(0,0);
}
//------------------------------------------------------------
void puBlock::SetInitVelocity(float xVel, float yVel )
{
    _blockDef._xVel = xVel;
    _blockDef._yVel = yVel;
    _body->v = cpv(xVel, yVel);
}