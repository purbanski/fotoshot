#ifndef __PUBALL_H__
#define __PUBALL_H__
//------------------------------------------------------
#include "puBlock.h"
//------------------------------------------------------
// puBall
//------------------------------------------------------
class puBall : public puBlock
{
public:
    puBall();
    puBall( float radius );
    puBall(float x, float y);
    puBall( float radius, float x, float y );
    virtual ~puBall();
    
private:
    void Const(float radius);
    
};
//------------------------------------------------------
#endif

