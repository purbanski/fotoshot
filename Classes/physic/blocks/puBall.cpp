#include "puBall.h"
#include "World.h"
#include "Tools.h"

//------------------------------------------------------------
// puBall
//------------------------------------------------------------
puBall::puBall()
{
    Const(101.95f);
}
//------------------------------------------------------------
puBall::puBall(float radius, float x, float y) : puBlock(cocos2d::Point(x,y))
{
    Const(radius);
    _body->p = cpv(x,y);
}
//------------------------------------------------------------
puBall::puBall(float x, float y)
{
    Const(3.20f);
    _body->p = cpv(x,y);
}
//------------------------------------------------------------
void puBall::Const(float radius)
{
    _body = cpBodyNew(1.0f, INFINITY);

	_shape = cpCircleShapeNew(_body, radius, cpvzero);
	_shape->e = 0.0f;
    _shape->u = 0.0f;

    _shape->collision_type = CollisionType::eCollision_Ball;
    
    _shape->data = this;

	cpSpaceAddBody(World::GetSpace(), _body);
	cpSpaceAddShape(World::GetSpace(), _shape);
   
    float r = Tools::Rand(0, 1);
    float g = Tools::Rand(0, 1);
    float b = Tools::Rand(0, 1);
    float a = Tools::Rand(0, 1);
    
    _color = cocos2d::Color4F(r, g, b, a);
    _blockType = eBlock_Ball;
}
//------------------------------------------------------------
puBall::~puBall()
{
//    cpSpaceRemoveBody(World::GetSpace(), _body);
//    cpSpaceRemoveShape(World::GetSpace(), _shape);
//
//    cpBodyFree(_body);
//    cpShapeFree(_shape);
}





