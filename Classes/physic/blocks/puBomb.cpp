#include "puBomb.h"
#include "World.h"
#include "Tools.h"
#include "MyTypes.h"
//------------------------------------------------------------
// puBall
//------------------------------------------------------------
puBomb::puBomb()
{
    Const(10.95f);
    CenterBody();
}
//------------------------------------------------------------
puBomb::puBomb(float radius) {
    Const(radius);
    CenterBody();
}
//------------------------------------------------------------
puBomb::puBomb(float radius, float x, float y) : puBlock(cocos2d::Point(x,y))
{
    Const(radius);
    _body->p = cpv(x,y);
}
//------------------------------------------------------------
puBomb::puBomb(float x, float y)
{
    Const(3.20f);
    _body->p = cpv(x,y);
}
//------------------------------------------------------------
cpBool catcherBarBegin(cpArbiter *arb, cpSpace *space, void *unused);
void puBomb::Const(float radius)
{
    _bombAlpha = 0.5;
    _body = cpBodyNew(1.0f, INFINITY);
    
    _shape = cpCircleShapeNew(_body, radius, cpvzero);
    cpShapeSetSensor(_shape,true);
    
    _shape->e = 0.0f;
    _shape->u = 0.0f;
    
    _shape->data = this;
    _shape->collision_type = CollisionType::eCollision_Bomb;
    
    cpSpaceAddBody(World::GetSpace(), _body);
    cpSpaceAddShape(World::GetSpace(), _shape);
    
    _color = cocos2d::Color4F(0.5, 0.1f, 0.1f, _bombAlpha);
    _blockType = eBlock_Bomb;
}
//------------------------------------------------------------
void puBomb::DecBombVisibility() {
    if (!_bombAlpha)
        return;
    
    _bombAlpha -= 0.04f;
    if (_bombAlpha<0.0f)
        _bombAlpha = 0.0f;
    _color = cocos2d::Color4F(0.5, 0.1f, 0.1f, _bombAlpha);
}
//------------------------------------------------------------
puBomb::~puBomb()
{
//    cpSpaceRemoveBody(World::GetSpace(), _body);
//    cpSpaceRemoveShape(World::GetSpace(), _shape);
//    
//    cpBodyFree(_body);
//    cpShapeFree(_shape);
}
//------------------------------------------------------------
void puBomb::Explode() {
    
}

//------------------------------------------------------------
cpBool catcherBarBegin(cpArbiter *arb, cpSpace *space, void *unused)
{
    cpShape *a, *b;
    cpArbiterGetShapes(arb, &a, &b);
    
    cpVect ap = a->body->p;
    cpVect bp = b->body->p;
    cpVect f;
    float scale = 100;
    
    f.x = scale*(ap.x - bp.x);
    f.y = scale*(ap.y - bp.y);
    cocos2d::log("%p %p",a,b);
    
    b->body->p = f;
    return cpFalse;
}






