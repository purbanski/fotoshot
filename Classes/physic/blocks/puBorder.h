#ifndef __FotoShot__puBorder__
#define __FotoShot__puBorder__

//------------------------------------------------------
#include "puBlock.h"
//------------------------------------------------------
// puBorder
//------------------------------------------------------
class puBorder : public puBlock
{
public:
    puBorder();
    puBorder(float x, float y);
    ~puBorder();
    
private:
    void Const();
    
};
//------------------------------------------------------
#endif

