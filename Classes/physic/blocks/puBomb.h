#ifndef __FotoShot__puBomb__
#define __FotoShot__puBomb__
//------------------------------------------------------
#include "puBlock.h"
//------------------------------------------------------
// puBomb
//------------------------------------------------------
class puBomb : public puBlock
{
public:
    puBomb();
    puBomb( float radius );
    puBomb(float x, float y);
    puBomb( float radius, float x, float y );
    virtual ~puBomb();
    void Explode();
    void DecBombVisibility();
    
private:
    void Const(float radius);
    
private:
    float _bombAlpha;
};
//------------------------------------------------------
#endif

