#include "puSlider.h"
#include "World.h"
#include "Tools.h"
#include "Blocks.h"

//------------------------------------------------------------
// puSlider
//------------------------------------------------------------
puSlider::puSlider(puSlider::SliderType type)
{
    Const(type);
}
//------------------------------------------------------------
void puSlider::Const(SliderType type)
{
    _body = cpBodyNew(INFINITY, INFINITY);


    switch (type) {
        case eSliderS:
            SetSlider(30, 45);
            break;
        
        case eSliderM:
            SetSlider(60, 90);
            break;
            
        case eSliderL:
            SetSlider(120, 180);
            break;
            
        case eSliderReversM:
            SetSlider(60, -90);
            break;

        case eSliderReversL:
            SetSlider(120, -180);
            break;

        case eSliderLine:
            SetSliderLine(120);
            break;
            
        case eSliderDotLines:
            SetSliderDotLine(6);
            break;
            
        default:
            break;
    }


    cpSpaceAddBody(World::GetSpace(), _body);

    _body->v = cpv(0, -1100);
    _color = cocos2d::Color4F(1, 0, 0, 1);
    _blockType = eBlock_Slider;
}
//------------------------------------------------------------
void puSlider::SetSliderLine(float len)
{
    cpVect a,b;
    cpShape *shape;
    
    //-------------------------------
    float d = len / 2.0f;
    a.x=-d;
    a.y=0;
    b.x=d;
    b.y=0;
    shape = cpSegmentShapeNew(_body, a, b, 5);
    
    shape->e = 0.0f;
    shape->u = 0.0f;
    shape->data = this;
    cpSpaceAddShape(World::GetSpace(), shape);
    _shapes.insert(shape);
}
//------------------------------------------------------------
void puSlider::SetSliderDotLine(int count)
{
    float lLine = 5;
    float lBlank = 55;
    
    float len = lLine * count + lBlank*(count-1);
    for (int i=0; i<count; i++)
    {
        cpVect a,b;
        cpShape *shape;
    
        //-------------------------------
        float d= i*(lLine+lBlank) - len/2.0f;
        a.x=d;
        b.x=d+lLine;
        a.y=b.y=0;
        shape = cpSegmentShapeNew(_body, a, b, 5);
    
    shape->e = 0.0f;
    shape->u = 0.0f;
    shape->data = this;
    cpSpaceAddShape(World::GetSpace(), shape);
    _shapes.insert(shape);
    }
}
//------------------------------------------------------------
puSlider::~puSlider()
{
//    cpSpaceRemoveBody(World::GetSpace(), _body);
//    cpSpaceRemoveShape(World::GetSpace(), _shape);
//    
//    cpBodyFree(_body);
//    cpShapeFree(_shape);
}
//------------------------------------------------------------
void puSlider::SetSlider(float dx, float dy) {
    cpVect a,b;
    cpShape *shape;
    
    //-------------------------------
    a.x=-dx;
    a.y=dy;
    b.x=0;
    b.y=0;
    shape = cpSegmentShapeNew(_body, a, b, 5);
    
    shape->e = 0.0f;
    shape->u = 0.0f;
    shape->data = this;
    cpSpaceAddShape(World::GetSpace(), shape);
    _shapes.insert(shape);
    
    //-------------------------------
    a.x=dx;
    a.y=dy;
    b.x=0;
    b.y=0;
    shape = cpSegmentShapeNew(_body, a, b, 5);
    
    shape->e = 0.0f;
    shape->u = 0.0f;
    shape->data = this;
    cpSpaceAddShape(World::GetSpace(), shape);
    _shapes.insert(shape);
}
