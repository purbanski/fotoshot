#ifndef __WORLD_H__
#define __WORLD_H__
//------------------------------------------------------------------
#include "cocos2d.h"
#include "chipmunk.h"
#include "PhysicRenderNode.h"
//------------------------------------------------------------------
using namespace std;
//------------------------------------------------------------------
class World : public cocos2d::Node
{
public:
    static World* Get();
    static cpSpace* GetSpace();
    
    static void Destroy();
    ~World();

    virtual void update(float delta);
    void ScheduleFresh();
    
    void GravitySetEarth();
    void GravitySetZero();
    
private:
    World();
    void SetFresh();
    void ApplySettings();
    
private:
    PhysicRenderNode* _debugLayer;
    cpSpace*            _space;
    cpShape*            _walls[4];
    
    
    typedef enum
    {
        eState_Unknown = 0,
        eState_Running = 1,
        eState_Refresh_Init,
        eState_Refresh,
        eState_Refresh_2,
        eState_Refresh_3,
        eState_Refresh_4,
        eState_Refresh_5
    } RunState;
    
    RunState     _state;
    float       _delayTimer;
    int         _delayCount;
    
    static unsigned int      _sBallStep;

    static World* _sInstance;
};
//------------------------------------------------------------------

#endif

