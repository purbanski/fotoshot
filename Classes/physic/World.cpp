#include "World.h"
#include "VisibleRect.h"
#include "Blocks.h"
#include "Config.h"
#include "Brain.h"


//------------------------------------------------------------------
World* World::_sInstance = NULL;
unsigned int World::_sBallStep = 2;
//------------------------------------------------------------------
World* World::Get()
{
    if ( ! _sInstance )
    {
        _sInstance = new World();
        _sInstance->autorelease();
        _sInstance->retain();
    }
    return _sInstance;
}
//------------------------------------------------------------------
void World::Destroy()
{
    if ( _sInstance )
    {
        Blocks::Get()->CleanUp();
        Blocks::Get()->RemoveBorder();
        cpSpaceFree( _sInstance->_space );

        _sInstance->removeFromParent();
        _sInstance->release();
        _sInstance = NULL;
    }
}
//------------------------------------------------------------------
cpSpace* World::GetSpace()
{
    return Get()->_space;
}
//------------------------------------------------------------------
World::World()
{
    _state = eState_Running;
    _delayTimer = 0;
    _delayCount = 0;
    
    _space = cpSpaceNew();
    _space->gravity = cpv(0, 0);
	_space->iterations = 1;

    _debugLayer = PhysicRenderNode::create(_space);
    addChild(_debugLayer, 0);
    
    scheduleUpdate();
}

//------------------------------------------------------------------
World::~World()
{
}
//------------------------------------------------------------------
void World::GravitySetZero()
{
    _space->gravity = cpv(0, 0);
}
//------------------------------------------------------------------
void World::GravitySetEarth()
{
    _space->gravity = cpv(0, -1000);
}
//------------------------------------------------------------------
void World::update(float delta)
{
    Blocks::Get()->Update();
    float delay;
    int refreshCount;
    
#ifdef BUILD_IOS
    delay=0.1f;
    refreshCount=4;
#endif
    
#if BUILD_ANDROID
    delay = 0.02f;
    refreshCount = 6;
#endif
    
    switch (_state )
    {
        case eState_Refresh_Init:
            _space->gravity = cpv(0, 0);
            SetFresh();
            _state = eState_Refresh;
            break;
            
        case eState_Refresh:
            _delayTimer += delta;
            if (_delayTimer > delay )
            {
                SetFresh();
                _delayTimer = 0;
                _delayCount++;
                
                if (_delayCount == refreshCount)
                {
                    _state = eState_Running;
                    _delayCount = 0;
                    
                    ApplySettings();
                }
            }
            cocos2d::log("delat timer: %f", _delayTimer);
            break;

        case eState_Running :
        case eState_Unknown :
        default :
            break;
            
    }

    // Should use a fixed size step based on the animation interval.
    int steps = 3;
    float dt = cocos2d::Director::getInstance()->getAnimationInterval()/(float)steps;

    for(int i=0; i<steps; i++){
        cpSpaceStep(World::GetSpace(), dt);
    }
}
//------------------------------------------------------------------
void SetFresh_(cpShape *shape, void *data)
{
    puBlock *block;
    BlockType blockType;
//    cpSpace *space;
    
    block = (puBlock*) shape->data;
    blockType = block->GetBlockType();
    
    if ( block )
        if (blockType == eBlock_Ball || blockType == eBlock_Slider )
            block->SetFresh();
}
//------------------------------------------------------------------
void World::SetFresh()
{
    cpSpaceEachShape(_space, SetFresh_, (void*) _space);
}
//------------------------------------------------------------------
void World::ScheduleFresh()
{
    _state = eState_Refresh_Init;
}
//------------------------------------------------------------------
void World::ApplySettings()
{
    Brain::Get()->ApplyWorldSettings();
}


//------------------------------------------------------------------


