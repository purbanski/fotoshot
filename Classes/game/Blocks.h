#ifndef __BLOCKS_H__
#define __BLOCKS_H__
//------------------------------------------------------------------
#include "chipmunk.h"
#include "cocos2d.h"
#include "puBlocks.h"
#include <set>
//------------------------------------------------------------------
using namespace std;
class puBlock;
class puBorder;
//------------------------------------------------------------------
class Blocks : public cocos2d::Node
{
public:
    static Blocks* Get();
    static void Destroy();
    
    ~Blocks();

    puSlider* AddSlider(puSlider::SliderType sliderType);
    puBall* AddBall(float x, float y);
    puBall* AddBall(float x, float y, float radius,  cocos2d::Color4F color );
    puBullet* AddBullet(float x, float y, float radius );
    puBomb* AddBomb(float r);
    puBomb* AddBomb(float x, float y, float radius );

    
    puBorder* AddBorder();
    void RemoveBorder();
    bool IsBorderEnabled();
    

    void ScheduleDeleteBlock( puBlock *block );
    void CleanUp();
    void CleanUpBullets();
    
    void Update();
    
private:
    Blocks();
    
private:
    static Blocks* _sInstance;
    
    typedef set<puBlock*> BlocksSet;
    BlocksSet   _blocksCreated;
    BlocksSet   _blocksToDelete;
    
    puBorder*   _border;
    
};
//------------------------------------------------------------------

#endif

