#ifndef __FotoShot__PhysicRenderNode__
#define __FotoShot__PhysicRenderNode__
//------------------------------------------------------------
#include "cocos2d.h"
//------------------------------------------------------------
class PhysicRenderBlocksNode;
class PhysicRenderBombsNode;
//------------------------------------------------------------
class PhysicRenderNode : public cocos2d::Node
{
public:
    static PhysicRenderNode* create(cpSpace *space);
    virtual bool init() override;

    virtual ~PhysicRenderNode();
    
    cpSpace* getSpace() const;
    void setSpace(cpSpace *space);
    
    // Overrides
//    virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags) override;
    
private:
    PhysicRenderNode(cpSpace* space);
    
protected:
    PhysicRenderBlocksNode* _renderBlocks;
    PhysicRenderBombsNode*  _renderBombs;

    cpSpace *_space;
};

//------------------------------------------------------------
// Physic Render Blocks Node
//------------------------------------------------------------
class PhysicRenderBlocksNode : public cocos2d::DrawNode
{
public:
    static PhysicRenderBlocksNode* create(cpSpace *space);
    virtual ~PhysicRenderBlocksNode();
    
    void setSpace(cpSpace *space);
    
    // Overrides
    virtual void draw(cocos2d::Renderer *renderer,
                      const cocos2d::Mat4 &transform,
                      uint32_t flags) override;
    
private:
    PhysicRenderBlocksNode(cpSpace *space);
    
protected:
    cpSpace *_space;
    
};

//------------------------------------------------------------
// Physic Render Bombs Node
//------------------------------------------------------------
class PhysicRenderBombsNode : public cocos2d::DrawNode
{
public:
    static PhysicRenderBombsNode* create(cpSpace *space);
    virtual ~PhysicRenderBombsNode();
    
    void setSpace(cpSpace *space);

    // Overrides
    virtual void draw(cocos2d::Renderer *renderer,
                      const cocos2d::Mat4 &transform,
                      uint32_t flags) override;

private:
    PhysicRenderBombsNode(cpSpace *space);
    
protected:
    cpSpace *_space;
};
#endif
