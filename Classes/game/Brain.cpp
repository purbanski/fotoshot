#include "Brain.h"
#include <stdio.h>
#include "World.h"
#include "DestroyScene.h"
#include "Blocks.h"
#include "Platform.h"
#include "Config.h"
#include "PngTool.h"
#include "TestNode.h"
#include "VisibleRect.h"
//-----------------------------------------------------------------------------
Brain* Brain::_sInstance = NULL;
//-----------------------------------------------------------------------------
Brain* Brain::Get() {
    if (!_sInstance) {
        _sInstance = new Brain();
    }
    return _sInstance;
}
//-----------------------------------------------------------------------------
Brain::Brain()
{
}
//-----------------------------------------------------------------------------
Brain::~Brain() {
    
}

//-----------------------------------------------------------------------------
void Brain::StartUp() {
    auto scene = cocos2d::Director::getInstance()->getRunningScene();
    
    _sceneLayer = (DestroySceneLayer*)scene->getChildByTag(DestroySceneLayer::LayerTag);

    _sceneLayer->addChild(World::Get(), eLayerWorld);

    _weapon.SetLayer(_sceneLayer);
    SetWeapon(Weapon::eWeapon_Shooter);

    Platform::AppStartUp();
    
#ifdef BUILD_DEBUG
    auto testNode = TestNode::create();
    _sceneLayer->addChild(testNode, eLayerTest);
//    testNode->setPosition(VisibleRect::center());
#endif
}
//-----------------------------------------------------------------------------
void Brain::Restart() {
//    PngTool::Get()->LoadPng("pngs/welcome_shot_big.png");
    _sceneLayer->AccelStop();
    _weapon.Reset();
  
    World::Get()->ScheduleFresh();
    Blocks::Get()->CleanUpBullets();
}
//-----------------------------------------------------------------------------
void Brain::SetWeapon(Weapon::Type weapon )
{
    _weapon.SetWeapon(weapon);
}
//-----------------------------------------------------------------------------
Weapon* Brain::GetWeapon() {
    return _weapon.GetWeapon();
}
//-----------------------------------------------------------------------------
void Brain::TakePhoto()
{
    Platform::TakePhoto();
}
//-----------------------------------------------------------------------------
void Brain::DefaultPhotoSaved()
{
//    _sceneLayer->ShowProcessing();

    string wrpath = cocos2d::CCFileUtils::getInstance()->getWritablePath();
    wrpath.append(Config::imageDefaultFilename);

    cocos2d::log("File: %s", wrpath.c_str());
    
    PngTool::Get()->LoadPng(wrpath.c_str());
    UpdateImage();
}
//-----------------------------------------------------------------------------
void Brain::LoadColors( cocos2d::Size size, const Colors &colors ) {
//    _sceneLayer->ShowProcessing();
    cocos2d::log("przurb Load colors");
 
    PngTool::Get()->SetColors(size, colors);
    UpdateImage();
}
//-----------------------------------------------------------------------------
void Brain::LoadColors(int w, int h, intptr_t colors[] )
{
    cocos2d::log("przurb Load colors from java array");
    
    cocos2d::Size size;
    Colors colorsRet;
    cocos2d::Color4B col;
    
    size = cocos2d::Size(w,h);

    for (int i=0; i< w*h;  i++)
    {
        col.a = (colors[i] >> 24) & 0xff;;
        col.r = (colors[i] >> 16) & 0xff;
        col.g = (colors[i] >> 8) & 0xff;
        col.b = colors[i] & 0xff;
        
        colorsRet.push_back(col);
    }
    
    PngTool::Get()->SetColors(size, colorsRet);
    UpdateImage();
}
//-----------------------------------------------------------------------------
void Brain::UpdateImage() {
    if ( _weapon.GetWeapon() )
        _weapon.GetWeapon()->WorldCreatedPre();
    
    World::Destroy();
    
    if ( _weapon.GetWeapon() )
        _weapon.GetWeapon()->WorldCreatedPost();
    
    _sceneLayer->addChild(World::Get(), eLayerWorld);
    PngTool::Get()->CreateBubbledImage();
    
    ApplyWorldSettings();
    _sceneLayer->HideProcessing();
}
//-----------------------------------------------------------------------------
void Brain::ToggleGravity() {
    _settings._gravityEnabled = ! _settings._gravityEnabled;
    
    if ( _settings._gravityEnabled )
    {
        _sceneLayer->AccelStop();
        _settings._accelEnabled = false;

        Blocks::Get()->AddBorder();
        _settings._borderEnabled = true;

        World::Get()->GravitySetEarth();
    }
    else
    {
        World::Get()->GravitySetZero();

        _sceneLayer->AccelStop();
        Blocks::Get()->RemoveBorder();
        
        _settings._borderEnabled = false;
        Restart();
    }
}
//-----------------------------------------------------------------------------
void Brain::ToggleAccelator() {
    _settings._accelEnabled = ! _settings._accelEnabled;
    
    if ( _settings._accelEnabled )
    {
        _sceneLayer->AccelStart();

        Blocks::Get()->AddBorder();
        _settings._borderEnabled = true;
        
        _settings._gravityEnabled = false;
    }
    else
    {
        _sceneLayer->AccelStop();
        
        Blocks::Get()->RemoveBorder();
        _settings._borderEnabled = false;
        Restart();
    }
}
//-----------------------------------------------------------------------------
void Brain::ToggleBorder() {
    _settings._borderEnabled = ! _settings._borderEnabled;
    
    if ( _settings._borderEnabled )
    {
        Blocks::Get()->AddBorder();
    }
    else
    {
        Blocks::Get()->RemoveBorder();
    }
}
//-----------------------------------------------------------------------------
bool Brain::IsBorderEnabled()
{
    return Blocks::Get()->IsBorderEnabled();
}
//-----------------------------------------------------------------------------
unsigned int Brain::GetBallCount() {
    return _ballCount.GetCount();
}
//-----------------------------------------------------------------------------
void Brain::DecBallCount() {
    _ballCount.DecCount();
}
//-----------------------------------------------------------------------------
void Brain::IncBallCount() {
    _ballCount.IncCount();
}
//-----------------------------------------------------------------------------
void Brain::ApplyWorldSettings()
{
    if (_settings._accelEnabled)
        _sceneLayer->AccelStart();
    else
        _sceneLayer->AccelStop();
    
    
    if (_settings._borderEnabled)
        Blocks::Get()->AddBorder();
    else
        Blocks::Get()->RemoveBorder();
    
    if (_settings._gravityEnabled)
        World::Get()->GravitySetEarth();
    else if (!_settings._accelEnabled)
        World::Get()->GravitySetZero();
        
}
//-----------------------------------------------------------------------------
RunningSettings Brain::GetRunningSettings()
{
    return _settings;
}
//-----------------------------------------------------------------------------
void Brain::ShowProcessing()
{
    _sceneLayer->ShowProcessing();
}
//-----------------------------------------------------------------------------
void Brain::HideProcessing()
{
    _sceneLayer->HideProcessing();
}
//-----------------------------------------------------------------------------
bool Brain::IsWeaponPackEnabled()
{
    return _status.IsWeaponExtEnabled();
}
//-----------------------------------------------------------------------------
void Brain::WeaponPackEnable()
{
    _status.SetWeaponExtEnabled(true);
    DestroySceneMenu::Get()->RestartMenus();
    _weapon.SetWeapon(Weapon::eWeapon_Shooter);
}
//-----------------------------------------------------------------------------
void Brain::WeaponPackDisable()
{
    _status.SetWeaponExtEnabled(false);
    DestroySceneMenu::Get()->RestartMenus();
    _weapon.SetWeapon(Weapon::eWeapon_Shooter);
}
//-----------------------------------------------------------------------------

