#ifndef __FotoShot__RunningSettings__
#define __FotoShot__RunningSettings__

#include <stdio.h>

class RunningSettings {
public:
    RunningSettings();
    
    bool _gravityEnabled;
    bool _accelEnabled;
    bool _borderEnabled;
    
};

#endif
