#ifndef FotoShot_Weapon_h
#define FotoShot_Weapon_h

#include "cocos2d.h"

class Weapon : public cocos2d::DrawNode {
public:
    typedef enum EWeaponType{
        eWeapon_Shooter = 0,
        eWeapon_Slider,
        eWeapon_Bomb,
//        eWeapon_AccelOn,
//        eWeapon_AccelOff,
        eWeapon_Gravity,
    } Type;
   
    typedef enum EBulletSize{
        eSizeXS = 0,
        eSizeS,
        eSizeM,
        eSizeL,
        eSizeXL,
    } BulletSize;
    
    virtual void SetStartPoint(float x, float y) = 0;
    virtual void SetEndPoint(float x, float y ) = 0;
    
    virtual void SetType(int weaponType) = 0;
    
    virtual void Shot() = 0;
    virtual void Reset() = 0;
    virtual void WorldCreatedPre() = 0;
    virtual void WorldCreatedPost() = 0;
    virtual void CleanUp() = 0;
    
protected:
    cocos2d::Point  _startPoint;
    cocos2d::Point  _endPoint;
    DrawNode*       _drawNode;

};
#endif
