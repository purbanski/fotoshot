#include "ShotDef.h"
#include "Tools.h"
#include <sstream>
#include <cocos2d.h>
#include "VisibleRect.h"
#include "Config.h"
//----------------------------------------------------------
using namespace std;
//----------------------------------------------------------
string ShotDefs::GetJson()
{
    
    auto screenSize = cocos2d::Director::getInstance()->getWinSize();
//    float ratio = screenSize.width / 640.0f * 2.3f;
//    CCLog("%f %f %f %f", screenSize.width, screenSize.height, VisibleRect::center().x, VisibleRect::center().y );
    stringstream ret;
    int i = 0;
    
    ret << "[{ ";
    ret << "\"ver\":\""<< Config::JsonFormatVer;
    ret << "\",\"width\":\"" << screenSize.width;
    ret << "\",\"height\":\"" << screenSize.height;
    ret << "\",\"bubbleScale\":\"" << Tools::GetBubbleScale();
    ret << "\",\"shots\":[";
    for ( auto def = begin(); def != end(); def++ )
    {
        ret << " {\"time\":\""<< def->time <<"\",";
        ret << " \"xs\":\""<< def->xs << "\",";
        ret << " \"ys\":\""<< def->ys << "\",";
        ret << " \"xv\":\""<< def->xv << "\",";
        ret << " \"yv\":\""<< def->yv << "\"}";
        if ( ++i < size() )
            ret << ",";
        
//        {"time":"0.82001", "xs":"231", "ys":"280.0", "xv":"252", "yv":"280"},
    }
    
    ret << "]}]";
    return ret.str();
}
