#ifndef __FotoShot__Bomber__
#define __FotoShot__Bomber__
//------------------------------------------------------
#include "cocos2d.h"
#include "ShotDef.h"
#include <string>
#include <set>
#include <map>
#include "puBall.h"

#include "chipmunk.h"
#include "Weapon.h"
//------------------------------------------------------
class puBomb;
//------------------------------------------------------
class Bomber : public Weapon
{
public:
    typedef enum {
        eBombDiagonal = 1,
        eBombBall,
        eBombSpiral,
        eBombMedusa,
        eBombChaos,
        eBombSide,
        eBombSqueze,
    } BombType;
    
public:
    static Bomber* Create();
    ~Bomber();
    
    //----------------
    // Weapon
    virtual void SetStartPoint(float x, float y) override;
    virtual void SetEndPoint(float x, float y ) override;
    virtual void SetType(int weaponType) override;
    
    virtual void Shot() override;
    virtual void Reset() override;
    virtual void CleanUp() override;
    virtual void WorldCreatedPre() override;
    virtual void WorldCreatedPost() override;
   
    //    virtual void draw();
    virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4& transform, uint32_t flags);
    
    void Step( float time );
    std::string GetJson();
    
private:
    Bomber();
    void Init();
    void LabelClear();
    void ExplodeBombs();

    void SetCollisionHandler();
    void RemoveCollisionHandler();
    void ApplyForces();
    
    cpVect GetForce(cpVect ballPos, cpVect bombPos);
    
    static cpBool CollisionBegin(cpArbiter *arb, cpSpace *space, void *unused);
    
private:
    //-------------------------;
    float _forceScale;
    float _forceDirection;
    float _timeToExplosion;
    bool  _explosionActivated;

    typedef std::set<puBomb*> Bombs;
    Bombs   _bombs;
    
    typedef std::map<puBall*, cpVect> BallForceMap;
    BallForceMap _ballForceMap;
    
    std::stringstream _json;
    
    typedef std::vector<cpShape*> Shapes;
    Shapes _shapes;
    
    ShotDefs _shotDefs;
    unsigned int _totalShotCount;
    float _bulletRadius;
    cocos2d::LabelTTF* _label;
    BombType _bombType;
};
//------------------------------------------------------

#endif /* defined(__FotoShot__Bomber__) */
