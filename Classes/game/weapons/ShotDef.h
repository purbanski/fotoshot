#ifndef __Foto_Shot__ShotDef__
#define __Foto_Shot__ShotDef__
//------------------------------------------------------
#include <string>
#include <vector>
//------------------------------------------------------
class ShotDef
{
public:
    ShotDef() : xs(0), ys(0), xv(0), yv(0), time(-1) {}
    ShotDef( float axs, float ays, float axv, float ayv, float atime ):
    xs(axs), ys(ays), xv(axv), yv(ayv), time(atime) {}
    
    std::string GetJson();
    
    float xs;
    float ys;
    float xv;
    float yv;
    double time;
};
//------------------------------------------------------
class ShotDefs : public std::vector<ShotDef>
{
public:
    std::string GetJson();
};

#endif /* defined(__Foto_Shot__ShotDef__) */
