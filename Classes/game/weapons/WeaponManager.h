#ifndef __FotoShot__WeaponManager__
#define __FotoShot__WeaponManager__

#include "Weapon.h"
#include "DestroyScene.h"

class WeaponManager {
public:
    WeaponManager();
    
    void SetWeapon(Weapon::Type weapon);
    Weapon* GetWeapon();
    
    void Reset();
    void SetLayer(DestroySceneLayer *layer);
    
private:
    void UpdateWeapon(Weapon* weapon);
    void SetWeaponRestrited(Weapon::Type weapon);

    
private:
    Weapon* _weapon;
    DestroySceneLayer *_layer;
};
#endif /* defined(__FotoShot__WeaponManager__) */
