#include "Bomber.h"
#include "World.h"
#include "VisibleRect.h"
#include "Blocks.h"
#include "Tools.h"
#include "Config.h"
//#include "GTracker.h"

//-------------------------------------------------------------
Bomber* Bomber::Create()
{
    Bomber *node;
    node = new Bomber();
    if ( node )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//-------------------------------------------------------------
Bomber::Bomber()
{
    _explosionActivated  = false;
}
//-------------------------------------------------------------
void Bomber::Init()
{
    _drawNode = cocos2d::DrawNode::create();
    _drawNode->setPosition(cocos2d::Point(100, 100));
    addChild(_drawNode, 100);
    
    _startPoint.setPoint(0,0);
    _endPoint.setPoint(0,0);
    
    _totalShotCount = 0;

    _forceScale = 10;
    _forceDirection = 1;
//    _totalTime = 0;
    
    _bulletRadius = Tools::GetBubbleScale() * 8;

    _label = cocos2d::LabelTTF::create("", "ArialBold", 35);
    _label->setColor(cocos2d::Color3B(255,255,255));
    _label->setPosition(cocos2d::Point(VisibleRect::center().x, VisibleRect::top().y-30));
    addChild(_label, 110);
    
    SetType(Bomber::eBombDiagonal);
    
    SetCollisionHandler();
    
    schedule(schedule_selector(Bomber::Step));
}
//-------------------------------------------------------------

//-------------------------------------------------------------
Bomber::~Bomber()
{
}
//-------------------------------------------------------------
void Bomber::SetStartPoint(float x, float y)
{
    _explosionActivated = false;
    LabelClear();
    _startPoint.setPoint(-999999, -999999);
    _endPoint.setPoint(x,y);
}
//-------------------------------------------------------------
void Bomber::SetEndPoint(float x, float y)
{
    _explosionActivated = false;
    _endPoint.setPoint(x,y);
}
//-------------------------------------------------------------
void Bomber::Shot()
{
    puBomb *bomb;
    _startPoint = _endPoint;
    
    bomb = Blocks::Get()->AddBomb(_endPoint.x, _endPoint.y, _bulletRadius);
    _bombs.insert(bomb);

    _timeToExplosion = 3.0f;
    _explosionActivated = true;
}
//-------------------------------------------------------------
void Bomber::LabelClear() {
    _label->setString("");
}
//-------------------------------------------------------------
void Bomber::Reset()
{
//    _totalTime = 0;
    _shotDefs.clear();
    
    for ( auto shape : _shapes )
    {
        cpSpaceRemoveShape(World::GetSpace(), shape );
        cpShapeFree(shape);
    }
    _shapes.clear();
}
//-------------------------------------------------------------
void  Bomber::draw(cocos2d::Renderer *renderer, const cocos2d::Mat4& transform, uint32_t flags)
{
    DrawNode::draw(renderer, transform, flags);
    _drawNode->clear();
    
    if ( _startPoint.equals(_endPoint) )
        return;

    auto end = cocos2d::Point(-100,-100) + _endPoint;

    _drawNode->drawSolidCircle(end, _bulletRadius, 3.14, 50, cocos2d::Color4F(0,0.5,0.5,0.5));
                               
}
//----------------------------------------------------------------------------------
void Bomber::ExplodeBombs() {
    for ( auto bomb : _bombs ) {
        Blocks::Get()->ScheduleDeleteBlock(bomb);
    }
    
    _bombs.clear();
    ApplyForces();
}
//----------------------------------------------------------------------------------
void Bomber::ApplyForces() {
    for (auto it : _ballForceMap)
    {
        auto ball = it.first;
        ball->GetBody()->v = it.second;
//        log("Apply force; ball: %p force:(%f %f)", ball, it.second.x, it.second.y);
    }
    
    _ballForceMap.clear();
}
//----------------------------------------------------------------------------------
void Bomber::Step(float time)
{
    if (_explosionActivated && _timeToExplosion > 1 )
    {
        _timeToExplosion -= time;

        if (_timeToExplosion <= 1)
        {
            _timeToExplosion = 1;
            _label->setString("");
            _explosionActivated = false;
            ExplodeBombs();
        }
        else
        {
            stringstream ss;
            ss << "Bomb exlosion in: " << (int)round(_timeToExplosion) << " s";
            _label->setString(ss.str());
        }
    }
    
    for (auto bomb : _bombs)
    {
        bomb->DecBombVisibility();
    }
}
//----------------------------------------------------------------------------------
void Bomber::SetType(int weaponType)
{
    _bombType = (Bomber::BombType)weaponType;
    
    float base = 8.0f;
    float multiply = 2.0f;
    
    _bulletRadius = Tools::GetBubbleScale() * base * multiply * 10;
    _forceScale = 3;
    _forceDirection = 1;

    return;
    }
//----------------------------------------------------------------------------------
cpVect Bomber::GetForce(cpVect ballPos, cpVect bombPos)
{
    float tmp;
    cpVect f;
    float limit =80;
    auto dx = ballPos.x - bombPos.x;
    auto dy = ballPos.y - bombPos.y;
    static float mx = 0;
    
    switch (_bombType) {
        case eBombSpiral:
        {
            puPoint ballP;
            puPoint bombP;
            ballP.set(ballPos);
            bombP.set(bombPos);
            
            puPoint p = Tools::RotatePointAroundPoint( ballP, bombP, 30);
            dx = ballPos.x - p.x;
            dy = ballPos.y - p.y;
            
            
            tmp = _forceDirection * _forceScale * 3;
            
            f.x = tmp * dx ;
            f.y = tmp * dy ;
            return f;
        }
            break;// to sati
            
        case eBombMedusa:
            tmp = _forceDirection * _forceScale/100;
            
            f.x = tmp * dx * dx;
            f.y = tmp * dy * dy;
            if (ballPos.x-bombPos.x<0)
                f.x *= -1;

            if (ballPos.y-bombPos.y<0)
                f.y *= -1;
            
            return f;
            break;
            
        case eBombChaos:
        {
            int l = 300;
            dx = Tools::Rand(-l,l);
            dy = Tools::Rand(-l,l);
            tmp = _forceDirection * _forceScale;

            f.x = tmp * dx;
            f.y = tmp * dy;
            return f;
        }
            break;
            
        case eBombSide:
            limit = 400;
            tmp = _forceDirection * _forceScale;
            tmp *= 3000;
            f.x = tmp / (ballPos.x - bombPos.x);
            f.y = 0;
            if (f.x> limit)
                f.x=limit;
            
            return f;


        case eBombDiagonal:
            tmp = _forceDirection * _forceScale * 1.75;
            f.y = tmp * (ballPos.x - bombPos.x);
            f.x = tmp * (ballPos.y - bombPos.y);
            return f;

            
        case eBombSqueze:
            tmp = -1.35 * _forceScale;
            f.x = tmp * (ballPos.x - bombPos.x);
            f.y = tmp * (ballPos.y - bombPos.y);
            return f;
            
        case eBombBall:
        {
            float s = 120;
            dx = sin( Tools::DegToRad(dx)) * s;
            dy = sin( Tools::DegToRad(dy)) * s;

            tmp = _forceDirection * _forceScale;
            f.x = tmp * dx;
            f.y = tmp * dy;
            return f;
        }
            break;// to satifsfy compiler warnings
            
//            tmp = _forceDirection * _forceScale;
//            tmp *= 2000;
//            f.x = tmp / (ballPos.x - bombPos.x);
//            f.y = tmp / (ballPos.y - bombPos.y);
//            if (f.x> limit)
//                f.x=limit;
//            if (f.y> limit)
//                f.y=limit;

//            tmp = _forceDirection * _forceScale;
//            f.x = tmp * (ballPos.x - bombPos.x);
//            f.y = tmp * (ballPos.y - bombPos.y);
//            f.y = 0;
//            return f;
            
        default:
            tmp = _forceDirection * _forceScale;
            f.x = tmp * (ballPos.x - bombPos.x);
            f.y = tmp * (ballPos.y - bombPos.y);
            f.y = 0;
            return f;
    }
    
}
//----------------------------------------------------------------------------------
void Bomber::CleanUp() {
    RemoveCollisionHandler();
    unscheduleUpdate();
}
//----------------------------------------------------------------------------------
string Bomber::GetJson()
{
    return _shotDefs.GetJson();
}
//----------------------------------------------------------------------------------
void Bomber::SetCollisionHandler() {
    cpSpaceAddCollisionHandler(World::GetSpace(),
                               CollisionType::eCollision_Bomb,
                               CollisionType::eCollision_Ball,
                               Bomber::CollisionBegin,
                               NULL, NULL, NULL, this);
}
//----------------------------------------------------------------------------------
void Bomber::RemoveCollisionHandler() {
    cpSpaceRemoveCollisionHandler(World::GetSpace(),
                                  CollisionType::eCollision_Bomb,
                                  CollisionType::eCollision_Ball);
}
//----------------------------------------------------------------------------------
cpBool Bomber::CollisionBegin(cpArbiter *arb, cpSpace *space, void *bomberTool)
{
    puBomb *bomb;
    puBall *ball;
    Bomber *bomber;
    
    cpShape *shapeBomb, *shapeBall;
    cpArbiterGetShapes(arb, &shapeBomb, &shapeBall);
    
    bomb = (puBomb*) shapeBomb->data;
    ball = (puBall*) shapeBall->data;
    bomber = (Bomber*) bomberTool;
    
    if (!bomb || !ball || !bomber)
        return cpFalse;
    
    cpVect bombPos = shapeBomb->body->p;
    cpVect ballPos = shapeBall->body->p;
    cpVect f;
    
    f = bomber->GetForce(ballPos,bombPos);
    if ( bomber->_ballForceMap.find(ball) != bomber->_ballForceMap.end() )
        f = f + bomber->_ballForceMap[ball];
    
//    log("collide bal:%p set force:%f %f", ball, f.x, f.y);
    
    bomber->_ballForceMap[ball] = f;
    return cpFalse;
}
//----------------------------------------------------------------------------------
void Bomber::WorldCreatedPre()
{
    RemoveCollisionHandler();
}
//----------------------------------------------------------------------------------
void Bomber::WorldCreatedPost()
{
    SetCollisionHandler();
}
