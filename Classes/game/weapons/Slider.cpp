#include "Slider.h"
#include "Blocks.h"
#include "World.h"
//#include "GLES-Render.h"
#include "VisibleRect.h"
#include "Blocks.h"
#include "Tools.h"
//#include "GTracker.h"

//-------------------------------------------------------------
Slider* Slider::Create()
{
    Slider *node;
    node = new Slider();
    if ( node )
    {
        node->autorelease();
        return node;
    }
    return NULL;
}
//-------------------------------------------------------------
Slider::Slider()
{
    _drawNode = cocos2d::DrawNode::create();
    _drawNode->setPosition(cocos2d::Point(100, 100));
    addChild(_drawNode, 100);
    
    _startPoint.set(0,0);
    _endPoint.set(0,0);
    
    _totalShotCount = 0;
    _totalTime = 0;
    SetType(eSliderM);
    
    schedule(schedule_selector(Slider::Step));
}
//-------------------------------------------------------------
Slider::~Slider()
{
}
//-------------------------------------------------------------
void Slider::SetStartPoint(float x, float y)
{
    _startPoint.setPoint(x,y);
}
//-------------------------------------------------------------
void Slider::SetEndPoint(float x, float y)
{
    _endPoint.setPoint(x,y);
}
//-------------------------------------------------------------
void Slider::Shot()
{
    _totalShotCount++;
    puBlock *block;
    
    float xVel, yVel, accel;
    float scale;
    
    scale = Tools::GetBubbleScale();
    
    accel = 4;
    xVel = accel * ( _startPoint.x - _endPoint.x );
    yVel = accel * ( _startPoint.y - _endPoint.y );
    
    block = Blocks::Get()->AddSlider(_sliderType);
    cpBody *body = block->GetShape()->body;
    
    float angle;
    float dx =_startPoint.x-_endPoint.x;
    float dy = _startPoint.y-_endPoint.y;
    
    angle = -atan( dx/dy );
    angle = Tools::RadToDeg(angle);
    
    if ( angle != angle /*NaN*/)
    {
        angle = 0;
    }
    if ( dy > 0 )
        angle -= 180.0f;
    
    cocos2d::log("angle %f", angle);

    cpBodySetAngle(body, Tools::DegToRad(angle));
    block->SetInitVelocity(xVel, yVel);
    body->p = cpv(_endPoint.x, _endPoint.y);
    
    _startPoint = _endPoint;
    
    if (!_shotDefs.size())
        _timeDelta = _totalTime - 1.0;
    
    ShotDef shotDef( _endPoint.x, _endPoint.y, xVel, yVel, _totalTime - _timeDelta );
    _shotDefs.push_back(shotDef);
    //    _shapes.push_back(shape);
}
//-------------------------------------------------------------
void Slider::CleanUp() {
    
}
//-------------------------------------------------------------
void Slider::Reset()
{
    _totalTime = 0;
    _shotDefs.clear();
    
    _shapes.clear();
}
//-------------------------------------------------------------
//void Slider::draw()
void  Slider::draw(cocos2d::Renderer *renderer, const cocos2d::Mat4& transform, uint32_t flags)
{
    if ( _startPoint.equals(_endPoint) )
    {
        _drawNode->clear();
        return;
    }
    
    cocos2d::Point start;
    cocos2d::Point end;
    
    _drawNode->clear();
    
    start = cocos2d::Point(-100,-100) + _startPoint;
    end = cocos2d::Point(-100,-100) + _endPoint;
    _drawNode->drawSegment(start, end, 7.5, cocos2d::Color4F(0,1,0,0.5f));
    
    start = cocos2d::Point(-100,-100) + _startPoint;
    end = -end  + start +start ;
    _drawNode->drawSegment(start, end, 1.5f, cocos2d::Color4F(0,1,0,0.8f));
    
}
//----------------------------------------------------------------------------------
void Slider::Step(float time)
{
    _totalTime += time;
    //    CCLog("time:%f", _totalTime);
}
//----------------------------------------------------------------------------------
string Slider::GetJson()
{
    return _shotDefs.GetJson();
}
//----------------------------------------------------------------------------------
void Slider::SetType(int weaponType)
{
    switch (weaponType) {
        case eSliderS:
            _sliderType = puSlider::eSliderS;
            break;

        case eSliderM:
            _sliderType = puSlider::eSliderM;
            break;

        case eSliderL:
            _sliderType = puSlider::eSliderL;
            break;

        case eSliderReversM:
            _sliderType = puSlider::eSliderReversM;
            break;

        case eSliderReversL:
            _sliderType = puSlider::eSliderReversL;
            break;
            
        case eSliderLine :
            _sliderType = puSlider::eSliderLine;
            break;
            
        case eSliderDotLines :
            _sliderType = puSlider::eSliderDotLines;
            break;
            
        default:
            break;
    }
}
//----------------------------------------------------------------------------------
void Slider::WorldCreatedPre()
{
    
}
//----------------------------------------------------------------------------------
void Slider::WorldCreatedPost()
{
    
}

