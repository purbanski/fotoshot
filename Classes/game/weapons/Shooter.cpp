#include "Shooter.h"
#include "Blocks.h"
#include "World.h"
//#include "GLES-Render.h"
#include "VisibleRect.h"
#include "Blocks.h"
#include "Tools.h"
//#include "GTracker.h"

//-------------------------------------------------------------
Shooter* Shooter::Create()
{
    Shooter *node;
    node = new Shooter();
    if ( node )
    {
        node->autorelease();
        return node;
    }
    return NULL;
}
//-------------------------------------------------------------
Shooter::Shooter()
{
    _drawNode = cocos2d::DrawNode::create();
    _drawNode->setPosition(cocos2d::Point(100, 100));
    addChild(_drawNode, 100);

    _startPoint.setPoint(0,0);
    _endPoint.setPoint(0,0);
    
    _totalShotCount = 0;
    _totalTime = 0;
    
    SetType(eSizeS);

    schedule(schedule_selector(Shooter::Step));
}
//-------------------------------------------------------------
Shooter::~Shooter()
{
}
//-------------------------------------------------------------
void Shooter::SetStartPoint(float x, float y)
{
    _startPoint.setPoint(x,y);
}
//-------------------------------------------------------------
void Shooter::SetEndPoint(float x, float y)
{
    _endPoint.setPoint(x,y);
}
//-------------------------------------------------------------
void Shooter::Shot()
{
    _totalShotCount++;
//    GTracker::Get()->Event_Shot(_totalShotCount);

    puBlock *block;
    cpShape *shape;
    
    float xVel, yVel, accel;

    
    accel = 4;
    xVel = accel * ( _startPoint.x - _endPoint.x );
    yVel = accel * ( _startPoint.y - _endPoint.y );

    block = Blocks::Get()->AddBullet(_endPoint.x, _endPoint.y, _bulletRadius);
    shape = (cpShape*) block->GetShape();
    block->SetInitVelocity(xVel, yVel);
    
    _startPoint = _endPoint;
    
    if (!_shotDefs.size())
        _timeDelta = _totalTime - 1.0;
    
    ShotDef shotDef( _endPoint.x, _endPoint.y, xVel, yVel, _totalTime - _timeDelta );
    _shotDefs.push_back(shotDef);
    //    _shapes.push_back(shape);
}
//-------------------------------------------------------------
void Shooter::Reset()
{
    _totalTime = 0;
    _shotDefs.clear();
    
    for ( auto shape : _shapes )
    {
        cpSpaceRemoveShape(World::GetSpace(), shape );
        cpShapeFree(shape);
    }
    _shapes.clear();
}
//-------------------------------------------------------------
//void Shooter::draw()
void  Shooter::draw(cocos2d::Renderer *renderer, const cocos2d::Mat4& transform, uint32_t flags)
{
    if ( _startPoint.equals(_endPoint) )
    {
        _drawNode->clear();
        return;
    }
    
    cocos2d::Point start;
    cocos2d::Point end;
    
    _drawNode->clear();

    start = cocos2d::Point(-100,-100) + _startPoint;
    end = cocos2d::Point(-100,-100) + _endPoint;
    _drawNode->drawSegment(start, end, 7.5, cocos2d::Color4F(0,1,0,0.5f));

    start = cocos2d::Point(-100,-100) + _startPoint;
    end = -end  + start +start ;
    _drawNode->drawSegment(start, end, 1.5f, cocos2d::Color4F(0,1,0,0.8f));
    
}
//----------------------------------------------------------------------------------
void Shooter::CleanUp() {
    
}
//----------------------------------------------------------------------------------
void Shooter::Step(float time)
{
    _totalTime += time;
//    CCLog("time:%f", _totalTime);
}
//----------------------------------------------------------------------------------
void Shooter::SetType(int size)
{
    float base = 8.0f;
    float multiply = 2.0f;
    
    switch ((Size)size) {
        case eSizeXXS:
            _bulletRadius = Tools::GetBubbleScale() * base * multiply * 1;
            break;

        case eSizeXS:
            _bulletRadius = Tools::GetBubbleScale() * base * multiply * 2;
            break;
            
        case eSizeS:
            _bulletRadius = Tools::GetBubbleScale() * base * multiply * 3;
            break;
            
        case eSizeM:
            _bulletRadius = Tools::GetBubbleScale() * base * multiply * 4;
            break;
            
        case eSizeL:
            _bulletRadius = Tools::GetBubbleScale() * base * multiply * 5;
            break;
            
        case eSizeXL:
            _bulletRadius = Tools::GetBubbleScale() * base * multiply * 6;
            break;
            
        default:
            break;
    }
}
//----------------------------------------------------------------------------------
string Shooter::GetJson()
{
    return _shotDefs.GetJson();
}
//----------------------------------------------------------------------------------
void Shooter::WorldCreatedPre()
{
    
}
//----------------------------------------------------------------------------------
void Shooter::WorldCreatedPost()
{
    
}