#ifndef __TestCpp__Shooter_H__
#define __TestCpp__Shooter_H__
//------------------------------------------------------
#include "cocos2d.h"
#include "ShotDef.h"
#include <string>
#include "chipmunk.h"
#include "Weapon.h"
//------------------------------------------------------
class Shooter : public Weapon
{
public:
    typedef enum {
        eSizeXXS = 1,
        eSizeXS,
        eSizeS,
        eSizeM,
        eSizeL,
        eSizeXL
    } Size;
    
public:
    static Shooter* Create();
    ~Shooter();
    
    //----------------
    // Weapon
    virtual void SetStartPoint(float x, float y) override;
    virtual void SetEndPoint(float x, float y ) override;
    virtual void SetType(int weaponType) override;
    
    virtual void Shot() override;
    virtual void Reset() override;
    virtual void CleanUp() override;
    virtual void WorldCreatedPre() override;
    virtual void WorldCreatedPost() override;

    
    virtual void draw(cocos2d::Renderer *renderer,
                      const cocos2d::Mat4& transform,
                      uint32_t flags);
    
    void Step( float time );
    std::string GetJson();
    
private:
    Shooter();

private:
    double  _totalTime;
    float   _timeDelta;
    std::stringstream _json;
    
    typedef std::vector<cpShape*> Shapes;
//
    Shapes _shapes;
    
    ShotDefs _shotDefs;
    unsigned int _totalShotCount;
    float _bulletRadius;
};
//------------------------------------------------------
#endif
