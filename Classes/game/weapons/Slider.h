#ifndef __FotoShot__Slider__
#define __FotoShot__Slider__
//------------------------------------------------------
#include "cocos2d.h"
#include "ShotDef.h"
#include <string>
#include "chipmunk.h"
#include "Weapon.h"
#include "puSlider.h"
//------------------------------------------------------
class SliderSettings {
public:
    float dx;
    float dy;
    
};
//------------------------------------------------------
class Slider : public Weapon
{
public:
    typedef enum {
        eSliderS = 1,
        eSliderM,
        eSliderL,
        eSliderReversM,
        eSliderReversL,
        eSliderLine,
        eSliderDotLines,
    } SliderType;
    
    static Slider* Create();
    ~Slider();
    
    //----------------
    // Weapon
    virtual void SetStartPoint(float x, float y) override;
    virtual void SetEndPoint(float x, float y ) override;
    virtual void SetType(int weaponType) override;
    
    virtual void Shot() override;
    virtual void Reset() override;
    virtual void CleanUp() override;
    virtual void WorldCreatedPre() override;
    virtual void WorldCreatedPost() override;

    //    virtual void draw();
    virtual void draw(cocos2d::Renderer *renderer,
                      const cocos2d::Mat4& transform,
                      uint32_t flags);
    
    void Step( float time );
    std::string GetJson();
    
private:
    Slider();
    
private:
    double  _totalTime;
    float   _timeDelta;
    std::stringstream _json;
    
    typedef std::vector<cpShape*> Shapes;
    Shapes _shapes;
    
    ShotDefs _shotDefs;
    unsigned int _totalShotCount;
    
    puSlider::SliderType _sliderType;
};
//------------------------------------------------------


#endif
