#include "WeaponManager.h"
#include "Slider.h"
#include "Shooter.h"
#include "Bomber.h"
#include "Brain.h"
#include "World.h"
#include "Blocks.h"
#include "puBlocks.h"
//--------------------------------------------------------------------
WeaponManager::WeaponManager()
{
    _weapon = NULL;
    _layer = NULL;
}
//--------------------------------------------------------------------
void WeaponManager::SetLayer(DestroySceneLayer *layer) {
    _layer = layer;
}
//--------------------------------------------------------------------
void WeaponManager::UpdateWeapon(Weapon* weapon) {
    if (_weapon)
    {
        _weapon->Reset();
        _weapon->CleanUp();
        _weapon->removeFromParent();
        _weapon = NULL;
    }
    _weapon = weapon;
    _layer->UpdateWeapon();
}
//--------------------------------------------------------------------
Weapon* WeaponManager::GetWeapon() {
    return _weapon;
}
//--------------------------------------------------------------------
void WeaponManager::SetWeapon(Weapon::Type weapon)
{
    if ( !Brain::Get()->IsWeaponPackEnabled() )
    {
        SetWeaponRestrited(weapon);
        return;
    }
    
    switch (weapon)
    {
        case Weapon::Type::eWeapon_Slider :
            UpdateWeapon(Slider::Create());
            break;
            
        case Weapon::Type::eWeapon_Shooter :
            UpdateWeapon(Shooter::Create());
            break;
            
        case Weapon::Type::eWeapon_Gravity :
            Brain::Get()->ToggleGravity();
            
            break;
            
//        case Weapon::Type::eWeapon_AccelOn :
//            _layer->AccelStart();
//            Blocks::Get()->AddBorder();
//            break;

        case Weapon::Type::eWeapon_Bomb :
        {
            //---------------
            // Checking if bomber is not already set
            // cycle remove->add on bomber causes
            // removal of colision handler
            Bomber *b = dynamic_cast<Bomber*>( _weapon);
            if (!b)
                UpdateWeapon(Bomber::Create());
            break;
        }
            
        default:
            UpdateWeapon(Shooter::Create());
            break;
    }
}
//--------------------------------------------------------------------
void WeaponManager::SetWeaponRestrited(Weapon::Type weapon)
{
    switch (weapon)
    {
        case Weapon::Type::eWeapon_Gravity :
            Brain::Get()->ToggleGravity();           
            break;

        case Weapon::Type::eWeapon_Bomb :
        {
            Bomber *b = dynamic_cast<Bomber*>( _weapon);
            if (!b)
                UpdateWeapon(Bomber::Create());
            break;
        }
            
        case Weapon::Type::eWeapon_Shooter :
        case Weapon::Type::eWeapon_Slider :
        default:
            UpdateWeapon(Shooter::Create());
            break;
    }
}
//--------------------------------------------------------------------
void WeaponManager::Reset() {
    if (_weapon)
        _weapon->Reset();
}