#ifndef __FotoShot__Status__
#define __FotoShot__Status__
//------------------------------------------------------------------------------
#include <string>
#include <cocos2d.h>
//------------------------------------------------------------------------------
class Status {

public:
    Status();
    ~Status();
    

public:
    bool IsWeaponExtEnabled();
    void SetWeaponExtEnabled( bool enabled );
    
private:
    void InitStatusFile();
    void Load();
    void Save();
    void Reset();
    
    std::string GetStatusFilename();
    
private:
    cocos2d::ValueMap _values;
   
};
//------------------------------------------------------------------------------

#endif
