#ifndef __FotoShot__Brain__
#define __FotoShot__Brain__
//---------------------------------------------------------
#include "cocos2d.h"
#include "Shooter.h"
#include "DestroyScene.h"
#include "FotoShotTypes.h"
#include "Weapon.h"
#include "WeaponManager.h"
#include "BallCount.h"
#include "RunningSettings.h"
#include "Status.h"
//---------------------------------------------------------
class Brain {

public:
    typedef enum  {
        eLayerBackground = 20,
        eLayerWorld     = 100,
        eLayerWeapon    = 220,
        eLayerMenu      = 150,
        eLayerProcessing = 200,
        eLayerTest       = 230,
    } ZOrder;
    
    static Brain* Get();
    static void Destroy();
    
private:
    Brain();
    
public:
    ~Brain();
    
    bool IsWeaponPackEnabled();
    void WeaponPackEnable();
    void WeaponPackDisable();
    
    void StartUp();
    
    void Restart();
    void SetWeapon( Weapon::Type weapon );
    Weapon *GetWeapon();
    
    void LoadColors( cocos2d::Size size, const Colors &colors );
    void LoadColors( int w, int h, intptr_t colors[] );
    
    void TakePhoto();
    void DefaultPhotoSaved();

    void ToggleGravity();
    void ToggleBorder();
    void ToggleAccelator();
    bool IsBorderEnabled();
    
    unsigned int GetBallCount();
    void IncBallCount();
    void DecBallCount();
    
    RunningSettings GetRunningSettings();

    void ApplyWorldSettings();
    
    void ShowProcessing();
    void HideProcessing();
    
private:
    void UpdateImage();
    
private:
    static Brain* _sInstance;
    
    WeaponManager       _weapon;
    DestroySceneLayer*  _sceneLayer;
    BallCount           _ballCount;
    RunningSettings     _settings;
    Status              _status;
};
//---------------------------------------------------------
#endif
