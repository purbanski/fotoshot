#ifndef __FotoShot__BallCount__
#define __FotoShot__BallCount__
//------------------------------------------------------
#include <stdio.h>
//------------------------------------------------------
class BallCount {
public:
    BallCount();
    ~BallCount();
    
    unsigned int GetCount();
    void IncCount();
    void DecCount();
    
private:
    unsigned int _count;
    
};
//------------------------------------------------------
#endif

