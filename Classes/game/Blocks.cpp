#include "Blocks.h"
#include "World.h"
#include "VisibleRect.h"
#include "puBall.h"
//------------------------------------------------------------------
Blocks* Blocks::_sInstance = NULL;
//------------------------------------------------------------------
Blocks* Blocks::Get()
{
    if ( ! _sInstance )
    {
        _sInstance = new Blocks();
    
    }
    return _sInstance;
}
//------------------------------------------------------------------
Blocks::Blocks()
{
    _blocksCreated.clear();
    _border = NULL;
}
//------------------------------------------------------------------
Blocks::~Blocks()
{
}
//------------------------------------------------------------------
puSlider* Blocks::AddSlider(puSlider::SliderType sliderType)
{
    puSlider *slider;
    slider = new puSlider(sliderType);
//    slider->SetInitVelocity(xvel, yvel);
    _blocksCreated.insert(slider);
    
    return slider;
}
//------------------------------------------------------------------
puBall* Blocks::AddBall( float x, float y )
{
    return AddBall(x, y, 4, cocos2d::Color4F( 0,0,1,0.5f));
}
//------------------------------------------------------------------
puBall* Blocks::AddBall( float x, float y, float radius, cocos2d::Color4F color )
{
 	puBall *ball;
    ball = new puBall(radius, x, y);
    ball->SetColor(color);
    
    _blocksCreated.insert(ball);
    return ball;
}
//------------------------------------------------------------------
puBullet* Blocks::AddBullet(float x, float y, float radius )
{
    puBullet *bullet;
    bullet = new puBullet(radius, x, y);
    bullet->SetColor(cocos2d::Color4F(1,0,0,1));
    
    _blocksCreated.insert(bullet);
    return bullet;
}
//------------------------------------------------------------------
puBorder* Blocks::AddBorder()
{
    if (_border)
        return _border;
    
    _border = new puBorder();
    
//    _blocksCreated.insert(border);
    return _border;
}
//------------------------------------------------------------------
void Blocks::RemoveBorder()
{
    if ( ! _border)
        return;
    delete _border;
//    ScheduleDeleteBlock(_border);
    _border = NULL;
}
//------------------------------------------------------------------
bool Blocks::IsBorderEnabled()
{
    if (!_border)
        return false;
    
    return true;
}
//------------------------------------------------------------------
puBomb* Blocks::AddBomb(float r)
{
    puBomb *bomb;
    bomb = new puBomb(r);
    
    _blocksCreated.insert(bomb);
    return bomb;
}
//------------------------------------------------------------------
puBomb* Blocks::AddBomb(float x, float y, float r)
{
    puBomb *bomb = AddBomb(r);
    bomb->GetBody()->p=cpv(x,y);
    
    return bomb;
}
//------------------------------------------------------------------
void Blocks::CleanUp()
{
    for (BlocksSet::iterator it = _blocksCreated.begin(); it != _blocksCreated.end(); it++ )
    {
        puBlock *block = *it;
        delete block;
    }
    _blocksCreated.clear();
}
//------------------------------------------------------------------
void Blocks::CleanUpBullets()
{
    BlocksSet bullets;
    cocos2d::log("Blocks Created Size: %lu", (unsigned long)_blocksCreated.size());
    for (BlocksSet::iterator it = _blocksCreated.begin(); it != _blocksCreated.end(); it++ )
    {
        puBlock *block = *it;
    
        if (block->GetBlockType() == eBlock_Bullet ||
            block->GetBlockType() == eBlock_Slider )
            bullets.insert(block);
    }

    cocos2d::log("Bullets Size: %lu", (unsigned long)bullets.size());
    for (auto it = bullets.begin(); it != bullets.end(); it++ )
    {
        puBlock *block = *it;
        
        _blocksCreated.erase(block);
        delete block;

    }
    cocos2d::log("Blocks Created Size: %lu", (unsigned long)_blocksCreated.size());
    bullets.clear();
}
//------------------------------------------------------------------
void Blocks::ScheduleDeleteBlock(puBlock *block)
{
    _blocksToDelete.insert(block);
}
//------------------------------------------------------------------
void Blocks::Update()
{
    for ( auto block : _blocksToDelete )
    {
        _blocksCreated.erase(block);
        delete block;
    }
    _blocksToDelete.clear();
}
//------------------------------------------------------------------
