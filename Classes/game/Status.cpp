#include "Status.h"
#include "Config.h"
#include "cocos2d.h"
#include <string>
//----------------------------------------------------------------
using namespace std;
//----------------------------------------------------------------
Status::Status()
{
    string fullpath = GetStatusFilename();
    fullpath = cocos2d::FileUtils::getInstance()->getWritablePath();
    fullpath.append(Config::statusFilename);
    
    cocos2d::log("full path: %s", fullpath.c_str());
    
    if (! cocos2d::FileUtils::getInstance()->isFileExist(fullpath) )
        InitStatusFile();
    else
        Load();
}
//----------------------------------------------------------------
Status::~Status()
{
    
}
//----------------------------------------------------------------
std::string Status::GetStatusFilename()
{
    string fullpath;
    fullpath = cocos2d::FileUtils::getInstance()->getWritablePath();
    fullpath.append(Config::statusFilename);
    return fullpath;
}
//----------------------------------------------------------------
void Status::InitStatusFile()
{
    Reset();
    Save();
}
//----------------------------------------------------------------
void Status::Load()
{
    _values = cocos2d::FileUtils::getInstance()->getValueMapFromFile(GetStatusFilename());
 
    cocos2d::log("przurb billing: %s ", _values["TestString"].asString().c_str());
    cocos2d::log("przurb billing: %i", _values["WeaponExtensionEnabled"].asBool());
}
//----------------------------------------------------------------
void Status::Save()
{
    cocos2d::FileUtils::getInstance()->writeToFile(_values, GetStatusFilename());
}
//----------------------------------------------------------------
void Status::Reset()
{
    _values["WeaponExtensionEnabled"] = cocos2d::Value(false);
    _values["TestString"] = cocos2d::Value("to jest test");
}
//----------------------------------------------------------------
bool Status::IsWeaponExtEnabled()
{
    return _values["WeaponExtensionEnabled"].asBool();
}
//----------------------------------------------------------------
void Status::SetWeaponExtEnabled(bool enabled)
{
    _values["WeaponExtensionEnabled"] = cocos2d::Value(enabled);
    Save();
}

