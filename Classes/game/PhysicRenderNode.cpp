#include "PhysicRenderNode.h"
#include "chipmunk.h"

#include "base/ccTypes.h"
#include "math/CCGeometry.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <string.h>

#include "puBall.h"

//---------------------------------------------------------------
static cocos2d::Color4F ColorForBody(cpBody *body)
{
    if (cpBodyIsRogue(body) || cpBodyIsSleeping(body))
    {
        return cocos2d::Color4F(0.5f, 0.5f, 0.5f ,0.5f);
    }
    else if (body->CP_PRIVATE(node).idleTime > body->CP_PRIVATE(space)->sleepTimeThreshold)
    {
        return cocos2d::Color4F(0.33f, 0.33f, 0.33f, 0.5f);
    }
    else
    {
        return cocos2d::Color4F(1.0f, 0.0f, 0.0f, 0.5f);
    }
}
//---------------------------------------------------------------
static cocos2d::Color4F ColorForShape(cpShape *shape)
{
    puBlock *block;
    block = (puBlock*) shape->data;
    
    if ( !block )
        return cocos2d::Color4F( 0, 1, 0, 0.5f );
    
    return block->GetColor();
}
//---------------------------------------------------------------
static cocos2d::Vec2 cpVert2Point(const cpVect &vert)
{
    return cocos2d::Vec2(vert.x, vert.y);
}
//---------------------------------------------------------------
static cocos2d::Vec2* cpVertArray2ccpArrayN(const cpVect* cpVertArray, unsigned int count)
{
    if (count == 0) return nullptr;
    cocos2d::Vec2* pPoints = new (std::nothrow) cocos2d::Vec2[count];
    
    for (unsigned int i = 0; i < count; ++i)
    {
        pPoints[i].x = cpVertArray[i].x;
        pPoints[i].y = cpVertArray[i].y;
    }
    return pPoints;
}
//---------------------------------------------------------------
static void DrawShape(cpShape *shape, cocos2d::DrawNode *renderer)
{
    cpBody* body = shape->body;

    puBlock* block;
    block = (puBlock*)shape->data;
    
    if (block->GetBlockType() == eBlock_Bomb)
        return;
    
    //	Color4F color = ColorForBody(body);
    cocos2d::Color4F color = ColorForShape(shape);
    
    switch (shape->CP_PRIVATE(klass)->type)
    {
        case CP_CIRCLE_SHAPE:
        {
            cpCircleShape *circle = (cpCircleShape *)shape;
            cpVect center = circle->tc;
            cpFloat radius = circle->r;
            renderer->drawDot(cpVert2Point(center), cpfmax(radius, 1.0), color);
            renderer->drawSegment(cpVert2Point(center), cpVert2Point(cpvadd(center, cpvmult(body->rot, radius))), 1.0, color);
        }
        break;
            
        case CP_SEGMENT_SHAPE:
        {
            cpSegmentShape *seg = (cpSegmentShape *)shape;
            renderer->drawSegment(cpVert2Point(seg->ta), cpVert2Point(seg->tb), cpfmax(seg->r, 2.0), color);
        }
        break;
            
        case CP_POLY_SHAPE:
        {
            cpPolyShape *poly = (cpPolyShape *)shape;
            cocos2d::Color4F line = color;
            line.a = cpflerp(color.a, 1.0, 0.5);
            cocos2d::Vec2* pPoints = cpVertArray2ccpArrayN(poly->tVerts, poly->numVerts);
            renderer->drawPolygon(pPoints, poly->numVerts, color, 1.0, line);
            CC_SAFE_DELETE_ARRAY(pPoints);
        }
        break;
            
        default:
            cpAssertHard(false, "Bad assertion in DrawShape()");
    }
}

//---------------------------------------------------------------
static void DrawBomb(cpShape *shape, cocos2d::DrawNode *renderer)
{
    cpBody* body = shape->body;
    
    puBlock* block;
    block = (puBlock*)shape->data;
    
    if (block->GetBlockType() != eBlock_Bomb)
        return;
    
    //	Color4F color = ColorForBody(body);
    cocos2d::Color4F color = ColorForShape(shape);
    
            cpCircleShape *circle = (cpCircleShape *)shape;
            cpVect center = circle->tc;
            cpFloat radius = circle->r;
            renderer->drawDot(cpVert2Point(center), cpfmax(radius, 1.0), color);
            renderer->drawSegment(cpVert2Point(center), cpVert2Point(cpvadd(center, cpvmult(body->rot, radius))), 1.0, color);
  }
//---------------------------------------------------------------
static cocos2d::Color4F CONSTRAINT_COLOR(0, 1, 0, 0.5);
//---------------------------------------------------------------
static void DrawConstraint(cpConstraint *constraint, cocos2d::DrawNode *renderer)
{
    cpBody *body_a = constraint->a;
    cpBody *body_b = constraint->b;
    
    const cpConstraintClass *klass = constraint->CP_PRIVATE(klass);
    if (klass == cpPinJointGetClass())
    {
        cpPinJoint *joint = (cpPinJoint *)constraint;
        
        cpVect a = cpBodyLocal2World(body_a, joint->anchr1);
        cpVect b = cpBodyLocal2World(body_b, joint->anchr2);
        
        renderer->drawDot(cpVert2Point(a), 3.0, CONSTRAINT_COLOR);
        renderer->drawDot(cpVert2Point(b), 3.0, CONSTRAINT_COLOR);
        renderer->drawSegment(cpVert2Point(a), cpVert2Point(b), 1.0, CONSTRAINT_COLOR);
    }
    else if (klass == cpSlideJointGetClass())
    {
        cpSlideJoint *joint = (cpSlideJoint *)constraint;
        
        cpVect a = cpBodyLocal2World(body_a, joint->anchr1);
        cpVect b = cpBodyLocal2World(body_b, joint->anchr2);
        
        renderer->drawDot(cpVert2Point(a), 3.0, CONSTRAINT_COLOR);
        renderer->drawDot(cpVert2Point(b), 3.0, CONSTRAINT_COLOR);
        renderer->drawSegment(cpVert2Point(a), cpVert2Point(b), 1.0, CONSTRAINT_COLOR);
    }
    else if (klass == cpPivotJointGetClass())
    {
        cpPivotJoint *joint = (cpPivotJoint *)constraint;
        
        cpVect a = cpBodyLocal2World(body_a, joint->anchr1);
        cpVect b = cpBodyLocal2World(body_b, joint->anchr2);
        
        renderer->drawDot(cpVert2Point(a), 3.0, CONSTRAINT_COLOR);
        renderer->drawDot(cpVert2Point(b), 3.0, CONSTRAINT_COLOR);
    }
    else if (klass == cpGrooveJointGetClass())
    {
        cpGrooveJoint *joint = (cpGrooveJoint *)constraint;
        
        cpVect a = cpBodyLocal2World(body_a, joint->grv_a);
        cpVect b = cpBodyLocal2World(body_a, joint->grv_b);
        cpVect c = cpBodyLocal2World(body_b, joint->anchr2);
        
        renderer->drawDot(cpVert2Point(c), 3.0, CONSTRAINT_COLOR);
        renderer->drawSegment(cpVert2Point(a), cpVert2Point(b), 1.0, CONSTRAINT_COLOR);
    }
    else if (klass == cpDampedSpringGetClass())
    {
        // TODO: uninplemented
    }
    else
    {
        //		printf("Cannot draw constraint\n");
    }
}

//---------------------------------------------------------------
// Physic Render Node
//---------------------------------------------------------------
PhysicRenderNode::PhysicRenderNode(cpSpace* space)
{
    _space = space;
}
//---------------------------------------------------------------
PhysicRenderNode* PhysicRenderNode::create(cpSpace *space)
{
    PhysicRenderNode *node = new (std::nothrow) PhysicRenderNode(space);
    if (node && node->init())
    {
        node->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(node);
    }
    
    return node;
}
//---------------------------------------------------------------
bool PhysicRenderNode::init()
{
    if ( !Node::init() )
        return false;
    
    _renderBlocks = PhysicRenderBlocksNode::create(_space);
    _renderBombs = PhysicRenderBombsNode::create(_space);

    addChild(_renderBombs, 100);
    addChild(_renderBlocks, 50);
    
    return true;
}
//---------------------------------------------------------------
PhysicRenderNode::~PhysicRenderNode()
{
}
//---------------------------------------------------------------
cpSpace* PhysicRenderNode::getSpace() const
{
    return _space;
}
//---------------------------------------------------------------
void PhysicRenderNode::setSpace(cpSpace *space)
{
    _space = space;
}


//---------------------------------------------------------------
// Physic Render Blocks Node
//---------------------------------------------------------------
PhysicRenderBlocksNode* PhysicRenderBlocksNode::create(cpSpace *space)
{
    PhysicRenderBlocksNode *node = new (std::nothrow) PhysicRenderBlocksNode(space);
    if (node && node->init())
    {
        node->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(node);
    }
    
    return node;
}
//---------------------------------------------------------------
PhysicRenderBlocksNode::PhysicRenderBlocksNode(cpSpace *space)
{
    _space = space;
}
//---------------------------------------------------------------
PhysicRenderBlocksNode::~PhysicRenderBlocksNode()
{
}
//---------------------------------------------------------------
void PhysicRenderBlocksNode::setSpace(cpSpace *space)
{
    _space = space;
}
//---------------------------------------------------------------
void PhysicRenderBlocksNode::draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags)
{
    if (! _space)
        return;
    
    cocos2d::DrawNode::clear();
    cpSpaceEachShape(_space, (cpSpaceShapeIteratorFunc)DrawShape, this);
    cocos2d::DrawNode::draw(renderer, transform, flags);
}


//---------------------------------------------------------------
// Physic Render Bombs Node
//---------------------------------------------------------------
PhysicRenderBombsNode* PhysicRenderBombsNode::create(cpSpace *space)
{
    PhysicRenderBombsNode *node = new (std::nothrow) PhysicRenderBombsNode(space);
    if (node && node->init())
    {
        node->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(node);
    }
    
    return node;
}
//---------------------------------------------------------------
PhysicRenderBombsNode::PhysicRenderBombsNode(cpSpace *space)
{
    _space = space;
}
//---------------------------------------------------------------
PhysicRenderBombsNode::~PhysicRenderBombsNode()
{
}
//---------------------------------------------------------------
void PhysicRenderBombsNode::setSpace(cpSpace *space)
{
    _space = space;
}
//---------------------------------------------------------------
void PhysicRenderBombsNode::draw(cocos2d::Renderer *renderer,
                                 const cocos2d::Mat4 &transform,
                                 uint32_t flags)
{
    if (! _space)
        return;
    
    cocos2d::DrawNode::clear();
    cpSpaceEachShape(_space, (cpSpaceShapeIteratorFunc)DrawBomb, this);
    cocos2d::DrawNode::draw(renderer, transform, flags);
}
