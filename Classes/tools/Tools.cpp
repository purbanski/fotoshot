#include <stdlib.h>
#include <dirent.h>
#include "Tools.h"
#include "VisibleRect.h"
//#include "cocos-ext.h"
#include "cocos2d.h"
#include <sys/utsname.h>

#define PI 3.141592653589793
//-------------------------------------------------------------------------------------------------------
int Tools::Rand( int begin, int end )
{
	int range;
	int ret;

	range = end - begin + 1;

	ret = ( rand() % range + begin );
	return ret;
}
//-------------------------------------------------------------------------------------------------------
std::string Tools::GetFilename(const char *filename)
{
    std::string pathKey = filename;
    pathKey = cocos2d::CCFileUtils::getInstance()->fullPathForFilename(pathKey.c_str());
    if (pathKey.size() == 0)
        return "";
    
//    for (unsigned int i = 0; i < pathKey.length(); ++i)
//        pathKey[i] = tolower(pathKey[i]);
    
    return pathKey;
}
//-------------------------------------------------------------------------------------------------------
void Tools::GetFilesInDir(const char *dirName, std::vector<string> &files)
{
    std::string dirFull;
    
    DIR *dir;
    struct dirent *ent;

    dirFull = GetFilename(dirName);
//    dirFull = ".";
    
    if ((dir = opendir (dirFull.c_str())) != NULL)
    {
        /* print all the files and directories within directory */
        while ((ent = readdir (dir)) != NULL)
        {
            if ( ent->d_type == 8 /*DT_REG*/ )
            {
                files.push_back(ent->d_name);
            }
//            printf ("%s %d\n", ent->d_name, ent->d_type);
        }
        closedir (dir);
    }
    else
    {
        /* could not open directory */
        perror ("");
    }
}
//-------------------------------------------------------------------------------------------------------
float Tools::GetBubbleScale()
{
    float scale;
    scale = VisibleRect::center().x / 300;
    return scale;
}
//-------------------------------------------------------------------------------------------------------
DeviceType Tools::GetDeviceType()
{
    char *machine;
    
    DeviceType device;
    struct utsname systemInfo;
    
    uname( &systemInfo );
    machine = systemInfo.machine;
    cocos2d::log("machine %s", machine );
    
    //-------------
    // simulator
    if ( ! strcmp( machine, "i386" ) || ! strcmp( machine, "x86_64" ))
        device = eDevice_simulator;
    
    //-------------
    // iPhone
    else if ( ! strcmp( machine, "iPhone5,1" ))
        device = eDevice_iPhone5_1;
    
    else if ( ! strcmp( machine, "iPhone5,2" ))
        device = eDevice_iPhone5_2;
    
    else if ( ! strcmp( machine, "iPhone4,1" ))
        device = eDevice_iPhone4_1;
    
    else if ( ! strcmp( machine, "iPhone3,1" ))
        device = eDevice_iPhone3_1;
    
    else if ( ! strcmp( machine, "iPhone2,1" ))
        device = eDevice_iPhone2_1;
    
    else if ( ! strcmp( machine, "iPhone1,2" ))
        device = eDevice_iPhone1_2;
    
    else if ( ! strcmp( machine, "iPhone1,1" ))
        device = eDevice_iPhone1_1;
    
    
    //-------------
    // iPad
    else if ( ! strncmp( machine, "iPad3,", 6 ))
        device = eDevice_iPad3_1;
    
    else if ( ! strcmp( machine, "iPad2,1" ))
        device = eDevice_iPad2_1;
    
    else if ( ! strcmp( machine, "iPad1,1" ))
        device = eDevice_iPad1_1;
    
    else if ( ! strcmp( machine, "iPad2,7" ))
        device = eDevice_iPad_Mini;
    
    
    
    //------------
    // iPod
    else if ( ! strcmp( machine, "iPod4,1" ))
        device = eDevice_iPod4_1;
    
    else if ( ! strcmp( machine, "iPod3,1" ))
        device = eDevice_iPod3_1;
    
    else if ( ! strcmp( machine, "iPod2,1" ))
        device = eDevice_iPod2_1;
    
    else if ( ! strcmp( machine, "iPod1,1" ))
        device = eDevice_iPod1_1;
    
    
    else device = eDevice_unknown;
    
    cocos2d::log("machine: (%s), type: %d", machine, device );
    
    return device;
}
////--------------------------------------------------------------
string Tools::IOSgetFilePath()
{
    return "";
}

//-------------------------------------------------------------------------------------------------------
float Tools::DegToRad( float degrees )
{
    return ( degrees * PI ) / 180.0f;
}
//-------------------------------------------------------------------------------------------------------
float Tools::RadToDeg( float radians )
{
    return ( radians * 180.0f ) / PI;
}
//-------------------------------------------------------------------------------------------------------
puPoint Tools::RotatePointAroundPoint( puPoint point, puPoint center, float angleDegree )
{
	float angleRad;
	angleRad = angleDegree / 180.0f * PI;

	float xn = (( point.x - center.x ) * cos( angleRad ) - ( point.y - center.y ) * sin( angleRad )) + center.x;
	float yn = (( point.x - center.x ) * sin( angleRad ) + ( point.y - center.y ) * cos( angleRad )) + center.y;

	return puPoint( xn, yn) ;
}
//-------------------------------------------------------------------------------------------------------

//bool Tools::FileResourceExists( const char *filename )
//{
//    NSString* file = [NSString stringWithUTF8String:FileResourcePath( filename ).c_str() ];
//    bool fileExists = [[NSFileManager defaultManager] fileExistsAtPath:file];
//    
//    return fileExists;
//}
////--------------------------------------------------------------
//bool Tools::FileDocumentExists( const char *filename )
//{
//    NSString* file = [NSString stringWithUTF8String:FileDocumentPath( filename ).c_str() ];
//    bool fileExists = [[NSFileManager defaultManager] fileExistsAtPath:file];
//    
//    return fileExists;
//}
//--------------------------------------------------------------
string Tools::FileResourcePath( const char *filename )
{
    // might not work
    string fullpath;
    fullpath = cocos2d::CCFileUtils::getInstance()->fullPathForFilename( filename );
    
    return fullpath;
}
//--------------------------------------------------------------
string Tools::FileDocumentPath( const char *filename )
{
    string fullpath;
    fullpath = IOSgetFilePath();
    fullpath.append( filename );

    return fullpath;
}
//--------------------------------------------------------------
//string Tools::IOSgetFilePath()
//{
//    // save to document folder
//	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//	NSString *documentsDirectory = [paths objectAtIndex:0];
//	string path = [documentsDirectory UTF8String];
//	path.append( "/" );
//    
//	return path;
//
//}
//--------------------------------------------------------------



//void Tools::GetFilesInDir( const char *dir, Filenames& files )
//{
//	const int maxFileLen = 64;
//	char filename[ maxFileLen ];
//	unFileList *handle;
//
//	string directory;
//	directory.append( "./" );
//	directory.append( dir );
//
//	handle = unFileListDirectory( directory.c_str() );
//	if ( !handle )
//	{
//		unAssertMsg(Filer, false, ("Can't get file list"));
//		return;
//	}
//
//	while(  UN_RESULT_SUCCESS == unFileListNext( handle, filename, maxFileLen ))
//	{
//		files.insert( string( filename ));
//	}
//
//	if ( UN_RESULT_SUCCESS !=  unFileListClose( handle ) )
//	{
//		unAssertMsg(Filer, false, ("Can't close file list"));
//	}
//}
////-------------------------------------------------------------------------------------------------------
//CCSprite* Tools::GetBoxSprite(const char *classname, float width, float height)
//{
//    CCSprite *sprite;
//    CCSprite *spriteLeft;
//    char filename[128];
//	snprintf( &filename[0], 128, "Images/Blocks/%s%dx%d.png", classname, (int) ( width ), (int)( height ));
//
//	string file;
//	file = Skins::GetSkinName( filename );
//
//	
//	if ( unResourceFileCheckExists( file.c_str() ))
//	{
//		sprite = CCSprite::spriteWithFile( file.c_str() );
//	}
//	else
//	{
//
//
//#ifdef DEBUG_MEGA
//		unAssertMsg( BaseBox, false, ("Missing: %s", file.c_str() ));
//#endif
//
//		snprintf( &filename[0], 128, "Images/Blocks/%s.png", "WallBox" );
//		file = Skins::GetSkinName( filename );
//		sprite = CCSprite::spriteWithFile( file.c_str() );
//		
//		float scalex = width / sprite->getContentSize().width * RATIO;
//		float scaley = height / sprite->getContentSize().height * RATIO;
//		sprite->setScaleX( scalex );
//		sprite->setScaleY( scaley );
//	}
////
////    spriteLeft = CCSprite::spriteWithFile("Editor/Images/BlockTextures/WallBoxLeft.png");
////    CCTexture2D *texture;
////    CCImage::
////    sprite = CCSprite::spriteWithTexture(spriteLeft->getTexture(), CCRect::CCRect(0.0f, 0.0f, 300.0f, 400.0f));
//
//    return sprite;
//}
////-------------------------------------------------------------------------------------------------------
//bool Tools::StringRepleaceFirst(std::string& str, const std::string& from, const std::string& to)
//{
//	size_t start_pos = str.find(from);
//	if(start_pos == std::string::npos)
//		return false;
//	str.replace(start_pos, from.length(), to);
//	return true;
//}
////-------------------------------------------------------------------------------------------------------
//void Tools::StringRepleace(std::string& str, const std::string& from, const std::string& to) 
//{
//	if(from.empty())
//		return;
//	size_t start_pos = 0;
//	while((start_pos = str.find(from, start_pos)) != std::string::npos) {
//		str.replace(start_pos, from.length(), to);
//		start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
//	}
//}
////-------------------------------------------------------------------------------------------------------
//b2Vec2 Tools::TouchAdjust( CCTouch * touch )
//{
//#ifdef BUILD_EDITOR
////	return Tools::ConvertScaledLocation( touch );
//#endif
//
//	CCPoint screenMiddle;
//	CCPoint touchLocation = touch->locationInView();	
//
//	screenMiddle = Tools::GetScreenMiddle();
//	touchLocation = CCDirector::sharedDirector()->convertToGL( touchLocation );
//
//	touchLocation.x = screenMiddle.x - (screenMiddle.x - touchLocation.x ) / GameManager::GetGame()->GetScale();
//	touchLocation.y = screenMiddle.y - (screenMiddle.y - touchLocation.y ) / GameManager::GetGame()->GetScale();
//
//	return b2Vec2( touchLocation.x / RATIO, touchLocation.y / RATIO );
//}
////-------------------------------------------------------------------------------------------------------
//b2Vec2 Tools::TouchAdjustToGame( CCTouch *touch )
//{
//    CCSize size;
//    CCPoint screenMiddle;
//    CCPoint touchLocation;
//    float scale;
//    b2Vec2 pos;
//    b2Vec2 resizeDelta;
//
//    touchLocation = touch->locationInView();
//    touchLocation = CCDirector::sharedDirector()->convertToGL( touchLocation );
//    
//    size = CCDirector::sharedDirector()->getWinSize();
//    scale = GameManager::GetGame()->GetScale();
//
//    resizeDelta.x  = ( size.width - Config::GameSize.width * scale ) / 2.0f;
//    resizeDelta.y  = ( size.height - Config::GameSize.height * scale ) / 2.0f;
//        
//    pos.x = touchLocation.x / scale - resizeDelta.x;
//    pos.y = touchLocation.y / scale - resizeDelta.y;
//
//    return pos;
//}
////-------------------------------------------------------------------------------------------------------
//string Tools::AddLeadingZero( int number )
//{
//	stringstream ss;
//	if ( number < 10 )
//		ss << "0";
//	ss << number;
//	return ss.str();
//}
////-------------------------------------------------------------------------------------------------------
//string Tools::GetTimeDiffrence( const timespec &now, const timespec &old )
//{
//	const unsigned long long billion = 1000000000;
//	
//	typedef unsigned long long VeryLong;
//
//	VeryLong nowNano;
//	VeryLong oldNano;
//	VeryLong ret;
//
//	nowNano = now.tv_sec * billion ;
//	nowNano += now.tv_nsec;
//
//	oldNano = old.tv_sec * billion ;
//	oldNano += old.tv_nsec;
//
//	ret = nowNano - oldNano;
//
//	int sec;
//	int nanoSec;
//	stringstream ss;
//
//	sec = (int)( ret / billion );
//	nanoSec = (int)( ret - (VeryLong)sec * billion ) / 10000;
//
//	ss << "sec: " << sec << "." << nanoSec;
//
//	return ss.str();
//}
////-------------------------------------------------------------------------------------------------------
//cocos2d::CCPoint Tools::AdjustPostionWithSurface( CCPoint point )
//{
//	CCSize screenSize;
//	CCSize gameSize;
//	
//	screenSize = CCDirector::sharedDirector()->getWinSizeInPixels();
//	gameSize = Config::GameSize;
//
//	if ( screenSize.width <= gameSize.width && screenSize.height <= gameSize.height )
//		return point;
//
//	CCPoint ret;
//	float xDelta;
//	float yDelta;
//
//	xDelta = ( screenSize.width - gameSize.width ) / 2.0f;
//	yDelta = ( screenSize.height - gameSize.height ) / 2.0f;
//
//	ret.x = point.x - xDelta;
//	ret.y = point.y - yDelta;
//
//	return ret;
//}
////-------------------------------------------------------------------------------------------------------
//string Tools::StripTillBackslash( const char *filename )
//{
//	string ret;
//	string work;
//	int pos;
//
//	work.append( filename );
//	pos = work.rfind( "\\" ) + 1;
//	ret = work.substr( pos, work.length() - pos ) ;
//	return ret;
//}
////-------------------------------------------------------------------------------------------------------
//b2Vec2 Tools::ConvertScaledLocation( CCTouch *touch )
//{
//    CCSize winSize;
//    CCSize border;
//    float scale;
//    
//    scale = GameManager::GetGame()->GetScale();
//    winSize = CCDirector::sharedDirector()->getWinSize();
//    
//    border.height = winSize.height - Config::GameSize.height * scale;
//    border.width = winSize.width - Config::GameSize.width * scale;
//    
//    
//	CCPoint touchLocation;
//    CCPoint screenMiddle;
//	
//    screenMiddle = Tools::GetScreenMiddle();
//	scale = GameManager::GetGame()->GetScale();
//
//    touchLocation= touch->locationInView();
//	touchLocation = CCDirector::sharedDirector()->convertToGL( touchLocation );
//
//    
////    touchLocation.x = screenMiddle.x - (screenMiddle.x - touchLocation.x ) / scale;
////	touchLocation.y = screenMiddle.y - (screenMiddle.y - touchLocation.y ) / scale;
//
//    touchLocation.x = ( touchLocation.x - border.width / 2.0f )  /scale;
//    touchLocation.y = ( touchLocation.y - border.height / 2.0f ) /scale;
//
//	return b2Vec2( touchLocation.x / RATIO, touchLocation.y / RATIO );
//}
////-------------------------------------------------------------------------------------------------------
//b2Vec2 Tools::ConvertLocation( CCTouch *touch )
//{
//	CCPoint touchLocation = touch->locationInView();	
//	touchLocation = CCDirector::sharedDirector()->convertToGL( touchLocation );
//
//	return b2Vec2( touchLocation.x / RATIO, touchLocation.y / RATIO );
//}
////-------------------------------------------------------------------------------------------------------
//b2Vec2 Tools::ScaledToLocation( const b2Vec2& pos )
//{
//	float scale;
//	scale = GameManager::GetGame()->GetRuningLevel()->getScale();
//
//	b2Vec2 temp = pos;
//	temp.x *= RATIO;
//	temp.y *= RATIO;
//
//	CCPoint middle;
//	middle = Tools::GetScreenMiddle();
//
//	temp.x = middle.x + ( temp.x - middle.x ) * scale;
//	temp.y = middle.y + ( temp.y - middle.y ) * scale; 
//
//	return b2Vec2( temp.x / RATIO, temp.y / RATIO );
//}
////-------------------------------------------------------------------------------------------------------
//b2Vec2 Tools::LocationToScaled( const b2Vec2& pos )
//{
//	b2Vec2 temp = pos;
//	temp.x *= RATIO;
//	temp.y *= RATIO;
//
//	float scale;
//	scale = GameManager::GetGame()->GetRuningLevel()->getScale();
//
//	CCPoint middle;
//	middle = Tools::GetScreenMiddle();
//
//	temp.x = middle.x - ( middle.x - temp.x  ) / scale;
//	temp.y = middle.y - ( middle.y - temp.y  ) / scale;
//
//	return b2Vec2( temp.x / RATIO, temp.y / RATIO );
//}
////-------------------------------------------------------------------------------------------------------
//b2Vec2 Tools::ConvertLocationNoRatio( CCTouch *touch )
//{
//	CCPoint touchLocation = touch->locationInView();	
//	touchLocation = CCDirector::sharedDirector()->convertToGL( touchLocation );
//
//	return b2Vec2( touchLocation.x, touchLocation.y );
//}
////-------------------------------------------------------------------------------------------------------
//float Tools::GetScreenMiddleX()
//{
//	CCSize size;
//	size = CCDirector::sharedDirector()->getWinSize();
//	return ( size.width / 2 );
//}
////-------------------------------------------------------------------------------------------------------
//float Tools::GetScreenMiddleY()
//{
//	CCSize size;
//	size = CCDirector::sharedDirector()->getWinSize();
//	return ( size.height / 2 );
//}
////-------------------------------------------------------------------------------------------------------
//CCPoint Tools::GetScreenMiddle()
//{
//	CCSize size;
//	size = CCDirector::sharedDirector()->getWinSize();
//	return ( CCPoint( size.width/2, size.height/2 ));
//}
////-------------------------------------------------------------------------------------------------------
//CCSize Tools::GetScreenSize()
//{
//	CCSize size;
//	size = CCDirector::sharedDirector()->getWinSize();
//	return size;
//}
////-------------------------------------------------------------------------------------------------------
//CCSize Tools::GetBorderSize()
//{
//	CCSize size;
//	size = CCDirector::sharedDirector()->getWinSize();
//    
//    D_FLOAT(size.width);
//    D_FLOAT(size.height);
//    D_FLOAT(GameManager::GetGame()->GetScale());
//    
//    size.width = size.width - Config::GameSize.width * GameManager::GetGame()->GetScale();
//    size.height = size.height - Config::GameSize.height * GameManager::GetGame()->GetScale();
//    
//	return size;
//}
////-------------------------------------------------------------------------------------------------------
//string Tools::GetUrbexString()
//{
//	string ret;
//	ret.append("\x50");
//	ret.append("\x72");
//	ret.append("\x6f");
//	ret.append("\x70");
//	ret.append("\x65");
//	ret.append("\x72");
//	ret.append("\x74");
//	ret.append("\x79");
//	ret.append("\x20");
//	ret.append("\x6f");
//	ret.append("\x66");
//	ret.append("\x3A");
//	ret.append("\x20");
//	ret.append("\x55");
//	ret.append("\x72");
//	ret.append("\x62");
//	ret.append("\x65");
//	ret.append("\x78");
//	ret.append("\x49");
//	ret.append("\x54");
//
//	return ret;
//}
////-------------------------------------------------------------------------------------------------------
//float Tools::GetBoundWidth( b2Body *body )
//{
//	b2Shape *shape;
//	b2AABB boundingBox;
//
//	shape = body->GetFixtureList()->GetShape();
//	shape->ComputeAABB( &boundingBox, body->GetTransform());
//	
//	return boundingBox.upperBound.x - boundingBox.lowerBound.x ;
//}
////-------------------------------------------------------------------------------------------------------
//float Tools::GetBoundHeight( b2Body *body )
//{
//	b2Shape *shape;
//	b2AABB boundingBox;
//
//	shape = body->GetFixtureList()->GetShape();
//	shape->ComputeAABB( &boundingBox, body->GetTransform());
//	
//	return boundingBox.upperBound.y - boundingBox.lowerBound.y ;
//}
////-------------------------------------------------------------------------------------------------------
//b2Vec2 Tools::RotatePointAroundPoint( b2Vec2 point, b2Vec2 center, float angleDegree )
//{
//	float angleRad;
//	angleRad = angleDegree / 180.0f * b2_pi;
//	
//	float xn = (( point.x - center.x ) * cos( angleRad ) - ( point.y - center.y ) * sin( angleRad )) + center.x;
//	float yn = (( point.x - center.x ) * sin( angleRad ) + ( point.y - center.y ) * cos( angleRad )) + center.y;
//	
//	return b2Vec2( xn, yn) ;
//}
////-------------------------------------------------------------------------------------------------------
//void Tools::RotatePointAroundPoint( b2Vec2 *vec, int count, b2Vec2 center, float angleDegree )
//{
//	for ( int i = 0 ; i < count ; i++ )
//	{
//		vec[i] = RotatePointAroundPoint(  vec[i], center, angleDegree );
//	}
//}
//
////-------------------------------------------------------------------------------------------------------
//void Tools::Container_FixtureToBlock( const FixtureContainer &src, BlockContainer &dest )
//{
//	puBlock *block;
//	FixtureContainer::const_iterator fixture;
//
//	for ( fixture = src.begin(); fixture != src.end(); fixture++ )
//	{
//		block = (puBlock *) (*fixture)->GetBody()->GetUserData();
//		if ( ! block )
//			unAssertMsg(MultiBlockOperator, false, ( "Failed to create puBlock from fixture"));
//		else
//			dest.push_back( block );
//	}
//}
////-------------------------------------------------------------------------------------------------------
//void Tools::Container_FixtureToMainBlock( const FixtureContainer &src, BlockContainer &dest )
//{
//	puBlock *block;
//	FixtureContainer::const_iterator fixture;
//
//	for ( fixture = src.begin(); fixture != src.end(); fixture++ )
//	{
//		block = (puBlock *) (*fixture)->GetBody()->GetUserData();
//		if ( ! block )
//			unAssertMsg(MultiBlockOperator, false, ( "Failed to create puBlock from fixture"));
//		else if ( block->AmIRoot() )
//			dest.push_back( block );
//	}
//}
////-------------------------------------------------------------------------------------------------------
//float  Tools::SafeDivide( int value, float diveBy )
//{
//	// HACK:
//	// FIXME:
//	// + 0.0001 because 36 / 10 = 3.599999
//	float ret;
//	ret = ( value / 10.0f );
//	ret += 0.000001f;
//
//	return ret;
//}

//-------------------------------------------------------------------------------------------------------
//void Tools::RunButtonAnim( CCNode* node )
//{
//	float duration;
//	float deltaScale;
//	
//	duration = 0.5f;
//	deltaScale = 0.1f;
//
//	CCRepeatForever *repeat = CCRepeatForever::actionWithAction(
//		(CCActionInterval *)
//		CCSequence::actions( 
//		CCScaleTo::actionWithDuration( duration, 1.0f - deltaScale, 1.0f + deltaScale),
//		CCScaleTo::actionWithDuration( duration, 1.0f + deltaScale, 1.0f - deltaScale),
//		NULL ));
//	node->runAction( repeat );
//}
////-------------------------------------------------------------------------------------------------------
//void Tools::RunBombPulse( CCSprite *sprite )
//{
//    //    return;
//	float dur;
//	float xs;
//	float ys;
//
//	dur = ((float)( rand() % 5 + 6 )) / 10.0f;
//	xs = ((float)( rand() % 6 + 8 )) / 100.0f;
//	ys = ((float)( rand() % 6 + 8 )) / 100.0f;
//
//	CCScaleBy *scaleBy = CCScaleBy::actionWithDuration( dur, 1.0f + xs, 1.0f + ys );
//	
//	CCRepeatForever *repeat =
//		CCRepeatForever::actionWithAction( dynamic_cast<CCActionInterval*>( 
//		CCSequence::actions( scaleBy, scaleBy->reverse(), NULL )));
//
//	sprite->runAction( repeat );
//}
////-------------------------------------------------------------------------------------------------------
//float Tools::RotationNormalize( float angle )
//{
//	float sign;
//	float angleWork;
//	
//	angleWork = abs( angle );
//
//	if ( angle )
//		sign = angleWork / angle;
//	else
//		sign = 1.0f;
//
//	while ( angleWork >= 360.0f)
//	{
//		angleWork -= 360.0f;
//	}
//	
//	return ( angleWork * sign );
//}
////-------------------------------------------------------------------------------------------------------
//string Tools::IntToDottedString( int number )
//{
//	stringstream ss;
//	ss << number;
//
//
//	string str;
//	string str3;
//	
//	typedef vector<string>	VecString;
//	VecString stringVec;
//
//	str = ss.str();
//
//	int len;
//	unsigned int count;
//
//	//------------------
//	// process tripples
//	count = 1;
//	while( str.length() > 3 * count )
//	{
//		len = str.length() - 3 * count;
//		count++;
//		stringVec.push_back( str.substr( len, 3 ));
//	}
//
//	//------------------
//	// add left left overs
//	stringVec.push_back( str.substr( 0, str.length() % 3 ));
//
//
//	//--------------------
//	// reverse buil return
//	string ret;
//	VecString::reverse_iterator it2;
//
//	for ( VecString::reverse_iterator it = stringVec.rbegin(); it != stringVec.rend(); it++ )
//	{
//		ret.append( *it );
//		it2 = it;
//		it2++;
//
//		if ( it2 != stringVec.rend() ) ret.append( "." );
//	}
//
//	return ret;
//}
////-------------------------------------------------------------------------------------------------------
//string Tools::GetTimeStamp( time_t *ptime )
//{
//	time_t now;
//	struct tm* nowFormated;
//	stringstream stamp;
//
//	if ( !ptime )
//	{
//		time( &now );
//		nowFormated = localtime( &now );
//	}
//	else
//	{
//		nowFormated = localtime( ptime );
//	}
//	
//	stamp << "[" ;
//	stamp << Tools::AddLeadingZero( nowFormated->tm_hour );
//	stamp << ":" ;
//	stamp << Tools::AddLeadingZero( nowFormated->tm_min );
//	stamp << ":" ;
//	stamp << Tools::AddLeadingZero( nowFormated->tm_sec );
//	stamp << "] ";
//
//	return stamp.str();
//}
////-------------------------------------------------------------------------------------------------------
//vector<string>& Tools::StringSplit(const string &s, char delim, vector<string> &elems)
//{
//    stringstream ss(s);
//    string item;
//    while (std::getline(ss, item, delim))
//    {
//        elems.push_back(item);
//    }
//    return elems;
//}
////-------------------------------------------------------------------------------------------------------
//vector<string> Tools::StringSplit(const string &s, char delim)
//{
//    vector<std::string> elems;
//    return StringSplit(s, delim, elems);
//}
////-------------------------------------------------------------------------------------------------------
//string Tools::ConvertSecToTime( int sec )
//{
//    int hours;
//	int minutesA;
//	int minutesB;
//	int secondsA;
//	int secondsB;
//    stringstream ss;
//
//	//------------------
//	// Code
//	hours		= Tools::GetHoursCount( sec ) ;
//	minutesA	= Tools::GetMinutesCount( sec ) / 10;
//	minutesB	= Tools::GetMinutesCount( sec ) ;
//	minutesB	= minutesB - ( minutesB / 10 ) * 10;
//
//	secondsA	= GetSecondsCount( sec ) / 10;
//	secondsB	= GetSecondsCount( sec ) ;
//	secondsB	= secondsB - ( secondsB / 10 ) * 10;
//    
//    ss << hours << ":" << minutesA << minutesB << ":" << secondsA << secondsB;
//    return ss.str();
//}
////-------------------------------------------------------------------------------------------------------
//CCParticleSystem* Tools::GetFacebookEmitter()
//{
//    CCParticleFlower *emitter;
//    emitter = CCParticleFlower::node();
//    emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage( "Skins/BluePocket/Images/Particles/fire.png") );
//    
//    return emitter;
//}
////-------------------------------------------------------------------------------------------------------
//
//#define PU_SECONDS_IN_MINUTE 60
//#define PU_MINUTE_IN_HOUR 60
//#define PU_HOUR_IN_DAY 24
//
////---------------------------------------------------------------------
//unsigned int Tools::GetSecondsCount( int seconds )
//{
//	return ( seconds % PU_SECONDS_IN_MINUTE  );
//}
////---------------------------------------------------------------------
//unsigned int Tools::GetMinutesCount( int seconds )
//{
//	return ( seconds / PU_SECONDS_IN_MINUTE  ) % PU_MINUTE_IN_HOUR;
//}
////---------------------------------------------------------------------
//unsigned int Tools::GetHoursCount( int seconds )
//{
//	return (( seconds / ( PU_SECONDS_IN_MINUTE  * PU_MINUTE_IN_HOUR )) %  PU_HOUR_IN_DAY );
//}
////---------------------------------------------------------------------
//unsigned int Tools::GetDaysCount( int seconds )
//{
//	return ( seconds / ( PU_SECONDS_IN_MINUTE  * PU_MINUTE_IN_HOUR *  PU_HOUR_IN_DAY ));
//}
////---------------------------------------------------------------------
//
//#undef PU_SECONDS_IN_MINUTE
//#undef PU_MINUTE_IN_HOUR
//#undef PU_HOUR_IN_DAY
//
//
//#ifdef BUILD_EDITOR
//
//b2Vec2 Tools::ReverseBlockPositionToScreenPosition( float x, float y )
//{
//	b2Vec2 reversedPos;
//	CCSize size;
//
//	size = CCDirector::sharedDirector()->getWinSize();
//	reversedPos.x = x - size.width  / 2.0f / RATIO + 24.0f; // iphone
//	reversedPos.y = y - size.height / 2.0f / RATIO + 16.0f; // iphone
//
//	return reversedPos;
//}
//
//b2Vec2 Tools::AdjustToEditorScreen( float x, float y )
//{
//	b2Vec2 pos;
//	CCSize size;
//
//	size = CCDirector::sharedDirector()->getWinSize();
//
//    float scale;
//    scale = GameManager::Get()->GetGame()->GetScale();
//    
//	pos.x = x + ( size.width - Config::GameSize.width ) * scale / 2.0f / RATIO;
//	pos.y = y + ( size.height - Config::GameSize.height ) * scale / 2.0f / RATIO;
//
//	return pos;
//}
//
//b2Vec2 Tools::ReverseAdjustToEditorScreen( b2Vec2 pos )
//{
//	return ReverseAdjustToEditorScreen( pos.x, pos.y );
//}
//
//
//b2Vec2 Tools::ReverseAdjustToEditorScreen( float x, float y )
//{
//	b2Vec2 pos;
//	CCSize size;
//
//	size = CCDirector::sharedDirector()->getWinSize();
//
//    
//	pos.x = x - ( size.width - Config::GameSize.width ) / 2.0f / RATIO;
//	pos.y = y - ( size.height - Config::GameSize.height ) / 2.0f / RATIO;
//
//	return pos;
//}
//
//b2Vec2 Tools::BlockLikePosition( float x, float y, float scale )
//{
//	// scale must be applied for drawings
//	// for calculations 
//	//it should not be applied
//	b2Vec2 pos;
//	CCSize size;
//
//	size = CCDirector::sharedDirector()->getWinSize();
//	
//	pos.x = x * scale + ( size.width  - Config::GameSize.width / 2.0f * scale ) / 2.0f / RATIO ;
//	pos.y = y * scale + ( size.height - Config::GameSize.height / 2.0f * scale ) / 2.0f / RATIO ;
//
//	return pos;
//}
//
//b2Vec2 Tools::BlockLikePosition( const b2Vec2 &pos, float scale  )
//{
//	return BlockLikePosition( pos.x, pos.y, scale );
//}
//
//void Tools::Container_FixtureToBlockDef( const FixtureContainer &src, BlockDefsContainer &dest )
//{
//	puBlock *block;
//	puBlockDef blockDef;
//	FixtureContainer::const_iterator fixture;
//
//	for ( fixture = src.begin(); fixture != src.end(); fixture++ )
//	{
//		block = (puBlock *) (*fixture)->GetBody()->GetUserData();
//		blockDef = block->GetBlockDef();
//
//		if ( ! block )
//			unAssertMsg(MultiBlockOperator, false, ( "Failed to create puBlock from fixture"));
//		else
//			dest.push_back( blockDef );
//	}
//}
//
//void Tools::Container_FixtureToMainBlockDef( const FixtureContainer &src, BlockDefsContainer &dest )
//{
//	puBlock *block;
//	puBlockDef blockDef;
//	FixtureContainer::const_iterator fixture;
//
//	for ( fixture = src.begin(); fixture != src.end(); fixture++ )
//	{
//		block = (puBlock *) (*fixture)->GetBody()->GetUserData();
//		blockDef = block->GetBlockDef();
//
//		if ( ! block )
//			unAssertMsg(MultiBlockOperator, false, ( "Failed to create puBlock from fixture"));
//		else if ( block->AmIRoot() )
//			dest.push_back( blockDef );
//	}
//}
//
//b2Vec2 Tools::GetBlocksCenter( const BlockContainer& container )
//{
//	puBlock *block;
//	
//	b2Vec2 max;
//	b2Vec2 min;
//	b2Vec2 temp;
//
//	max.Set( -1000, -1000 );
//	min.Set( 1000, 1000 );
//
//	BlockContainer::const_iterator it;
//	for ( it = container.begin(); it != container.end(); it++ )
//	{
//		block = (*it);
//
//		temp = block->GetReversedPosition();
//		if ( temp.x > max.x )
//			max.x = temp.x;
//
//		if ( temp.y > max.y )
//			max.y = temp.y;
//
//		if ( temp.x < min.x )
//			min.x = temp.x;
//
//		if ( temp.y < min.y )
//			min.y = temp.y;
//	}
//
//	b2Vec2 ret;
//
//	ret.x = min.x + ( max.x - min.x ) / 2.0f;
//	ret.y = min.y + ( max.y - min.y ) / 2.0f;
//
//	return ret;
//}
//
//float Tools::GetDistance( const b2Vec2& vec1, const b2Vec2& vec2 )
//{
//	float distance;
//	distance = sqrt( pow(( vec1.x - vec2.x ), 2 ) + pow(( vec1.y - vec2.y ), 2 ));
//	return distance;
//}
//
//b2Vec2 Tools::GetVectorsMiddle( const b2Vec2& vec1, const b2Vec2& vec2 )
//{
//	b2Vec2 ret;
//
//	ret = vec1 + vec2;
//	ret = 0.5f * ret;
//
//	return ret;
//}
//
//b2Vec2 Tools::GetVectorsMiddle( b2Vec2 vec[], int count )
//{
//	b2Vec2 ret;
//	ret.SetZero();
//
//	for ( int i = 0; i < count; i++ )
//	{
//		ret += vec[i];
//	}
//
//	ret =  ( 1.0f / (float) count ) * ret;
//	return ret;
//}
//
//b2Vec2 Tools::AdjustWithLevelPosition( float x, float y )
//{
//	float scale;
//	b2Vec2 ret;
//	CCPoint levelPos;
//
//	levelPos = GameManager::GetGame()->GetRuningLevel()->getPosition();
//	scale = GameManager::GetGame()->GetRuningLevel()->getScale();
//
//	ret.x = x + (( levelPos.x - Tools::GetScreenMiddleX() * scale ) / RATIO );
//	ret.y = y + (( levelPos.y - Tools::GetScreenMiddleY() * scale ) / RATIO );
//	
//	return ret;
//}
//
//b2Vec2 Tools::AdjustWithLevelPosition( const b2Vec2& pos )
//{
//	return AdjustWithLevelPosition( pos.x, pos.y );
//}
//
//string Tools::ToFloatString( float value )
//{
//
//	char str[32];
//	snprintf( str, 32, "%5.4ff", value );
//
//	string ret;
//	ret.append( str );
//
//	return ret;
//}
//
//bool Tools::IsJointOnBlock( puBlock *block, b2Joint *joint )
//{
//	b2JointEdge *j;
//
//	j = block->GetBody()->GetJointList();
//
//	while( j )
//	{
//		if ( j->joint == joint )
//			return true;
//		
//		j = j->next;
//	}
//	return false;
//}
//
//#endif
