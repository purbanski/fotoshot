#ifndef __PU_LIB_TOOLS_H__
#define __PU_LIB_TOOLS_H__
//--------------------------------------------------------------------------------
#include <vector>
#include <string>
#include "MyTypes.h"
//--------------------------------------------------------------------------------
using namespace std;
//--------------------------------------------------------------------------------
class puPoint
{
public:
    puPoint() : x(0), y(0) {}
    puPoint(float ax,float ay) : x(ax), y(ay) {}
    
    template<class T>
    void set(T p) {
        x = p.x;
        y = p.y;
    }
    
    float x;
    float y;
};

class Tools
{
public:
    static int Rand( int begin, int end );
    static string GetFilename( const char *filename );
    static void GetFilesInDir(const char *dir, std::vector<string> &files );
    static float GetBubbleScale();
    static DeviceType GetDeviceType();
    
    static bool FileResourceExists( const char *filename );
    static bool FileDocumentExists( const char *filename );
    static string FileResourcePath( const char *filename );
    static string FileDocumentPath( const char *filename );
    
    static string IOSgetFilePath();

    static string GetMD5( const char *txt );
    
    static void VisitWWW( const char *url );
    static void SendMail(const char *txt);

    static float DegToRad( float degrees );
    static float RadToDeg( float radians );
    static puPoint RotatePointAroundPoint( puPoint point, puPoint center, float angleDegree );

};
//--------------------------------------------------------------------------------

    
    
    
//    
//    static CCSprite* GetBoxSprite(const char *classname, float width, float height);
//	static bool StringRepleaceFirst(std::string& str, const std::string& from, const std::string& to);
//	static void StringRepleace(std::string& str, const std::string& from, const std::string& to);
//	static b2Vec2 TouchAdjust( CCTouch * touch );
//    static b2Vec2 TouchAdjustToGame( CCTouch *touch );
//	static float RotationNormalize( float angle );
//	static CCPoint AdjustPostionWithSurface( CCPoint point );
//	static string AddLeadingZero( int number );  
//    static CCParticleSystem* GetFacebookEmitter();
//	static string StripTillBackslash( const char *filename );
//	static string GetUrbexString();
//	static string GetTimeDiffrence( const timespec &now, const timespec &old );
//	static b2Vec2 ConvertLocation( CCTouch *touch );
//	static b2Vec2 LocationToScaled( const b2Vec2& location );
//	static b2Vec2 ScaledToLocation( const b2Vec2& location );
//	static b2Vec2 ConvertLocationNoRatio( CCTouch *touch );
//	
//	static float GetScreenMiddleX();
//	static float GetScreenMiddleY();
//	static CCPoint GetScreenMiddle();
//	static CCSize  GetScreenSize();
//    static CCSize GetBorderSize();
//	static string IntToDottedString( int number );	    
//	static float GetBoundWidth( b2Body *body );
//	static float GetBoundHeight( b2Body *body );
//	static b2Vec2 RotatePointAroundPoint( b2Vec2 point, b2Vec2 center, float angleDegree );
//	static void RotatePointAroundPoint( b2Vec2 *vec, int count, b2Vec2 center, float angleDegree );
//	
//	//static void Container_BlocksToPointerList( const BlockContainer &src, BlockContainer &dest );
//	static void Container_FixtureToBlock( const FixtureContainer &src, BlockContainer &dest );
//	static void Container_FixtureToMainBlock( const FixtureContainer &src, BlockContainer &dest );
//
//	static string ToFloatString( float value );
//
//	static b2Vec2 ConvertScaledLocation( CCTouch *touch );
//	static void GetFilesInDir( const char *dir, Filenames& files );
//   
//    static vector<string>& StringSplit(const string &s, char delim, vector<string> &elems);
//    static vector<string> StringSplit(const string &s, char delim);
// 
//
//	// te na bank sa w grze
//	static float DegToRad( float degrees );
//	static float RadToDeg( float radians );
//
//	static void RunButtonAnim( CCNode* node );
//	static void RunBombPulse( CCSprite *sprite );
//	static float SafeDivide( int value, float diveBy );
//	static string GetTimeStamp( time_t *ptime = NULL );
//    static string ConvertSecToTime( int sec );
//    
//    static unsigned int GetSecondsCount( int seconds );
//    static unsigned int GetMinutesCount( int seconds );
//    static unsigned int GetHoursCount( int seconds );
//    static unsigned int GetDaysCount( int seconds );
//
//#ifdef BUILD_EDITOR
//	static b2Vec2 AdjustWithLevelPosition( const b2Vec2& pos );
//	static b2Vec2 AdjustWithLevelPosition( float x, float y );
//	static b2Vec2 ReverseBlockPositionToScreenPosition( float x, float y );
//	static b2Vec2 ReverseAdjustToEditorScreen( float x, float y );
//	static b2Vec2 ReverseAdjustToEditorScreen( b2Vec2 pos );
//	static b2Vec2 AdjustToEditorScreen( float x, float y );
//	static b2Vec2 BlockLikePosition( float x, float y, float scale = 1.0f );
//	static b2Vec2 BlockLikePosition( const b2Vec2 &pos, float scale = 1.0f );
//	
//	static void Container_FixtureToMainBlockDef( const FixtureContainer &src, BlockDefsContainer &dest );
//	static void Container_FixtureToBlockDef( const FixtureContainer &src, BlockDefsContainer &dest );
//	static b2Vec2 GetBlocksCenter( const BlockContainer& container );
//	static float GetDistance( const b2Vec2& vec1, const b2Vec2& vec2 );
//	static b2Vec2 GetVectorsMiddle( const b2Vec2& vec1, const b2Vec2& vec2 );
//	static b2Vec2 GetVectorsMiddle( b2Vec2 vec1[], int count );
//	static bool IsJointOnBlock( puBlock *block, b2Joint *joint );
//#endif

#endif
