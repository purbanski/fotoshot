#include "VisibleRect.h"
//-----------------------------------------------------------
cocos2d::Rect VisibleRect::s_visibleRect;
//-----------------------------------------------------------
void VisibleRect::lazyInit()
{
    if (s_visibleRect.size.width == 0.0f && s_visibleRect.size.height == 0.0f)
    {
        s_visibleRect.size = cocos2d::Director::getInstance()->getVisibleSize();;
        s_visibleRect.origin = cocos2d::Director::getInstance()->getVisibleOrigin();
    }
}
//-----------------------------------------------------------
cocos2d::Rect VisibleRect::getVisibleRect()
{
    lazyInit();
    return s_visibleRect;
}
//-----------------------------------------------------------
cocos2d::Point VisibleRect::left()
{
    lazyInit();
    return cocos2d::Point(s_visibleRect.origin.x,
                          s_visibleRect.origin.y+s_visibleRect.size.height/2);
}
//-----------------------------------------------------------
cocos2d::Point VisibleRect::right()
{
    lazyInit();
    return cocos2d::Point(s_visibleRect.origin.x+s_visibleRect.size.width,
                          s_visibleRect.origin.y+s_visibleRect.size.height/2);
}
//-----------------------------------------------------------
cocos2d::Point VisibleRect::top()
{
    lazyInit();
    return cocos2d::Point(s_visibleRect.origin.x+s_visibleRect.size.width/2,
                          s_visibleRect.origin.y+s_visibleRect.size.height);
}
//-----------------------------------------------------------
cocos2d::Point VisibleRect::bottom()
{
    lazyInit();
    return cocos2d::Point(s_visibleRect.origin.x+s_visibleRect.size.width/2, s_visibleRect.origin.y);
}
//-----------------------------------------------------------
cocos2d::Point VisibleRect::center()
{
    lazyInit();
    return cocos2d::Point(s_visibleRect.origin.x+s_visibleRect.size.width/2,
                 s_visibleRect.origin.y+s_visibleRect.size.height/2);
}
//-----------------------------------------------------------
cocos2d::Point VisibleRect::leftTop()
{
    lazyInit();
    return cocos2d::Point(s_visibleRect.origin.x,
                          s_visibleRect.origin.y+s_visibleRect.size.height);
}
//-----------------------------------------------------------
cocos2d::Point VisibleRect::rightTop()
{
    lazyInit();
    return cocos2d::Point(s_visibleRect.origin.x+s_visibleRect.size.width,
                          s_visibleRect.origin.y+s_visibleRect.size.height);
}
//-----------------------------------------------------------
cocos2d::Point VisibleRect::leftBottom()
{
    lazyInit();
    return s_visibleRect.origin;
}
//-----------------------------------------------------------
cocos2d::Point VisibleRect::rightBottom()
{
    lazyInit();
    return cocos2d::Point(s_visibleRect.origin.x+s_visibleRect.size.width,
                          s_visibleRect.origin.y);
}
