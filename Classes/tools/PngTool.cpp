#include "PngTool.h"

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "cocos2d.h"
#include "Tools.h"
#include "Blocks.h"
#include "Config.h"
#include "Brain.h"

#define PNG_DEBUG 3
#include "png.h"
#include "lodepng.h"

//-----------------------------------------------------------------
PngTool* PngTool::_sInstance = NULL;
//-----------------------------------------------------------------
PngTool* PngTool::Get() {
    if (!_sInstance)
        _sInstance = new PngTool();
    return _sInstance;
}
//-----------------------------------------------------------------
void PngTool::Destroy() {
    if (_sInstance)
        delete _sInstance;
    _sInstance = NULL;
}
//-----------------------------------------------------------------
PngTool::PngTool()
{
}
//-----------------------------------------------------------------
PngTool::~PngTool()
{
}
//-----------------------------------------------------------------
void PngTool::LoadPng(const char *file)
{
//    auto filename = CCFileUtils::getInstance()->fullPathForFilename(file);
    
    std::vector<unsigned char> png;     // png data
    std::vector<unsigned char> image;   //the raw pixels
    unsigned width, height;
    
    lodepng::load_file(png, file);
    unsigned error = lodepng::decode(image, width, height, png);
    
    _imageSize.width = width;
    _imageSize.height = height;
    
    _colors.clear();
    
    if(error)
    {
        cocos2d::log( "decoder error:%d %s",error ,lodepng_error_text(error));
        return;
    }
    
    for ( auto i = 0; i<image.size(); i += 4 )
        _colors.push_back(cocos2d::Color4B(image[i],image[i+1] ,image[i+2] ,image[i+3] ));

    cocos2d::log("Size: %d", (int)_colors.size());
//    _colors = _colors.Resample( width, height, 30, 30);
    
}
//-----------------------------------------------------------------
void PngTool::SetColors( cocos2d::Size size, const Colors& colors) {
    _colors = colors;
    _imageSize = size;
}
//-----------------------------------------------------------------
Colors PngTool::GetColors() {
    return _colors;
}
//-----------------------------------------------------------------
cocos2d::Size PngTool::ScaledSize(int w, int h) {
    cocos2d::Size s;
    
    unsigned int size;
    float scale;
    int ballCount = Brain::Get()->GetBallCount();
    
    size = w * h;
    scale = size / (float)ballCount;
    
    s.width = w / sqrt(scale);
    s.height = h / sqrt(scale);
    
    return s;
}
//-----------------------------------------------------------------
void PngTool::CreateBubbledImage()
{
    if (_colors.size() ==0 )
    {
        cocos2d::log("Colors empty!!!");
        return;
    }
    
    float scale;
    float radius;
//    auto imageSize = Size(30,30);
    
    scale = Tools::GetBubbleScale();
    radius = Config::BallRadius;
    
    float xDelta = 2 * radius * scale;
    float yDelta = 2 * radius * scale;
    
    auto size = cocos2d::Director::getInstance()->getWinSize();
    
    auto xStart = size.width / 2 - _imageSize.width * xDelta / 2.0f;
    auto yStart = size.height / 2 + _imageSize.height  * yDelta / 2.0f + Config::BubbleImageYDelta ;
    
    float xPos;
    float yPos;
    cocos2d::Color4B color;
    cocos2d::Color4F colorF;
    
    cocos2d::log("Colors size: %d", (int) _colors.size());
    
    unsigned int count = 0;
    
    for (int y = 0; y < _imageSize.height ; y++ )
        for (int x = 0; x < _imageSize.width ; x++ )
        {
            color = _colors[count];
            colorF = cocos2d::Color4F(color);
            count ++;
            
            xPos =  xStart + x * xDelta;
            yPos =  yStart - y * yDelta;
            
            Blocks::Get()->AddBall(xPos, yPos, radius * scale, colorF);
//            log("%d %f %f %f %f", count, colorF.r, colorF.g, colorF.b, colorF.a  );
        }
    
//    Blocks::Get()->AddBall(xPos, yPos, radius * scale, colorF);
    //    DestroyScene::Get()->HideProcessingLabel();
}
