#ifndef __FotoShot__PngTool__
#define __FotoShot__PngTool__
//------------------------------------------------------------------
#include <stdio.h>
#include "png.h"
#include "FotoShotTypes.h"
#include "cocos2d.h"

//------------------------------------------------------------------
class PngTool {
public:
    static PngTool* Get();
    static void Destroy();
    
    ~PngTool();
    
    void LoadPng(const char *filename);
    
    void CreateBubbledImage();
    void CreateBubbledImage(const Colors& colors);

    Colors GetColors();
    void SetColors( cocos2d::Size size, const Colors& colors);
    
private:
    PngTool();
    void ProcessFile();
    cocos2d::Size ScaledSize(int w, int h);

private:
    cocos2d::Size _imageSize;
    
    Colors _colors;
    static PngTool *_sInstance;
};
//------------------------------------------------------------------
#endif 

