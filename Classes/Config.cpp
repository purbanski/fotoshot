//
//  Config.cpp
//  FotoShot
//
//  Created by Przemek Urbanski on 13/04/15.
//
//

#include "Config.h"

const char* Config::appName = "FotoShot";
const char* Config::imageDefaultFilename = "last.png";
const char* Config::statusFilename = "status.dat";
const char* Config::IPAWeaponPack = "fotoshot.weaponpack";

const char* Config::FontBombTimeout = "Arial";
const char* Config::FontBallCountRestartTimeout = "Arial";

const float Config::MenuPadding = 5.0f;

//------------------------------------------------------------------------
const int Config::BallRadius = 5;
const int Config::BulletRadius = 3;

const float Config::BubbleImageYDelta = 50;
const char* Config::JsonFormatVer = "0.1";

const char*     Config::iTunesUrlFree = "https://itunes.apple.com/app/id846152507";
const char*     Config::iTunesUrlPaid = "https://itunes.apple.com/app/id845512070";

//const char* Config::UrlBackend = "http://192.168.3.2/fsdev/";
const char* Config::UrlBackend = "http://fotoshot.blackted.com/";

const unsigned int    Config::RestartAdvCount = 5;


#ifdef BUILD_FREE //------------------

#ifdef BUILD_DEBUG
const char*     Config::GoogleTrackerId = "UA-38130172-11";
#else
const char*     Config::GoogleTrackerId = "UA-38130172-10";
#endif

const float     Config::DisplayBottomSpace = 150;

#else //------------------

#ifdef BUILD_DEBUG
const char*     Config::GoogleTrackerId = "UA-38130172-11";
#else
const char*     Config::GoogleTrackerId = "UA-38130172-9";
#endif

const float     Config::DisplayBottomSpace = 185;

#endif //-------------------------





#ifdef BUILD_DEBUG //-------------------------

const unsigned int    Config::GoogleTrackerRefreshEverySec = 1;

#else //-------------------------

const unsigned int    Config::GoogleTrackerRefreshEverySec = 10;

#endif //-------------------------
