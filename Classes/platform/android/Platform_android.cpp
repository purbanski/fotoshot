#ifdef BUILD_ANDROID

#include "Platform.h"
#include "toJavaCalls.h"
//-------------------------------------------------------------------------
void Platform::TakePhoto() {
    takePhotoJNI();
    
}
//-------------------------------------------------------------------------
void Platform::AppStartUp() {
    appStartUpJNI();
}
//-------------------------------------------------------------------------
void Platform::BuyWeaponPack() {
    billing_buyWeaponPackJNI();
}
//-------------------------------------------------------------------------
void Platform::RestoreWeaponPack() {
    billing_buyWeaponPackJNI();
}
#endif
