#ifndef __BILLINGIOS_H__
#define __BILLINGIOS_H__

#import <StoreKit/StoreKit.h>
#import <Foundation/Foundation.h>

//------------------------------------------------------------------------
// Transaction Observer
//------------------------------------------------------------------------
@interface TransObserver : NSObject<SKPaymentTransactionObserver>
//------------------------------------------------------------------------
{
}

- (id) init;
- (void) paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions;
- (void) completeTransaction: (SKPaymentTransaction *)transaction;
- (void) restoreTransaction: (SKPaymentTransaction *)transaction;
- (void) failedTransaction: (SKPaymentTransaction *)transaction;
- (void) cancel;

@end
//------------------------------------------------------------------------



//------------------------------------------------------------------------
// Billing iOS
//------------------------------------------------------------------------
@interface BillingIOS : NSObject<SKProductsRequestDelegate>
{
    TransObserver       *m_transObserver;
    bool                 m_restore;
    SKProductsRequest   *m_productRequest;
}
+ (BillingIOS *)sharedInstance;
+ (void) destroy;
+ (bool) isPurchaseAllowed;

- (id) init;
- (void) dealloc;
- (void) buyExtensionPack:(bool) restore;
- (void) productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response;
- (void) cancel;
- (void) restoreTimerCancel;
@end
//------------------------------------------------------------------------

#endif
