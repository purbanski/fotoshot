#import "Billing_iOS.h"
#import "Config.h"
#import "Brain.h"
#import "DestroyScene.h"

static BillingIOS *gBillingIOS = NULL;


@interface TransObserver ( Private )
- (void) transactionCleanUp: (SKPaymentTransaction *)transaction;
@end

//------------------------------------------------------------------------
//
//------------------------------------------------------------------------
@implementation TransObserver
{
}
//------------------------------------------------------------------------
- (id) init
{
    [super init];
    return self;
}
//------------------------------------------------------------------------
- (void) cancel
{
    //[super cancel];
}
//------------------------------------------------------------------------
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    NSLog( @"Transactions count %d", [transactions count] );

    for (SKPaymentTransaction *transaction in transactions)
    {
        NSLog( @"Transactions state %d", transaction.transactionState );

        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [ self completeTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
                
            case SKPaymentTransactionStatePurchasing:
                NSLog( @"Transaction state Purchasing" );
                break;

                
            case SKPaymentTransactionStateFailed:
            case SKPaymentTransactionStateDeferred:
            default:
                [self failedTransaction:transaction];
                break;
        }
    }
}
//------------------------------------------------------------------------
- (void) completeTransaction: (SKPaymentTransaction *)transaction
{
    NSLog(@"transaction complete");
    [ self transactionCleanUp: transaction ];

    DestroySceneMenu::Get()->LeftMenuEnable();
    Brain::Get()->WeaponPackEnable();

//    Billing::Get()->SetTransactionState( eBillingTransactionFinished );
}
//------------------------------------------------------------------------
- (void) restoreTransaction: (SKPaymentTransaction *)transaction
{
    NSLog(@"transaction restore");
    [ self transactionCleanUp: transaction ];

    DestroySceneMenu::Get()->LeftMenuEnable();
    Brain::Get()->WeaponPackEnable();
//    Billing::Get()->SetTransactionState( eBillingTransactionRestoreFinished );
}
//------------------------------------------------------------------------
- (void) failedTransaction: (SKPaymentTransaction *)transaction
{
    [ self transactionCleanUp: transaction ];
 
	NSLog(@"transaction failed: %d", transaction.error.code );
   
    DestroySceneMenu::Get()->LeftMenuEnable();

//    if (transaction.error.code != SKErrorPaymentCancelled)
//        Billing::Get()->SetTransactionState( eBillingTransactionErrorUnknown );
//    else
//        Billing::Get()->SetTransactionState( eBillingTransactionCanceled );
}
//------------------------------------------------------------------------
- (void) transactionCleanUp: (SKPaymentTransaction *)transaction
{
    [[ SKPaymentQueue defaultQueue ] finishTransaction: transaction ];
//    [[ SKPaymentQueue defaultQueue ] removeTransactionObserver:self ];
}
//------------------------------------------------------------------------
@end




//------------------------------------------------------------------------
//
//------------------------------------------------------------------------
@interface BillingIOS()
@property (strong,nonatomic) NSTimer *restoreTimer;
@end

@implementation BillingIOS
{
}
//------------------------------------------------------------------------
+ (BillingIOS *)sharedInstance
{
    if ( ! gBillingIOS )
    {
        gBillingIOS = [[ BillingIOS alloc] init ];
    }
    
    return gBillingIOS;
}
//------------------------------------------------------------------------
+ (void) destroy
{
    if ( gBillingIOS )
        [ gBillingIOS dealloc ];
    
    gBillingIOS = NULL;
}
//------------------------------------------------------------------------
+ (bool) isPurchaseAllowed
{
    return [SKPaymentQueue canMakePayments];
}
//------------------------------------------------------------------------
- (id) init
{
    [ super init ];
    
    m_transObserver = [[ TransObserver alloc] init ];
    m_restore = false;
    m_productRequest = NULL;
    
    //------------------------------------
    // place observer
    [[ SKPaymentQueue defaultQueue ] addTransactionObserver: m_transObserver ];
    

    return self;
}
//------------------------------------------------------------------------
- (void) dealloc
{
    [ super dealloc ];

    [[ SKPaymentQueue defaultQueue ] removeTransactionObserver:m_transObserver ];
    [ m_transObserver release ];
}
//------------------------------------------------------------------------
- (void) buyExtensionPack: (bool) restore
{
    NSLog(@"Buy extension pack");
    m_restore = restore;
    
    //-------------------
    // requestProductData
    NSSet *productIdentifiers =[ NSSet setWithObjects:
                                [ NSString stringWithUTF8String: Config::IPAWeaponPack ],
                                nil];
    
    m_productRequest = [[ SKProductsRequest alloc ]
                        initWithProductIdentifiers: productIdentifiers ];

    m_productRequest.delegate = self;
    [m_productRequest start];

}
//------------------------------------------------------------------------
- (void) productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSArray *myProducts = response.products;

    if ( myProducts.count != 1 )
    {
        DestroySceneMenu::Get()->LeftMenuEnable();
//        Billing::Get()->SetTransactionState( GameTypes::eBillingTransactionProductNotFound );
        return;
    }

//    //------------------------------------
//    // place observer
//    [[ SKPaymentQueue defaultQueue ] addTransactionObserver: m_transObserver ];
//    

    NSLog(@"Prodcut response msg");
    //------------------------------------
    // requestProductData
    SKProduct *productId;
    productId = [ myProducts objectAtIndex: 0 ];
    
    if ( m_restore )
    {
        self.restoreTimer = [NSTimer scheduledTimerWithTimeInterval:10.0f
                                                             target:self
                                                           selector:@selector(restoreFail:)
                                                           userInfo:nil
                                                            repeats:false];
        
        [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    }
    else
    {
        SKPayment *payment = [SKPayment paymentWithProduct:productId];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
}
//------------------------------------------------------------------------
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    DestroySceneMenu::Get()->LeftMenuEnable();

//    NSLog(@"Failed to load list of products.");
//    _productsRequest = nil;
//    
//    _completionHandler(NO, nil);
//    _completionHandler = nil;
    
}
//------------------------------------------------------------------------
-(void) restoreTimerCancel {
    if (self.restoreTimer )
    {
        [self.restoreTimer invalidate];
        self.restoreTimer = nil;
    }
}
//------------------------------------------------------------------------
-(void) restoreFail:(NSTimer*)timer {
    DestroySceneMenu::Get()->LeftMenuEnable();
//    Billing::Get()->SetTransactionState( eBillingTransactionRestoreFailed );
}
//------------------------------------------------------------------------
- (void) cancel
{
    if ( m_productRequest )
    {
        [ m_productRequest cancel ];
        m_productRequest = NULL;
    }
    
    [m_transObserver cancel ];
    
//    [[ SKPaymentQueue defaultQueue ] removeTransactionObserver:m_transObserver ];
}
//------------------------------------------------------------------------
@end
