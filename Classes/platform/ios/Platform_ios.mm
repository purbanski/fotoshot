#ifdef BUILD_IOS
//-------------------------------------------------------------------------
#include "Platform.h"
#import "Billing_iOS.h"
#import "DestroyScene.h"
#include "AppController.h"
#include "cocos2d.h"
#include "Config.h"
#include "Brain.h"
#include <string>
//-------------------------------------------------------------------------
using namespace std;
//-------------------------------------------------------------------------
void Platform::TakePhoto() {
    auto app = (AppController*) [[UIApplication sharedApplication] delegate];
    [app takePhoto];
}
//-------------------------------------------------------------------------
void Platform::AppStartUp() {
    auto app = (AppController*) [[UIApplication sharedApplication] delegate];
    [app startUp];
}
//-------------------------------------------------------------------------
//void Platform::ImageSave(void* img) {
//    string wrpart;
//    UIImage *image = (UIImage*)img;
//    NSString *filePath;
//    
//    wrpart = CCFileUtils::getInstance()->getWritablePath();
//    filePath = [NSString stringWithFormat:@"%s%s", wrpart.c_str(), Config::imageDefaultFilename];
//    log("Save file: %s", [filePath UTF8String]);
//    
//    // Save image.
//    [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
//}
//-------------------------------------------------------------------------
void Platform::BuyWeaponPack() {
    DestroySceneMenu::Get()->LeftMenuDisable();
    [[ BillingIOS sharedInstance ] buyExtensionPack: false ];
}
//-------------------------------------------------------------------------
void Platform::RestoreWeaponPack() {
    DestroySceneMenu::Get()->LeftMenuDisable();
    [[ BillingIOS sharedInstance ] buyExtensionPack: true ];
}
#endif
