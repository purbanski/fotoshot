//
//  Platform.h
//  FotoShot
//
//  Created by Przemek Urbanski on 13/04/15.
//
//

#ifndef __FotoShot__Platform__
#define __FotoShot__Platform__

class Platform {
public:
    static void TakePhoto();
    static void AppStartUp();
    static void BuyWeaponPack();
    static void RestoreWeaponPack();
    

//    static void* ImageNormalize(void* image);
//    static void ImageSave(void* image);
    
};
#endif /* defined(__FotoShot__Platform__) */
