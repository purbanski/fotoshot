#include <stdio.h>
#include "cocos2d.h"
#include "FotoShotTypes.h"
//---------------------------------------------------------------
Colors Colors::Resample(int oldW, int oldH, int newWidth, int newHeight) {
    int w1 = oldW;
    int h1 = oldH;
    int w2 = newWidth;
    int h2 = newHeight;

    auto x_ratio = (int)((w1<<16)/w2) +1;
    auto y_ratio = (int)((h1<<16)/h2) +1;

    Colors cols;
    cols.resize(w2*h2);

    int x2, y2;
    
    for (auto i=0;i<h2;i++) {
        for (auto j=0;j<w2;j++) {
            x2 = ((j*x_ratio)>>16) ;
            y2 = ((i*y_ratio)>>16) ;

            cols.at((i*w2)+j) = at( (y2*w1)+x2);
        }
    }
    
    return cols;
}
