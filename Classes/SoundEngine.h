#ifndef __FotoShot__SoundEngine__
#define __FotoShot__SoundEngine__
//----------------------------------------------------------------------
#include "SimpleAudioEngine.h"
#include <map>
#include <string>
//----------------------------------------------------------------------
class SoundEngine {
public:
    typedef enum {
        eEffect_ButtonTap = 1,
        eEffect_Other
    } EffectType;
    
public:
    static SoundEngine* Get();
    static void Destroy();
    
    ~SoundEngine();
    
    void PlayEffect(EffectType effect);

private:
    SoundEngine();
    void Init();
    CocosDenshion::SimpleAudioEngine* getCocosSound();
    
private:
    static SoundEngine* _sInstance;
    
    typedef std::map<EffectType, std::string> SoundMap;
    SoundMap _effectMap;
    
};
//----------------------------------------------------------------------
    
#endif /* defined(__FotoShot__Sound__) */
