//
//  Config.h
//  FotoShot
//
//  Created by Przemek Urbanski on 13/04/15.
//
//

#ifndef __FotoShot__Config__
#define __FotoShot__Config__

#include <stdio.h>
class Config {
public:
    
    static const char* appName;
    static const char* imageDefaultFilename;
    static const char* statusFilename;
    static const char* IPAWeaponPack;
    
    //----------
    // oldqq
    
    static const char*  UrlBackend;
    
    static const float MenuPadding;
    static const unsigned int GoogleTrackerRefreshEverySec;
    static const unsigned int RestartAdvCount;
    static const char*  GoogleTrackerId;
    
    static const float DisplayBottomSpace;
    
    
    static const int BallRadius;
    static const int BulletRadius;
    
    
    static const float BubbleImageYDelta;
    static const char *JsonFormatVer;
    
    static const char *iTunesUrlFree;
    static const char *iTunesUrlPaid;
    
    static const char* FontBombTimeout;
    static const char* FontBallCountRestartTimeout;
};
#endif /* defined(__FotoShot__Config__) */
