#include <stdio.h>
#include "SoundEngine.h"
#include "SimpleAudioEngine.h"

//------------------------------------------------------------
SoundEngine* SoundEngine::_sInstance = NULL;
//------------------------------------------------------------
SoundEngine* SoundEngine::Get() {
    if (!_sInstance)
    {
        _sInstance = new SoundEngine();
        _sInstance->Init();
    }
    return _sInstance;
}
//------------------------------------------------------------
void SoundEngine::Destroy() {
    if (_sInstance)
        delete _sInstance;
    _sInstance = NULL;
}
//------------------------------------------------------------
SoundEngine::SoundEngine() {
    
}
//------------------------------------------------------------
SoundEngine::~SoundEngine() {
    
}
//------------------------------------------------------------
void SoundEngine::Init() {
    _effectMap[eEffect_ButtonTap] = "sounds/buttonTap.mp3";
    
    for (auto sound : _effectMap )
        getCocosSound()->preloadEffect(sound.second.c_str());
    
    // just to preload engine
    getCocosSound()->playEffect("sounds/none.mp3");
}
//------------------------------------------------------------
CocosDenshion::SimpleAudioEngine* SoundEngine::getCocosSound() {
    return CocosDenshion::SimpleAudioEngine::getInstance();
}
//------------------------------------------------------------
void SoundEngine::PlayEffect(SoundEngine::EffectType effect) {
    getCocosSound()->playEffect(_effectMap[effect].c_str());
}
//------------------------------------------------------------

//------------------------------------------------------------

//------------------------------------------------------------


//------------------------------------------------------------