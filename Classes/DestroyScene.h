#ifndef __FotoShot__DestroySceneLayer__
#define __FotoShot__DestroySceneLayer__
//---------------------------------------------------------------------------------
#include "cocos2d.h"
#include "MainMenu.h"
#include "Weapon.h"
#include "Background.h"
#include "ProcessingLayer.h"
//---------------------------------------------------------------------------------
class DestroySceneLayer : public cocos2d::Layer
{
public:
    CREATE_FUNC(DestroySceneLayer);
    static cocos2d::Scene* createScene();
    static DestroySceneLayer* sLastScene;
    static const int LayerTag = 100;
    
//    void postInit();
    void UpdateWeapon();
    ~DestroySceneLayer();
    
    void AccelStart();
    void AccelStop();
    
    void ShowProcessing();
    void HideProcessing();
    
private:
    DestroySceneLayer();
    
    virtual bool init();
    void menuCloseCallback(cocos2d::Ref* pSender);

    virtual void update(float delta);

    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    
    void onAccel(cocos2d::Acceleration* accel, cocos2d::Event* event);

private:
    Weapon* _weapon;
    cocos2d::EventListenerAcceleration *_accelListener;
    
    Background* _background;
    ProcessingLayer* _processImg;
    
    static DestroySceneLayer* _sInstance;
};

#endif

