#ifndef __Foto_Shot__DestroySceneMenu__
#define __Foto_Shot__DestroySceneMenu__
//------------------------------------------------------------------------------
#include <cocos2d.h>
//------------------------------------------------------------------------------
class SideMenu;
class DestroySceneMenu : public cocos2d::Node
{
public:
    static DestroySceneMenu* Create();
    static DestroySceneMenu* Get();
    
    ~DestroySceneMenu();

    void LeftMenuDisable();
    void LeftMenuEnable();

    void Menu_slider();
    void Menu_shot();
    void Menu_bomb();
    void Menu_weapon();
    void Menu_bubbleCount();
    void Menu_buy();

    void Menu_settings(cocos2d::Ref* sender);

    void RestartMenus();
    
private:
    DestroySceneMenu();
    void Init();
    
    void Menu_share(cocos2d::Ref* sender);
    void Menu_effects(cocos2d::Ref* sender);
    void Menu_photo(cocos2d::Ref* sender);
    void Menu_restart(cocos2d::Ref* sender);
    
    void Menu_test(Ref* pSender);

    
    void MenuRightSetup();
    void MenuRightPostup();

    void MenuLeftSetup();
    void MenuLeftPostup();
    
    void UpdateButtons();

    
    
private:
    SideMenu* _rightMenu;
    SideMenu* _leftMenu;

    static DestroySceneMenu* _sInstance;   
};


//------------------------------------------------------------------------------
#endif
