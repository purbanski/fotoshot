#ifndef __FotoShot__ShareMenu__
#define __FotoShot__ShareMenu__
//------------------------------------------------------------------------------
#include <cocos2d.h>
//------------------------------------------------------------------------------
class ShareMenu : public cocos2d::Node
{
public:
    static ShareMenu* Create();
    
    ~ShareMenu();
    
private:
    ShareMenu();
    void Init();
    
    void Menu_facebook(cocos2d::Ref *sender);
    void Menu_email(cocos2d::Ref *sender);
    void Menu_www(cocos2d::Ref *sender);
    
private:
    cocos2d::Menu *_menu;
};


//------------------------------------------------------------------------------

#endif /* defined(__FotoShot__ShareMenu__) */
