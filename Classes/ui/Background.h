#ifndef __FotoShot__Background__
#define __FotoShot__Background__
//----------------------------------------------------------------
#include <iostream>
#include "cocos2d.h"
//----------------------------------------------------------------
class Background : public cocos2d::Node
{
public:
    static Background* create();
    ~Background();

    void SetWhite();
    void SetBlack();
    void SetColor( int r, int g, int b, int a);
    
private:
    Background();
    void Init();
    
private:
    cocos2d::Sprite *_sprite;
};
//----------------------------------------------------------------
#endif
