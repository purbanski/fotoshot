#include "ShareMenu.h"
#include "Config.h"
#include "FxMenuItemImage.h"
//------------------------------------------------------------------------------
ShareMenu* ShareMenu::Create()
{
    ShareMenu* node;
    node = new ShareMenu();
    if ( node )
    {
        node->autorelease();
        node->Init();
    }
    return node;
}
//------------------------------------------------------------------------------
ShareMenu::ShareMenu()
{
}
//------------------------------------------------------------------------------
ShareMenu::~ShareMenu()
{
}
//------------------------------------------------------------------------------
void ShareMenu::Init() {
   
    auto itemEmail = FxMenuItemImage::create("images/menus/tools/share/email.png",
                                             "images/menus/tools/share/emailOn.png",
                                             CC_CALLBACK_1(ShareMenu::Menu_email,this ));
    
    auto itemFacebook = FxMenuItemImage::create("images/menus/tools/share/facebook.png",
                                                "images/menus/tools/share/facebookOn.png",
                                                CC_CALLBACK_1(ShareMenu::Menu_email,this ));
    
    auto itemWWW = FxMenuItemImage::create("images/menus/tools/share/www.png",
                                           "images/menus/tools/share/wwwOn.png",
                                           CC_CALLBACK_1(ShareMenu::Menu_email,this ));

    auto menu = cocos2d::Menu::create(itemEmail, itemFacebook, itemWWW, NULL);
    menu->alignItemsHorizontallyWithPadding(Config::MenuPadding);
    
    addChild(menu);
}
//------------------------------------------------------------------------------
void ShareMenu::Menu_facebook(cocos2d::Ref *sender)
{
//    GTracker::Get()->Event_UI_ButtonPressed("share facebook");
//    _sharer.ShareFacebook(_imageTool.GetJson().c_str(), _shooter->GetJson().c_str());
}
//------------------------------------------------------------------------------
void ShareMenu::Menu_email(cocos2d::Ref *sender)
{
//    GTracker::Get()->Event_UI_ButtonPressed("share mail");
//    _sharer.ShareMail(_imageTool.GetJson().c_str(), _shooter->GetJson().c_str());
}
//------------------------------------------------------------------------------
void ShareMenu::Menu_www(cocos2d::Ref *sender)
{
//    GTracker::Get()->Event_UI_ButtonPressed("share www");
//    _sharer.ViewOnWeb(_imageTool.GetJson().c_str(), _shooter->GetJson().c_str());
}



