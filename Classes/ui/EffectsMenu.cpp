#include "EffectsMenu.h"
#include "Config.h"
#include "World.h"
#include "Blocks.h"
#include "Brain.h"
#include "FxMenuItemImage.h"
#include "VisibleRect.h"
//------------------------------------------------------------------------------
EffectsMenu* EffectsMenu::Create()
{
    EffectsMenu* node;
    node = new EffectsMenu();
    if ( node )
    {
        node->autorelease();
        node->Init();
    }
    return node;
}
//------------------------------------------------------------------------------
EffectsMenu::EffectsMenu()
{
}
//------------------------------------------------------------------------------
EffectsMenu::~EffectsMenu()
{
}
//------------------------------------------------------------------------------
void EffectsMenu::Init() {
    SideMenu::Init();
    
    auto itemShot = FxMenuItemImage::create("images/menus/tools/effects/shot.png",
                                            "images/menus/tools/effects/shotOn.png",
                                            CC_CALLBACK_1(EffectsMenu::Menu_shot, this));
    
    auto itemBomb = FxMenuItemImage::create("images/menus/tools/effects/bomb.png",
                                            "images/menus/tools/effects/bombOn.png",
                                            CC_CALLBACK_1(EffectsMenu::Menu_bomb, this));
    
    auto itemSlider = FxMenuItemImage::create("images/menus/tools/effects/slider.png",
                                              "images/menus/tools/effects/sliderOn.png",
                                              CC_CALLBACK_1(EffectsMenu::Menu_slider, this));
    

    // Accel
    auto itemAccelDisabled = cocos2d::MenuItemImage::create("images/menus/tools/effects/accelDisabled.png",
                                                   "images/menus/tools/effects/accelOn.png",
                                                   "images/menus/tools/effects/accelOff.png",
                                                   CC_CALLBACK_1(EffectsMenu::Menu_accel, this));

    auto itemAccelEnabled = cocos2d::MenuItemImage::create("images/menus/tools/effects/accelEnabled.png",
                                                  "images/menus/tools/effects/accelOn.png",
                                                  "images/menus/tools/effects/accelOff.png",
                                                  CC_CALLBACK_1(EffectsMenu::Menu_accel, this));
    itemAccelDisabled->setUserData(0);
    itemAccelEnabled->setUserData(0);

    _itemAccelT = FxMenuItemToggle::createWithCallback(CC_CALLBACK_1(EffectsMenu::Menu_accel,this),
                                                    itemAccelDisabled,
                                                    itemAccelEnabled,
                                                    NULL);
    
    // Border
    auto itemBorderDisabled = cocos2d::MenuItemImage::create("images/menus/tools/effects/borderDisabled.png",
                                                   "images/menus/tools/effects/borderOn.png",
                                                   CC_CALLBACK_1(EffectsMenu::Menu_border, this));

    auto itemBorderEnabled = cocos2d::MenuItemImage::create("images/menus/tools/effects/borderEnabled.png",
                                                  "images/menus/tools/effects/borderOn.png",
                                                  CC_CALLBACK_1(EffectsMenu::Menu_border, this));
    
    _itemBorderT = FxMenuItemToggle::createWithCallback(CC_CALLBACK_1(EffectsMenu::Menu_border,this),
                                                    itemBorderDisabled,
                                                    itemBorderEnabled,
                                                    NULL);


    // Gravity
    auto itemGravityDisabled = cocos2d::MenuItemImage::create("images/menus/tools/effects/gravityDisabled.png",
                                                    "images/menus/tools/effects/gravityOn.png",
                                                    CC_CALLBACK_1(EffectsMenu::Menu_gravity, this));
    
    auto itemGravityEnabled = cocos2d::MenuItemImage::create("images/menus/tools/effects/gravityEnabled.png",
                                                     "images/menus/tools/effects/gravityOn.png",
                                                     CC_CALLBACK_1(EffectsMenu::Menu_gravity, this));
    
    itemGravityDisabled->setUserData(0);
    itemGravityEnabled->setUserData(0);
    
    _itemGravityT = FxMenuItemToggle::createWithCallback(CC_CALLBACK_1(EffectsMenu::Menu_gravity,this),
                                                       itemGravityDisabled,
                                                       itemGravityEnabled,
                                                       NULL);

    _menu = cocos2d::Menu::create(_itemAccelT,
                         _itemGravityT,
                         _itemBorderT,
                         itemBomb,
                         itemShot,
                         itemSlider,
                         NULL);
    
    if (!Brain::Get()->IsWeaponPackEnabled())
    {
        _itemAccelT->setEnabled(false);
    }

    _menu->alignItemsVerticallyWithPadding(Config::MenuPadding);
    addChild(_menu);
    
    SetMenuPositionRight();
    HideNoAnim();
    UpdateButtons();
}
//------------------------------------------------------------------------------
void EffectsMenu::Menu_shot(cocos2d::Ref* sender)
{
    ResetTimeout();
    Brain::Get()->SetWeapon(Weapon::eWeapon_Shooter);
    DestroySceneMenu::Get()->Menu_shot();
}
//------------------------------------------------------------------------------
void EffectsMenu::Menu_bomb(cocos2d::Ref* sender)
{
    ResetTimeout();
    Brain::Get()->SetWeapon(Weapon::eWeapon_Bomb);
    DestroySceneMenu::Get()->Menu_bomb();
}
//------------------------------------------------------------------------------
void EffectsMenu::Menu_slider(cocos2d::Ref* sender)
{
    ResetTimeout();
    Brain::Get()->SetWeapon(Weapon::eWeapon_Slider);
    DestroySceneMenu::Get()->Menu_slider();
}
//------------------------------------------------------------------------------
void EffectsMenu::Menu_accel(cocos2d::Ref* sender)
{
    ResetTimeout();
    Brain::Get()->ToggleAccelator();
    UpdateButtons();
}
//------------------------------------------------------------------------------
void EffectsMenu::Menu_border(cocos2d::Ref* sender)
{
    ResetTimeout();
    Brain::Get()->ToggleBorder();
    UpdateButtons();
}
//------------------------------------------------------------------------------
void EffectsMenu::Menu_gravity(cocos2d::Ref* sender)
{
    ResetTimeout();
    Brain::Get()->SetWeapon(Weapon::eWeapon_Gravity);
    UpdateButtons();
}
//------------------------------------------------------------------------------
void EffectsMenu::UpdateButtons()
{
    RunningSettings settings;
    settings = Brain::Get()->GetRunningSettings();
    
    if (settings._accelEnabled)
        _itemAccelT->setSelectedIndex(1);
    else
        _itemAccelT->setSelectedIndex(0);
    
    if (settings._borderEnabled)
        _itemBorderT->setSelectedIndex(1);
    else
        _itemBorderT->setSelectedIndex(0);
    
    if (settings._gravityEnabled)
        _itemGravityT->setSelectedIndex(1);
    else
        _itemGravityT->setSelectedIndex(0);
    
}
//------------------------------------------------------------------------------
