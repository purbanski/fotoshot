#ifndef __FotoShot__EffectsMenu__
#define __FotoShot__EffectsMenu__
//------------------------------------------------------------------------------
#include <cocos2d.h>
#include "BaseMenu.h"
//------------------------------------------------------------------------------
class FxMenuItemToggle;
class EffectsMenu : public SideMenu
{
public:
    static EffectsMenu* Create();
    
    ~EffectsMenu();
    
private:
    EffectsMenu();
    void Init();
    
    void Menu_shot(cocos2d::Ref* sender);
    void Menu_bomb(cocos2d::Ref* sender);
    void Menu_slider(cocos2d::Ref* sender);
    void Menu_accel(cocos2d::Ref* sender);
    void Menu_border(cocos2d::Ref* sender);

    void Menu_gravity(cocos2d::Ref* sender);
    
    void UpdateButtons();
    
private:
    FxMenuItemToggle* _itemAccelT;
    FxMenuItemToggle* _itemBorderT;
    FxMenuItemToggle* _itemGravityT;
};
//------------------------------------------------------------------------------
#endif /* defined(__FotoShot__EffectsMenu__) */
