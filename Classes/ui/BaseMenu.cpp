#include "BaseMenu.h"
#include "FxMenuItemImage.h"
#include "VisibleRect.h"
#include "MainMenu.h"
#include "Config.h"
//------------------------------------------------------------------------------
float SideMenu::TIMEOUT = 4;
float SideMenu::MENU_MOVE = 730.0f;
float SideMenu::BUTTON_MOVE = 130.0f;
//------------------------------------------------------------------------------
SideMenu::SideMenu() {
    _menu = NULL;
    _menuBack = NULL;
    _shown = true;
    _timeout = TIMEOUT;
}
//------------------------------------------------------------------------------
void SideMenu::Init(const char* showMenuImg, const char* showMenuImgOn) {
    auto showMenu = FxMenuItemImage::create(showMenuImg,
                                            showMenuImgOn,
                                            CC_CALLBACK_1(SideMenu::Menu_show, this));
    InitCommon(showMenu);
}
//------------------------------------------------------------------------------
void SideMenu::Init() {
    auto showMenu = FxMenuItemImage::create("images/menus/tools/effects.png",
                                            "images/menus/tools/effectsOn.png",
                                            CC_CALLBACK_1(SideMenu::Menu_show, this));

    InitCommon(showMenu);
}
//------------------------------------------------------------------------------
void SideMenu::InitCommon(cocos2d::MenuItem *itemShowMenu)
{
    _menuBack = cocos2d::Menu::create(itemShowMenu, NULL);
    addChild(_menuBack);
    
    scheduleUpdate();
}
//------------------------------------------------------------------------------
void SideMenu::SetMenuPositionRight() {
    if (!_menu)
        return;

    auto count = _menu->getChildrenCount();

    cocos2d::Vec2 pos;
    pos.x = VisibleRect::right().x - 60;
    pos.y = VisibleRect::bottom().y + (113+Config::MenuPadding)*count/2; // 113 - image size
    _menu->setPosition(pos);
    
    pos.x = VisibleRect::right().x - 60 ;
    pos.y = VisibleRect::bottom().y - (113+Config::MenuPadding)/2;
    _menuBack->setPosition(pos);
}
//------------------------------------------------------------------------------
void SideMenu::SetMenuPositionLeft() {
    if (!_menu)
        return;
    
    auto count = _menu->getChildrenCount();
    
    cocos2d::Vec2 pos;
    pos.x = VisibleRect::left().x + 60;
    pos.y = VisibleRect::bottom().y + (113+Config::MenuPadding)*count/2; // 113 - image size
    _menu->setPosition(pos);
    
    pos.x = VisibleRect::left().x + 60 ;
    pos.y = VisibleRect::bottom().y - (113+Config::MenuPadding)/2;
    _menuBack->setPosition(pos);

}
//------------------------------------------------------------------------------
cocos2d::MenuItem* SideMenu::GetLeftBackMenuItem()
{
    auto itemBack = FxMenuItemImage::create("images/menus/tools/backLeft.png",
                                            "images/menus/tools/backLeftOn.png",
                                            "images/menus/tools/backLeftOff.png",
                                            CC_CALLBACK_1(SideMenu::Menu_backmenu, this));
    return itemBack;
}
//------------------------------------------------------------------------------
cocos2d::MenuItem* SideMenu::GetRigthBackMenuItem()
{
    auto itemBack = FxMenuItemImage::create("images/menus/tools/backRight.png",
                                            "images/menus/tools/backRightOn.png",
                                            "images/menus/tools/backRightOff.png",
                                            CC_CALLBACK_1(SideMenu::Menu_backmenu, this));
    return itemBack;
}
//------------------------------------------------------------------------------
void SideMenu::Menu_show(cocos2d::Ref* sender)
{
    Show();
}
//------------------------------------------------------------------------------
void SideMenu::Menu_backmenu(cocos2d::Ref* sender)
{
    DestroySceneMenu::Get()->Menu_weapon();
}
//------------------------------------------------------------------------------
void SideMenu::Show() {
    if (_shown)
        return;
    
    _shown = true;
    _menu->runAction(cocos2d::MoveBy::create(0.25f,
                                             cocos2d::Vec2(0,GetMenuHeight())));
    _menuBack->runAction(cocos2d::MoveBy::create(0.25f,
                                                 cocos2d::Vec2(0,-(113+Config::MenuPadding))));
    ResetTimeout();
}
//------------------------------------------------------------------------------
void SideMenu::Hide() {
    if (!_shown)
        return;
    
    _shown = false;
    _menu->runAction(cocos2d::MoveBy::create(0.25f,
                                             cocos2d::Vec2(0,-GetMenuHeight())));
    _menuBack->runAction(cocos2d::MoveBy::create(0.25f,
                                                 cocos2d::Vec2(0,(113+Config::MenuPadding))));
}
//------------------------------------------------------------------------------
void SideMenu::ShowNoAnim()
{
    if (_shown)
        return;
    
    _shown = true;
    cocos2d::Vec2 pos;
    pos = _menu->getPosition();
//    _menu->setPosition(pos.x,pos.y+_menu->getContentSize().height);
    _menu->setPosition(pos.x,pos.y+GetMenuHeight());
    
    pos = _menuBack->getPosition();
    _menuBack->setPosition(pos.x,pos.y-(113+Config::MenuPadding));
}
//------------------------------------------------------------------------------
void SideMenu::HideNoAnim()
{
    if (!_shown)
        return;
    
    _shown = false;

    cocos2d::Vec2 pos;
    pos = _menu->getPosition();

    _menu->setPosition(pos.x,
                       pos.y - GetMenuHeight());
    
    pos = _menuBack->getPosition();
    _menuBack->setPosition(pos.x,pos.y+(113+Config::MenuPadding));
}
//------------------------------------------------------------------------------
float SideMenu::GetMenuHeight()
{
    auto children = _menu->getChildren();
    float ret = 0.0f;
    
    for (auto item: children)
    {
        ret += item->getContentSize().height + Config::MenuPadding;
    }
    
    return ret;
}
//------------------------------------------------------------------------------
void SideMenu::update(float delta) {
    if (!_timeout)
        return;
    
    _timeout -= delta;
    
    if (_timeout <= 0 )
    {
        _timeout = 0;
        Hide();
    }
}
//------------------------------------------------------------------------------
void SideMenu::ResetTimeout() {
    _timeout = TIMEOUT;
}
//------------------------------------------------------------------------------
void SideMenu::SetEnable(bool enable)
{
        if (_menu && _menu->getParent())
        {
            auto children = _menu->getChildren();
            for ( auto item : children )
            {
                cocos2d::MenuItemImage* i;
                i=(cocos2d::MenuItemImage*) item;
                i->setEnabled(enable);
            }
            _menu->setEnabled(enable);
        }
}