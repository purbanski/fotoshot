#ifndef __FotoShot__BaseMenu__
#define __FotoShot__BaseMenu__
//------------------------------------------------------------------------------
#include "cocos2d.h"
//------------------------------------------------------------------------------
class SideMenu : public cocos2d::Node {
protected:
    SideMenu();

    virtual void Init();
    virtual void Init(const char* showMenuImg, const char* showMenuImgOn);
    
    cocos2d::MenuItem* GetLeftBackMenuItem();
    cocos2d::MenuItem* GetRigthBackMenuItem();
    
    void ResetTimeout();

    void SetMenuPositionRight();
    void SetMenuPositionLeft();
    
    virtual void Menu_backmenu(cocos2d::Ref* sender);

public:
    virtual void Hide();
    virtual void Show();
    
    void HideNoAnim();
    void ShowNoAnim();

    void SetEnable(bool enable);
 
private:
    void InitCommon(cocos2d::MenuItem *itemShowMenu);

    void Menu_show(cocos2d::Ref* sender);
    
    void update(float delta);
    float GetMenuHeight();

protected:
    cocos2d::Menu *_menu;
    
private:

    cocos2d::Menu *_menuBack;
    
    bool _shown;
    
    float _timeout;
    static float TIMEOUT;
    static float MENU_MOVE;
    static float BUTTON_MOVE;
};

#endif /* defined(__FotoShot__BaseMenu__) */
