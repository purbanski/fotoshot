#ifndef __FotoShot__SettingsMenu__
#define __FotoShot__SettingsMenu__
//------------------------------------------------------------------------------
#include <cocos2d.h>
#include "BaseMenu.h"
//------------------------------------------------------------------------------
class SettingsMenu : public SideMenu
{
public:
    static SettingsMenu* Create();
    
    ~SettingsMenu();
    
private:
    SettingsMenu();
    void Init();
//    void InitWithExtensionPack();
//    void InitWithoutExtensionPack();
    
    void Menu_buy(Ref* sender);
    void Menu_bubbleCount(Ref* sender);
};
//------------------------------------------------------------------------------
#endif /* defined(__FotoShot__SettingsMenu__) */
