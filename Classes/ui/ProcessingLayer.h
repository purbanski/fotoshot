#ifndef __FotoShot__ProcessingLayer__
#define __FotoShot__ProcessingLayer__
//----------------------------------------------------------------
#include <iostream>
#include "cocos2d.h"
//----------------------------------------------------------------
class ProcessingLayer : public cocos2d::Node
{
public:
    static ProcessingLayer* create();
    ~ProcessingLayer();

    void Show();
    void Hide();
    
private:
    ProcessingLayer();
    void Init();
    
private:
    cocos2d::Sprite* _bg;
    cocos2d::Sprite* _label;
};
//----------------------------------------------------------------
#endif

