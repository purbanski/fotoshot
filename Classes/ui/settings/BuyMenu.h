#ifndef __FotoShot__BuyMenu__
#define __FotoShot__BuyMenu__
//------------------------------------------------------------------------------
#include <cocos2d.h>
#include "BaseMenu.h"
//------------------------------------------------------------------------------
class BuyMenu : public SideMenu
{
public:
    static BuyMenu* Create();
    ~BuyMenu();
    
protected:
    virtual void Menu_backmenu(Ref* sender) override;

private:
    BuyMenu();
    void Init();
    
    void Menu_buy(Ref* sender);
    void Menu_restore(Ref* sender);
};
//------------------------------------------------------------------------------
#endif /* defined(__FotoShot__BuyMenu__) */
