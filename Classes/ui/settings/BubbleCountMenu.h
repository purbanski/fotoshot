#ifndef __FotoShot__BubbleCountMenu__
#define __FotoShot__BubbleCountMenu__
//------------------------------------------------------------------------------
#include <cocos2d.h>
#include "BaseMenu.h"
//------------------------------------------------------------------------------
class BubbleCountMenu : public SideMenu
{
public:
    static BubbleCountMenu* Create();
    
    ~BubbleCountMenu();
    
protected:
    virtual void Hide() override;
    virtual void Show() override;
    virtual void Menu_backmenu(Ref* sender) override;
    
private:
    BubbleCountMenu();
    void Init();
    
    void Menu_plus(Ref* sender);
    void Menu_minus(Ref* sender);
    
    void UpdateBallCountLabel();
    void UpdateRestartCountLabel();
    
    void ResetLevelStep(float delta);
    
    void FadeInLabels();
    void FadeOutLabels();
    
private:
    bool _countDownStarted;
    float _countDown;
    
    unsigned int _currentBallCount;
    cocos2d::LabelTTF* _labelBallCount;
    cocos2d::LabelTTF* _labelRestart;
    
    cocos2d::MenuItemImage* _itemMinus;
    cocos2d::MenuItemImage* _itemPlus;
};
//------------------------------------------------------------------------------
#endif /* defined(__FotoShot__BubbleCountMenu__) */
