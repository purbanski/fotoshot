#include "BuyMenu.h"
#include "Config.h"
#include "World.h"
#include "Blocks.h"
#include "Brain.h"
#include "FxMenuItemImage.h"
#include "VisibleRect.h"
#include "Platform.h"

//------------------------------------------------------------------------------
BuyMenu* BuyMenu::Create()
{
    BuyMenu* node;
    node = new BuyMenu();
    if ( node )
    {
        node->autorelease();
        node->Init();
    }
    return node;
}
//------------------------------------------------------------------------------
BuyMenu::BuyMenu()
{
}
//------------------------------------------------------------------------------
BuyMenu::~BuyMenu()
{
}
//------------------------------------------------------------------------------
void BuyMenu::Init() {
    SideMenu::Init("images/menus/tools/settings.png",
                   "images/menus/tools/settingsOn.png");
    
    auto itemBuy = FxMenuItemImage::create("images/menus/tools/settings/buy/buy.png",
                                           "images/menus/tools/settings/buy/buyOn.png",
                                           "images/menus/tools/settings/buy/buyOff.png",
                                           CC_CALLBACK_1(BuyMenu::Menu_buy, this));
    
    auto itemRestore = FxMenuItemImage::create("images/menus/tools/settings/buy/restore.png",
                                               "images/menus/tools/settings/buy/restoreOn.png",
                                               "images/menus/tools/settings/buy/restoreOff.png",
                                               CC_CALLBACK_1(BuyMenu::Menu_restore, this));
    
    
    _menu= cocos2d::Menu::create(itemBuy,
                        itemRestore,
                        GetLeftBackMenuItem(), NULL);
    
    _menu->alignItemsVerticallyWithPadding(Config::MenuPadding);
    addChild(_menu);
    
    SetMenuPositionLeft();
    HideNoAnim();
}
//------------------------------------------------------------------------------
void BuyMenu::Menu_buy(Ref* sender)
{
    ResetTimeout();
    Platform::BuyWeaponPack();
//    Hide();
}
//------------------------------------------------------------------------------
void BuyMenu::Menu_restore(Ref* sender)
{
    ResetTimeout();
    Platform::RestoreWeaponPack();
//    Hide();
}
//------------------------------------------------------------------------------
void BuyMenu::Menu_backmenu(Ref* sender)
{
    ResetTimeout();
    DestroySceneMenu::Get()->Menu_settings(NULL);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

