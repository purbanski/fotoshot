#include "BubbleCountMenu.h"
#include "Brain.h"
#include "Config.h"
#include "FxMenuItemImage.h"
#include "VisibleRect.h"
#include "Platform.h"
#include <sstream>
#include <ios>

static const int RESTART_IMAGE_TIMEOUT = 3;

static const int ACTION_FADE_IN = 100;
static const int ACTION_FADE_OUT = 101;
//------------------------------------------------------------------------------
void FadeNode(cocos2d::Node* node, int actionTag, int opacity);
//------------------------------------------------------------------------------
BubbleCountMenu* BubbleCountMenu::Create()
{
    BubbleCountMenu* node;
    node = new BubbleCountMenu();
    if ( node )
    {
        node->autorelease();
        node->Init();
    }
    return node;
}
//------------------------------------------------------------------------------
BubbleCountMenu::BubbleCountMenu()
{
    
}
//------------------------------------------------------------------------------
BubbleCountMenu::~BubbleCountMenu()
{
    unscheduleUpdate();
}
//------------------------------------------------------------------------------
void BubbleCountMenu::Init() {
//    SideMenu::Init("images/menus/tools/settings/bubbleCount.png",
//                   "images/menus/tools/settings/bubbleCountOn.png" );

    SideMenu::Init("images/menus/tools/settings.png",
                   "images/menus/tools/settingsOn.png");
    
    _itemPlus = FxMenuItemImage::create("images/menus/tools/settings/bubbleCount/plus.png",
                                        "images/menus/tools/settings/bubbleCount/plusOn.png",
                                        "images/menus/tools/settings/bubbleCount/plusOff.png",
                                        CC_CALLBACK_1(BubbleCountMenu::Menu_plus, this));
    
    _itemMinus = FxMenuItemImage::create("images/menus/tools/settings/bubbleCount/minus.png",
                                         "images/menus/tools/settings/bubbleCount/minusOn.png",
                                         "images/menus/tools/settings/bubbleCount/minusOff.png",
                                         CC_CALLBACK_1(BubbleCountMenu::Menu_minus, this));
    
    if (Brain::Get()->IsWeaponPackEnabled())
        _menu = cocos2d::Menu::create(_itemPlus,
                               _itemMinus,
                               NULL);
    else
        _menu = cocos2d::Menu::create(_itemPlus,
                               _itemMinus,
                               GetLeftBackMenuItem(),
                               NULL);
    
    _menu->alignItemsVerticallyWithPadding(Config::MenuPadding);
    addChild(_menu);
    
    
    float fontSize = 33.0f;
    //---------------
    // label ball count
    _labelBallCount = cocos2d::LabelTTF::create("", Config::FontBallCountRestartTimeout,
                                                fontSize, cocos2d::Size( 250,40),
                                                cocos2d::TextHAlignment::LEFT,
                                                cocos2d::TextVAlignment::CENTER );
    addChild(_labelBallCount);
    

    //---------------
    // label restart
    _labelRestart = cocos2d::LabelTTF::create("", Config::FontBallCountRestartTimeout,
                                              fontSize, cocos2d::Size(250,40),
                                              cocos2d::TextHAlignment::LEFT,
                                              cocos2d::TextVAlignment::CENTER );
    addChild(_labelRestart);
    
    //------------------
    //
    _currentBallCount = Brain::Get()->GetBallCount();
    _countDownStarted = false;
    _countDown = 0.0f;
    
    scheduleUpdate();
    
    _scheduler->schedule(CC_SCHEDULE_SELECTOR(BubbleCountMenu::ResetLevelStep),
                         this, 0.0f , CC_REPEAT_FOREVER, 0.0f, false);
    
    SetMenuPositionLeft();
    
    _labelBallCount->setPosition(_menu->getPosition().x+190, 165);
    _labelRestart->setPosition(_menu->getPosition().x+190, _labelBallCount->getPosition().y+35);
    
    _labelRestart->setOpacity(0);
    _labelBallCount->setOpacity(0);
    
    HideNoAnim();
}
//------------------------------------------------------------------------------
void BubbleCountMenu::Menu_plus(Ref* sender)
{
    Brain::Get()->IncBallCount();
    _countDownStarted = true;
    _countDown = ::RESTART_IMAGE_TIMEOUT;
    UpdateBallCountLabel();
    UpdateRestartCountLabel();
    ResetTimeout();
    
    if (Brain::Get()->GetBallCount() >= 2500)
        _itemPlus->setEnabled(false);
    
    if (!_itemMinus->isEnabled())
        _itemMinus->setEnabled(true);
}
//------------------------------------------------------------------------------
void BubbleCountMenu::Menu_minus(Ref* sender)
{
    Brain::Get()->DecBallCount();
    _countDownStarted = true;
    _countDown = ::RESTART_IMAGE_TIMEOUT;
    UpdateBallCountLabel();
    UpdateRestartCountLabel();
    ResetTimeout();
    
    if (Brain::Get()->GetBallCount() <= 100)
        _itemMinus->setEnabled(false);
    
    if (!_itemPlus->isEnabled())
        _itemPlus->setEnabled(true);
}
//------------------------------------------------------------------------------
void BubbleCountMenu::UpdateBallCountLabel()
{
    std::stringstream ss;
    ss << "Ball count: " << (int)round(Brain::Get()->GetBallCount());
    _labelBallCount->setString(ss.str());
}
//------------------------------------------------------------------------------
void BubbleCountMenu::UpdateRestartCountLabel()
{
    std::stringstream ss;
    //    ios_base::precision(4);
    ss.precision(1);
    ss << "Restart in: " << _countDown << " sec";
    _labelRestart->setString(ss.str().c_str());
    
    if (_countDown)
        FadeNode(_labelRestart, ACTION_FADE_IN, 255);
}
//------------------------------------------------------------------------------
void BubbleCountMenu::ResetLevelStep(float delta)
{
    if (!_countDownStarted)
        return;

    if (!_countDown)
    {
        Platform::AppStartUp();
        _countDownStarted = false;
        return;
    }
    
    _countDown -= delta;
    if ( _countDown <= 1 )
    {
        _countDown = 0;
        _labelRestart->runAction(cocos2d::FadeTo::create(0.25f, 0));
        Brain::Get()->ShowProcessing();
    }
    UpdateRestartCountLabel();
}
//------------------------------------------------------------------------------
void BubbleCountMenu::Show()
{
    SideMenu::Show();
    UpdateBallCountLabel();
    FadeInLabels();
    
    //    _countDownStarted = true;
    _scheduler->schedule(CC_SCHEDULE_SELECTOR(BubbleCountMenu::ResetLevelStep),
                         this, 0.0f , CC_REPEAT_FOREVER, 0.0f, false);
    
}
//------------------------------------------------------------------------------
void BubbleCountMenu::Hide()
{
    SideMenu::Hide();
    FadeOutLabels();
    
    _countDownStarted = false;
    _scheduler->unschedule(CC_SCHEDULE_SELECTOR(BubbleCountMenu::ResetLevelStep), this);
}
//------------------------------------------------------------------------------
void BubbleCountMenu::FadeInLabels()
{
    FadeNode(_labelBallCount, ACTION_FADE_IN, 255);
    if (_countDown)
        FadeNode(_labelRestart, ACTION_FADE_IN, 255);
}
//------------------------------------------------------------------------------
void BubbleCountMenu::FadeOutLabels()
{
    FadeNode(_labelBallCount, ACTION_FADE_OUT, 0);
    FadeNode(_labelRestart, ACTION_FADE_OUT, 0);
}
//------------------------------------------------------------------------------
void BubbleCountMenu::Menu_backmenu(Ref* sender)
{
    DestroySceneMenu::Get()->Menu_settings(NULL);
}
//------------------------------------------------------------------------------
void FadeNode(cocos2d::Node* node, int actionTag, int opacity) {
    cocos2d::Action* action;
    
    if( node->getOpacity() != opacity &&
       ! node->getActionByTag(actionTag))
    {
        action = cocos2d::FadeTo::create(0.25f, opacity);
        action->setTag(actionTag);
        
        node->stopAllActions();
        node->runAction(action);
    }
}
//------------------------------------------------------------------------------




