#ifndef __FotoShot__FxMenuItemImage__
#define __FotoShot__FxMenuItemImage__
//---------------------------------------------------------------
#include "cocos2d.h"
//---------------------------------------------------------------
class FxMenuItemImage : public cocos2d::MenuItemImage {
public:
    static FxMenuItemImage* create(const std::string& normalImage,
                                   const std::string& selectedImage,
                                   const cocos2d::ccMenuCallback& callback);

    static FxMenuItemImage* create(const std::string& normalImage,
                                   const std::string& selectedImage,
                                   const std::string& disabledImage,
                                   const cocos2d::ccMenuCallback& callback);
    
    
    virtual ~FxMenuItemImage();
    virtual void activate() override;

};
//---------------------------------------------------------------
class FxMenuItemToggle : public cocos2d::MenuItemToggle
{
public:
    static FxMenuItemToggle* createWithCallback(const cocos2d::ccMenuCallback& callback,
                                                cocos2d::MenuItem* item, ...) CC_REQUIRES_NULL_TERMINATION;
    
    virtual ~FxMenuItemToggle();
    virtual void activate() override;
};
//---------------------------------------------------------------

#endif
