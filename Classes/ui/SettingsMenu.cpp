#include "SettingsMenu.h"
#include "Config.h"
#include "World.h"
#include "Blocks.h"
#include "Brain.h"
#include "FxMenuItemImage.h"
#include "VisibleRect.h"
//------------------------------------------------------------------------------
SettingsMenu* SettingsMenu::Create()
{
    SettingsMenu* node;
    node = new SettingsMenu();
    if ( node )
    {
        node->autorelease();
        node->Init();
    }
    return node;
}
//------------------------------------------------------------------------------
SettingsMenu::SettingsMenu()
{
}
//------------------------------------------------------------------------------
SettingsMenu::~SettingsMenu()
{
}
//------------------------------------------------------------------------------
void SettingsMenu::Init() {
    SideMenu::Init("images/menus/tools/settings.png",
                   "images/menus/tools/settingsOn.png");
    
    
    auto itemCount = FxMenuItemImage::create("images/menus/tools/settings/bubbleCount.png",
                                             "images/menus/tools/settings/bubbleCountOn.png",
                                             CC_CALLBACK_1(SettingsMenu::Menu_bubbleCount, this));
    
    
    if (!Brain::Get()->IsWeaponPackEnabled())
    {
        auto itemBuy = FxMenuItemImage::create("images/menus/tools/settings/buy.png",
                                               "images/menus/tools/settings/buyOn.png",
                                               "images/menus/tools/settings/buyOff.png",
                                               CC_CALLBACK_1(SettingsMenu::Menu_buy, this));
        _menu = cocos2d::Menu::create(itemCount,
                             itemBuy,
                             NULL);
    }
    else
    {
        _menu = cocos2d::Menu::create(itemCount, NULL);
    }
    
    _menu->alignItemsVerticallyWithPadding(Config::MenuPadding);
    addChild(_menu);
    
    SetMenuPositionLeft();
    HideNoAnim();
}
////------------------------------------------------------------------------------
//void SettingsMenu::InitWithExtensionPack()
//{
//    auto itemCount = FxMenuItemImage::create("images/menus/tools/settings/bubbleCount.png",
//                                             "images/menus/tools/settings/bubbleCountOn.png",
//                                             CC_CALLBACK_1(SettingsMenu::Menu_bubbleCount, this));
//    
//    
//    if (!Brain::Get()->IsWeaponPackEnabled())
//    {
//        auto itemBuy = FxMenuItemImage::create("images/menus/tools/settings/buy.png",
//                                               "images/menus/tools/settings/buyOn.png",
//                                               "images/menus/tools/settings/buyOff.png",
//                                               CC_CALLBACK_1(SettingsMenu::Menu_buy, this));
//        _menu = Menu::create(itemCount,
//                             itemBuy,
//                             NULL);
//    }
//    else
//    {
//        _menu = Menu::create(itemCount, NULL);
//    }
//}
////------------------------------------------------------------------------------
//void SettingsMenu::InitWithoutExtensionPack()
//{
//    auto itemCount = FxMenuItemImage::create("images/menus/tools/settings/bubbleCount.png",
//                                             "images/menus/tools/settings/bubbleCountOn.png",
//                                             CC_CALLBACK_1(SettingsMenu::Menu_bubbleCount, this));
//    
//    
//    if (!Brain::Get()->IsWeaponPackEnabled())
//    {
//        auto itemBuy = FxMenuItemImage::create("images/menus/tools/settings/buy.png",
//                                               "images/menus/tools/settings/buyOn.png",
//                                               "images/menus/tools/settings/buyOff.png",
//                                               CC_CALLBACK_1(SettingsMenu::Menu_buy, this));
//        _menu = Menu::create(itemCount,
//                             itemBuy,
//                             NULL);
//    }
//    else
//    {
//        _menu = Menu::create(itemCount, NULL);
//    }
//}
//------------------------------------------------------------------------------
void SettingsMenu::Menu_bubbleCount(Ref* sender)
{
    ResetTimeout();
    DestroySceneMenu::Get()->Menu_bubbleCount();
}
//------------------------------------------------------------------------------
void SettingsMenu::Menu_buy(Ref* sender)
{
    ResetTimeout();
    DestroySceneMenu::Get()->Menu_buy();
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
