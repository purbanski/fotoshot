#include "FxMenuItemImage.h"
#include "SoundEngine.h"
//---------------------------------------------------------------
// Fx MenuItem Image
//---------------------------------------------------------------
FxMenuItemImage* FxMenuItemImage::create(const std::string& normalImage,
                                         const std::string& selectedImage,
                                         const std::string& disabledImage,
                                         const cocos2d::ccMenuCallback& callback)
{
    FxMenuItemImage *ret = new (std::nothrow) FxMenuItemImage();
    if (ret && ret->initWithNormalImage(normalImage, selectedImage, disabledImage, callback))
    {
        ret->autorelease();
        return ret;
    }
    CC_SAFE_DELETE(ret);
    return nullptr;
}
//---------------------------------------------------------------
FxMenuItemImage* FxMenuItemImage::create(const std::string& normalImage,
                                         const std::string& selectedImage,
                                         const cocos2d::ccMenuCallback& callback)
{
    return FxMenuItemImage::create(normalImage, selectedImage, "", callback);
}
//---------------------------------------------------------------
FxMenuItemImage::~FxMenuItemImage()
{
}
//---------------------------------------------------------------
void FxMenuItemImage::activate() {
    SoundEngine::Get()->PlayEffect(SoundEngine::eEffect_ButtonTap);
    MenuItemImage::activate();
}



//---------------------------------------------------------------
// Fx MenuItem Toggle
//---------------------------------------------------------------
FxMenuItemToggle* FxMenuItemToggle::createWithCallback(const cocos2d::ccMenuCallback& callback,
                                                     cocos2d::MenuItem* item, ...) {
    va_list args;
    va_start(args, item);
    FxMenuItemToggle *ret = new (std::nothrow) FxMenuItemToggle();
    ret->initWithCallback(callback, item, args);
    ret->autorelease();
    va_end(args);
    return ret;
}
//---------------------------------------------------------------
FxMenuItemToggle::~FxMenuItemToggle()
{
}
//---------------------------------------------------------------
void FxMenuItemToggle::activate() {
    SoundEngine::Get()->PlayEffect(SoundEngine::eEffect_ButtonTap);
    MenuItemToggle::activate();
}
//---------------------------------------------------------------


//---------------------------------------------------------------


//---------------------------------------------------------------
