#ifndef __FotoShot__SliderMenu__
#define __FotoShot__SliderMenu__
//------------------------------------------------------------------------------
#include <cocos2d.h>
#include "BaseMenu.h"
//------------------------------------------------------------------------------
class SliderMenu : public SideMenu
{
public:
    static SliderMenu* Create();
    ~SliderMenu();
    
private:
    SliderMenu();
    void Init();
    
    void Menu_shotS(Ref* sender);
    void Menu_shotM(Ref* sender);
    void Menu_shotL(Ref* sender);

    void Menu_shotReversM(Ref* sender);
    void Menu_shotReversL(Ref* sender);
    void Menu_sliderLine(Ref* sender);
    void Menu_sliderDotLine(Ref* sender);
};
//------------------------------------------------------------------------------
#endif 
