#include "SliderMenu.h"
#include "Slider.h"
#include "Config.h"
#include "World.h"
#include "Blocks.h"
#include "Brain.h"
#include "FxMenuItemImage.h"
#include "VisibleRect.h"
//------------------------------------------------------------------------------
SliderMenu* SliderMenu::Create()
{
    SliderMenu* node;
    node = new SliderMenu();
    if ( node )
    {
        node->autorelease();
        node->Init();
    }
    return node;
}
//------------------------------------------------------------------------------
SliderMenu::SliderMenu()
{
}
//------------------------------------------------------------------------------
SliderMenu::~SliderMenu()
{
}
//------------------------------------------------------------------------------
void SliderMenu::Init() {
    SideMenu::Init();
    auto sliderS = FxMenuItemImage::create("images/menus/tools/effects/slider/sliderS.png",
                                           "images/menus/tools/effects/slider/sliderSOn.png",
                                           "images/menus/tools/effects/slider/sliderSOff.png",
                                           CC_CALLBACK_1(SliderMenu::Menu_shotS, this));
    
    auto sliderM = FxMenuItemImage::create("images/menus/tools/effects/slider/sliderM.png",
                                           "images/menus/tools/effects/slider/sliderMOn.png",
                                           "images/menus/tools/effects/slider/sliderMOff.png",
                                           CC_CALLBACK_1(SliderMenu::Menu_shotM, this));
    
    auto sliderL = FxMenuItemImage::create("images/menus/tools/effects/slider/sliderL.png",
                                           "images/menus/tools/effects/slider/sliderLOn.png",
                                           "images/menus/tools/effects/slider/sliderLOff.png",
                                           CC_CALLBACK_1(SliderMenu::Menu_shotL, this));
    
    auto sliderRevM = FxMenuItemImage::create("images/menus/tools/effects/slider/sliderReversM.png",
                                              "images/menus/tools/effects/slider/sliderReversMOn.png",
                                              "images/menus/tools/effects/slider/sliderReversMOff.png",
                                              CC_CALLBACK_1(SliderMenu::Menu_shotReversM, this));
    
    
    auto sliderRevL = FxMenuItemImage::create("images/menus/tools/effects/slider/sliderReversL.png",
                                              "images/menus/tools/effects/slider/sliderReversLOn.png",
                                              "images/menus/tools/effects/slider/sliderReversLOff.png",
                                              CC_CALLBACK_1(SliderMenu::Menu_shotReversL, this));
    
    auto sliderLine = FxMenuItemImage::create("images/menus/tools/effects/slider/sliderLine.png",
                                              "images/menus/tools/effects/slider/sliderLineOn.png",
                                              "images/menus/tools/effects/slider/sliderLineOff.png",
                                              CC_CALLBACK_1(SliderMenu::Menu_sliderLine, this));
    

    auto sliderDotLine = FxMenuItemImage::create("images/menus/tools/effects/slider/sliderDotLines.png",
                                              "images/menus/tools/effects/slider/sliderDotLinesOn.png",
                                              "images/menus/tools/effects/slider/sliderDotLinesOff.png",
                                              CC_CALLBACK_1(SliderMenu::Menu_sliderDotLine, this));

    if (!Brain::Get()->IsWeaponPackEnabled())
    {
        sliderS->setEnabled(false);
        sliderM->setEnabled(false);
        sliderL->setEnabled(false);
        sliderRevM->setEnabled(false);
        sliderRevL->setEnabled(false);
        sliderLine->setEnabled(false);
        sliderDotLine->setEnabled(false);
    }

    _menu= cocos2d::Menu::create(
                        sliderLine,
                        sliderDotLine,
                        sliderRevM,
                        sliderRevL,
                        sliderS,
                        sliderM,
                        sliderL,
                        GetRigthBackMenuItem(), NULL);
    
    _menu->alignItemsVerticallyWithPadding(Config::MenuPadding);
    addChild(_menu);
    SetMenuPositionRight();
    HideNoAnim();
}
//------------------------------------------------------------------------------
void SliderMenu::Menu_sliderLine(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Slider::eSliderLine);
}
//------------------------------------------------------------------------------
void SliderMenu::Menu_sliderDotLine(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Slider::eSliderDotLines);
}
//------------------------------------------------------------------------------
void SliderMenu::Menu_shotS(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Slider::eSliderS);
}
//------------------------------------------------------------------------------
void SliderMenu::Menu_shotM(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Slider::eSliderM);
}
//------------------------------------------------------------------------------
void SliderMenu::Menu_shotL(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Slider::eSliderL);
}
//------------------------------------------------------------------------------
void SliderMenu::Menu_shotReversM(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Slider::eSliderReversM);
}
//------------------------------------------------------------------------------
void SliderMenu::Menu_shotReversL(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Slider::eSliderReversL);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

