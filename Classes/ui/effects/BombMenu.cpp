#include "BombMenu.h"
#include "Bomber.h"
#include "Config.h"
#include "World.h"
#include "Blocks.h"
#include "Brain.h"
#include "FxMenuItemImage.h"
#include "VisibleRect.h"
//------------------------------------------------------------------------------
BombMenu* BombMenu::Create()
{
    BombMenu* node;
    node = new BombMenu();
    if ( node )
    {
        node->autorelease();
        node->Init();
    }
    return node;
}
//------------------------------------------------------------------------------
BombMenu::BombMenu()
{
}
//------------------------------------------------------------------------------
BombMenu::~BombMenu()
{
}
//------------------------------------------------------------------------------
void BombMenu::Init() {
    SideMenu::Init();

    auto diagonal = FxMenuItemImage::create("images/menus/tools/effects/bomb/bombDiagonal.png",
                                            "images/menus/tools/effects/bomb/bombDiagonalOn.png",
                                            "images/menus/tools/effects/bomb/bombDiagonalOff.png",
                                            CC_CALLBACK_1(BombMenu::Menu_bombDiagonal, this));

    auto ball = FxMenuItemImage::create("images/menus/tools/effects/bomb/bombBall.png",
                                         "images/menus/tools/effects/bomb/bombBallOn.png",
                                         "images/menus/tools/effects/bomb/bombBallOff.png",
                                         CC_CALLBACK_1(BombMenu::Menu_bombBall, this));
    
    auto spiral = FxMenuItemImage::create("images/menus/tools/effects/bomb/bombSpiral.png",
                                          "images/menus/tools/effects/bomb/bombSpiralOn.png",
                                          "images/menus/tools/effects/bomb/bombSpiralOff.png",
                                          CC_CALLBACK_1(BombMenu::Menu_bombSpiral, this));
    
    auto medusa = FxMenuItemImage::create("images/menus/tools/effects/bomb/bombMedusa.png",
                                          "images/menus/tools/effects/bomb/bombMedusaOn.png",
                                          "images/menus/tools/effects/bomb/bombMedusaOff.png",
                                          CC_CALLBACK_1(BombMenu::Menu_bombMedusa, this));
    
    auto chaos = FxMenuItemImage::create("images/menus/tools/effects/bomb/bombChaos.png",
                                         "images/menus/tools/effects/bomb/bombChaosOn.png",
                                         "images/menus/tools/effects/bomb/bombChaosOff.png",
                                         CC_CALLBACK_1(BombMenu::Menu_bombChaos, this));

    auto side = FxMenuItemImage::create("images/menus/tools/effects/bomb/bombSide.png",
                                        "images/menus/tools/effects/bomb/bombSideOn.png",
                                        "images/menus/tools/effects/bomb/bombSideOff.png",
                                        CC_CALLBACK_1(BombMenu::Menu_bombSide, this));

    auto squeze = FxMenuItemImage::create("images/menus/tools/effects/bomb/bombSqueze.png",
                                          "images/menus/tools/effects/bomb/bombSquezeOn.png",
                                          "images/menus/tools/effects/bomb/bombSquezeOff.png",
                                          CC_CALLBACK_1(BombMenu::Menu_bombSqueze, this));

    if (!Brain::Get()->IsWeaponPackEnabled())
    {
//        diagonal->setEnabled(false);
//        ball->setEnabled(false);
        spiral->setEnabled(false);
        medusa->setEnabled(false);
        chaos->setEnabled(false);
        side->setEnabled(false);
        squeze->setEnabled(false);
    }
    
    _menu = cocos2d::Menu::create(diagonal,
                         ball,
                         spiral,
                         medusa,
                         chaos,
                         side,
                         squeze,
                         GetRigthBackMenuItem(), NULL);
    
    _menu->alignItemsVerticallyWithPadding(Config::MenuPadding);
    addChild(_menu);
    
    SetMenuPositionRight();
    HideNoAnim();
}
//------------------------------------------------------------------------------
void BombMenu::Menu_bombDiagonal(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Bomber::eBombDiagonal);
}
//------------------------------------------------------------------------------
void BombMenu::Menu_bombBall(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Bomber::eBombBall);
}
//------------------------------------------------------------------------------
void BombMenu::Menu_bombSpiral(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Bomber::eBombSpiral);
}
//------------------------------------------------------------------------------
void BombMenu::Menu_bombMedusa(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Bomber::eBombMedusa);
}
//------------------------------------------------------------------------------
void BombMenu::Menu_bombChaos(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Bomber::eBombChaos);
}
//------------------------------------------------------------------------------
void BombMenu::Menu_bombSqueze(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Bomber::eBombSqueze);
}
//------------------------------------------------------------------------------
void BombMenu::Menu_bombSide(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Bomber::eBombSide);
}
//------------------------------------------------------------------------------

