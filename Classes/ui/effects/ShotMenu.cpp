#include "ShotMenu.h"
#include "Config.h"
#include "World.h"
#include "Blocks.h"
#include "Brain.h"
#include "FxMenuItemImage.h"
#include "VisibleRect.h"
//------------------------------------------------------------------------------
ShotMenu* ShotMenu::Create()
{
    ShotMenu* node;
    node = new ShotMenu();
    if ( node )
    {
        node->autorelease();
        node->Init();
    }
    return node;
}
//------------------------------------------------------------------------------
ShotMenu::ShotMenu()
{
}
//------------------------------------------------------------------------------
ShotMenu::~ShotMenu()
{
}
//------------------------------------------------------------------------------
void ShotMenu::Init() {
    SideMenu::Init();
    auto itemShotXXS = FxMenuItemImage::create("images/menus/tools/effects/shot/shotXXS.png",
                                              "images/menus/tools/effects/shot/shotXXSOn.png",
                                               "images/menus/tools/effects/shot/shotXXSOff.png",
                                              CC_CALLBACK_1(ShotMenu::Menu_shotXXS, this));

    auto itemShotXS = FxMenuItemImage::create("images/menus/tools/effects/shot/shotXS.png",
                                          "images/menus/tools/effects/shot/shotXSOn.png",
                                              "images/menus/tools/effects/shot/shotXSOff.png",

                                              CC_CALLBACK_1(ShotMenu::Menu_shotXS, this));
    
    auto itemShotS = FxMenuItemImage::create("images/menus/tools/effects/shot/shotS.png",
                                          "images/menus/tools/effects/shot/shotSOn.png",                                          "images/menus/tools/effects/shot/shotSOff.png",
                                          CC_CALLBACK_1(ShotMenu::Menu_shotS, this));
    
    auto itemShotM = FxMenuItemImage::create("images/menus/tools/effects/shot/shotM.png",
                                            "images/menus/tools/effects/shot/shotMOn.png",
                                            "images/menus/tools/effects/shot/shotMOff.png",                                            CC_CALLBACK_1(ShotMenu::Menu_shotM, this));
    
    
    auto itemShotL = FxMenuItemImage::create("images/menus/tools/effects/shot/shotL.png",
                                           "images/menus/tools/effects/shot/shotLOn.png",                                           "images/menus/tools/effects/shot/shotLOff.png",
                                           CC_CALLBACK_1(ShotMenu::Menu_shotL, this));
    
    auto itemShotXL = FxMenuItemImage::create("images/menus/tools/effects/shot/shotXL.png",
                                             "images/menus/tools/effects/shot/shotXLOn.png",
                                              "images/menus/tools/effects/shot/shotXLOff.png",
                                              
                                             CC_CALLBACK_1(ShotMenu::Menu_shotXL, this));
    
    
    
    if (!Brain::Get()->IsWeaponPackEnabled())
    {
//        itemShotXXS->setEnabled(false);
//        itemShotXS->setEnabled(false);
//        itemShotS->setEnabled(false);
//        itemShotM->setEnabled(false);
//        itemShotL->setEnabled(false);
//        itemShotXL->setEnabled(false);
    }
    
    _menu= cocos2d::Menu::create(itemShotXXS,
                        itemShotXS,
                        itemShotS,
                        itemShotM,
                        itemShotL,
                        itemShotXL,
                        GetRigthBackMenuItem(), NULL);
    
    _menu->alignItemsVerticallyWithPadding(Config::MenuPadding);
    addChild(_menu);
    
   

    SetMenuPositionRight();
    HideNoAnim();
}
//------------------------------------------------------------------------------
void ShotMenu::Menu_shotXXS(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Shooter::eSizeXXS);
}
//------------------------------------------------------------------------------
void ShotMenu::Menu_shotXS(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Shooter::eSizeXS);
}
//------------------------------------------------------------------------------
void ShotMenu::Menu_shotS(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Shooter::eSizeS);
}
//------------------------------------------------------------------------------
void ShotMenu::Menu_shotM(Ref* sender)
{
    ResetTimeout();
   Brain::Get()->GetWeapon()->SetType(Shooter::eSizeM);
}
//------------------------------------------------------------------------------
void ShotMenu::Menu_shotL(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Shooter::eSizeL);
}
//------------------------------------------------------------------------------
void ShotMenu::Menu_shotXL(Ref* sender)
{
    ResetTimeout();
    Brain::Get()->GetWeapon()->SetType(Shooter::eSizeXL);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

