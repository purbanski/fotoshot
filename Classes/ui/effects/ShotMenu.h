#ifndef __FotoShot__ShotMenu__
#define __FotoShot__ShotMenu__
//------------------------------------------------------------------------------
#include <cocos2d.h>
#include "BaseMenu.h"
//------------------------------------------------------------------------------
class ShotMenu : public SideMenu
{
public:
    static ShotMenu* Create();
    ~ShotMenu();

private:
    ShotMenu();
    void Init();
    
    void Menu_shotXXS(Ref* sender);
    void Menu_shotXS(Ref* sender);
    void Menu_shotS(Ref* sender);
    void Menu_shotM(Ref* sender);
    void Menu_shotL(Ref* sender);
    void Menu_shotXL(Ref* sender);
};
//------------------------------------------------------------------------------
#endif
