#ifndef __FotoShot__BombMenu__
#define __FotoShot__BombMenu__
//------------------------------------------------------------------------------
#include <cocos2d.h>
#include "BaseMenu.h"
//------------------------------------------------------------------------------
class BombMenu : public SideMenu
{
public:
    static BombMenu* Create();
    ~BombMenu();
    
private:
    BombMenu();
    void Init();
    
    void Menu_bombDiagonal(Ref* sender);
    void Menu_bombBall(Ref* sender);
    void Menu_bombSpiral(Ref* sender);
    void Menu_bombMedusa(Ref* sender);
    void Menu_bombChaos(Ref* sender);
    void Menu_bombSide(Ref* sender);
    void Menu_bombSqueze(Ref* sender);
};
//------------------------------------------------------------------------------
#endif

