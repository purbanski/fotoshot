#include "ProcessingLayer.h"
#include "VisibleRect.h"
#include "Config.h"
//------------------------------------------------------------
ProcessingLayer* ProcessingLayer::create()
{
    ProcessingLayer* node;
    node = new ProcessingLayer();
    if (node)
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//------------------------------------------------------------
ProcessingLayer::ProcessingLayer()
{
    
}
//------------------------------------------------------------
ProcessingLayer::~ProcessingLayer()
{
    
}
//------------------------------------------------------------
void ProcessingLayer::Init()
{
    float gBottomSpace = 113 + 20;
    
    _bg = cocos2d::Sprite::create("images/bg.png");
    auto size = cocos2d::Director::getInstance()->getOpenGLView()->getFrameSize();
    float width = size.width;
    float height = size.height;
    
    _bg->setScale( width / _bg->getContentSize().width );
    _bg->setScale( height / _bg->getContentSize().height );
    
    _bg->setPosition(cocos2d::Point(VisibleRect::center().x,
                                    VisibleRect::center().y +gBottomSpace ));
    float c = 70;
    _bg->setColor(cocos2d::Color3B(c,c,c));

    addChild(_bg, 50);
    
    _label = cocos2d::Sprite::create("images/processing.png");
    _label->setPosition(cocos2d::Point(
                                       VisibleRect::center().x,
                                       VisibleRect::center().y + 60 ));
    addChild(_label, 100);
}
//------------------------------------------------------------
void ProcessingLayer::Show()
{
    _label->stopAllActions();
    _bg->stopAllActions();
    
    _label->setOpacity(255);
    _bg->setOpacity(220);
}
//------------------------------------------------------------
void ProcessingLayer::Hide()
{
    _label->stopAllActions();
    _bg->stopAllActions();
    
    _label->runAction(cocos2d::FadeTo::create(0.25f,0));
    _bg->runAction(cocos2d::FadeTo::create(0.25f,0));
}
//------------------------------------------------------------



