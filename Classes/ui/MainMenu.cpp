#include "MainMenu.h"
#include "VisibleRect.h"
#include "Config.h"

#include "ShareMenu.h"
#include "SettingsMenu.h"
#include "EffectsMenu.h"
#include "ShotMenu.h"
#include "SliderMenu.h"
#include "BombMenu.h"
#include "SoundEngine.h"
#include "FxMenuItemImage.h"
#include "BubbleCountMenu.h"
#include "BuyMenu.h"

#include "Brain.h"

//------------------------------------------------------------------------------
DestroySceneMenu* DestroySceneMenu::_sInstance = NULL;
//------------------------------------------------------------------------------
DestroySceneMenu* DestroySceneMenu::Get() {
    return _sInstance;
}
//------------------------------------------------------------------------------
DestroySceneMenu* DestroySceneMenu::Create()
{
    DestroySceneMenu* node;
    node = new DestroySceneMenu();
    if ( node )
    {
        node->autorelease();
        node->Init();
    }
    return node;
}
//------------------------------------------------------------------------------
DestroySceneMenu::DestroySceneMenu()
{
    _rightMenu = NULL;
    _leftMenu  = NULL;
    
    _sInstance = this;
}
//------------------------------------------------------------------------------
DestroySceneMenu::~DestroySceneMenu()
{
    _sInstance = NULL;
}
//------------------------------------------------------------------------------
void DestroySceneMenu::Init() {
    auto shareItem = FxMenuItemImage::create("images/menus/tools/share.png",
                                           "images/menus/tools/shareOn.png",
                                           CC_CALLBACK_1(DestroySceneMenu::Menu_share, this));

    auto settingsItem = FxMenuItemImage::create("images/menus/tools/settings.png",
                                              "images/menus/tools/settingsOn.png",
                                              CC_CALLBACK_1(DestroySceneMenu::Menu_settings, this));

    auto effectsItem = FxMenuItemImage::create("images/menus/tools/effects.png",
                                              "images/menus/tools/effectsOn.png",
                                              CC_CALLBACK_1(DestroySceneMenu::Menu_effects, this));
    

    auto restartItem = FxMenuItemImage::create("images/menus/restart.png",
                                             "images/menus/restartOn.png",
                                             CC_CALLBACK_1(DestroySceneMenu::Menu_restart, this));

    auto cameraItem = FxMenuItemImage::create("images/menus/camera.png",
                                             "images/menus/cameraOn.png",
                                             CC_CALLBACK_1(DestroySceneMenu::Menu_photo, this));
    

    auto menu = cocos2d::Menu::create(//effectsItem,
                               //shareItem,
//                               settingsItem,
                               cameraItem,
                               restartItem, NULL);
    
    menu->alignItemsHorizontallyWithPadding(Config::MenuPadding+5.0f);
    menu->setPosition(VisibleRect::center().x,
                      VisibleRect::bottom().y+settingsItem->getContentSize().height / 2.0f + 2.5f);
    addChild(menu);
    
    RestartMenus();
}
//------------------------------------------------------------------------------
void DestroySceneMenu::RestartMenus()
{
    MenuLeftSetup();
    MenuRightSetup();
    
    _rightMenu = EffectsMenu::Create();
    addChild(_rightMenu);
    
    if (Brain::Get()->IsWeaponPackEnabled())
        _leftMenu = BubbleCountMenu::Create();
    else
        _leftMenu = SettingsMenu::Create();
    addChild(_leftMenu);
    
}
//------------------------------------------------------------------------------
void DestroySceneMenu::Menu_test(cocos2d::Ref* sender )
{
    
}
//------------------------------------------------------------------------------
void DestroySceneMenu::Menu_share(cocos2d::Ref* sender)
{
//    MenuSetup();
//    _activeMenu = ShareMenu::Create();
//    MenuPostup();
}
//------------------------------------------------------------------------------
void DestroySceneMenu::Menu_settings(cocos2d::Ref* sender)
{
    MenuLeftSetup();
    _leftMenu = SettingsMenu::Create();
    MenuLeftPostup();
}
//------------------------------------------------------------------------------
void DestroySceneMenu::Menu_effects(cocos2d::Ref* sender)
{
    MenuRightSetup();
    _rightMenu = EffectsMenu::Create();
    MenuRightPostup();
}
//------------------------------------------------------------------------------
void DestroySceneMenu::Menu_buy()
{
    MenuLeftSetup();
    _leftMenu = BuyMenu::Create();
    MenuLeftPostup();
}
//------------------------------------------------------------------------------
void DestroySceneMenu::Menu_bubbleCount()
{
    MenuLeftSetup();
    _leftMenu = BubbleCountMenu::Create();
    MenuLeftPostup();
}
//------------------------------------------------------------------------------
void DestroySceneMenu::Menu_restart(Ref* sender)
{
    Brain::Get()->Restart();
}
//------------------------------------------------------------------------------
void DestroySceneMenu::Menu_photo(Ref* sender)
{
    Brain::Get()->TakePhoto();
}
//------------------------------------------------------------------------------
void DestroySceneMenu::MenuRightSetup() {
    if (_rightMenu && _rightMenu->getParent() )
    {
        _rightMenu->removeFromParentAndCleanup(true);
    }
}
//------------------------------------------------------------------------------
void DestroySceneMenu::MenuRightPostup() {
    addChild(_rightMenu);
    _rightMenu->Show();
}
//------------------------------------------------------------------------------
void DestroySceneMenu::MenuLeftSetup() {
    if (_leftMenu && _leftMenu->getParent() )
    {
        _leftMenu->removeFromParentAndCleanup(true);
    }
}
//------------------------------------------------------------------------------
void DestroySceneMenu::MenuLeftPostup() {
    addChild(_leftMenu);
    _leftMenu->Show();
}
//------------------------------------------------------------------------------
void DestroySceneMenu::Menu_slider() {
    MenuRightSetup();
    _rightMenu = SliderMenu::Create();
    MenuRightPostup();
}
//------------------------------------------------------------------------------
void DestroySceneMenu::Menu_shot() {
    MenuRightSetup();
    _rightMenu = ShotMenu::Create();
    MenuRightPostup();
}
//------------------------------------------------------------------------------
void DestroySceneMenu::Menu_bomb() {
    MenuRightSetup();
    _rightMenu = BombMenu::Create();
    MenuRightPostup();
}
//------------------------------------------------------------------------------
void DestroySceneMenu::Menu_weapon()
{
    Menu_effects(NULL);
//    _rightMenu->Show();
}
//------------------------------------------------------------------------------
void DestroySceneMenu::UpdateButtons()
{
    
}
//------------------------------------------------------------------------------
void DestroySceneMenu::LeftMenuDisable()
{
    if (_leftMenu && _leftMenu->getParent())
        _leftMenu->SetEnable(false);
}
//------------------------------------------------------------------------------
void DestroySceneMenu::LeftMenuEnable()
{
    if (_leftMenu && _leftMenu->getParent())
        _leftMenu->SetEnable(true);
}
