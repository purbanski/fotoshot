#include "Background.h"
#include "VisibleRect.h"
#include "Config.h"
//------------------------------------------------------------
Background* Background::create()
{
    Background* node;
    node = new Background();
    if (node)
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//------------------------------------------------------------
Background::Background()
{
    
}
//------------------------------------------------------------
Background::~Background()
{
    
}
//------------------------------------------------------------
void Background::Init()
{
    float gBottomSpace = 113 + 20;

    _sprite = cocos2d::Sprite::create("images/bg.png");
    auto size = cocos2d::Director::getInstance()->getOpenGLView()->getFrameSize();
    float width = size.width;
    float height = size.height;
    
    _sprite->setScale( width / _sprite->getContentSize().width );
    _sprite->setScale( height / _sprite->getContentSize().height );

    _sprite->setPosition(cocos2d::Point(VisibleRect::center().x,
                                        VisibleRect::center().y + gBottomSpace));
    _sprite->setColor(cocos2d::Color3B(30,30,30));
    addChild(_sprite);
}
//------------------------------------------------------------
void Background::SetBlack()
{
    _sprite->setColor(cocos2d::Color3B(0,0,0));
    
}
//------------------------------------------------------------
void Background::SetWhite()
{
    _sprite->setColor(cocos2d::Color3B(255,255,255));
}

//------------------------------------------------------------

//------------------------------------------------------------


