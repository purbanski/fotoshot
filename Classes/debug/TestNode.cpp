#ifdef BUILD_DEBUG
//----------------------------------------------------------------------
#include "TestNode.h"
#include "VisibleRect.h"
#include "toJavaCalls.h"
#include "Brain.h"
//----------------------------------------------------------------------
static cocos2d::LabelTTF* getLabel(const char* title)
{
    auto label = cocos2d::LabelTTF::create(title ,"ArialBold", 50.0f);
    label->setColor(cocos2d::Color3B(255,0,0));
    return label;
}
//----------------------------------------------------------------------
TestNode* TestNode::create()
{
    TestNode* node;
    node = new TestNode();
    if (node && node->init())
    {
        node->autorelease();
        return node;
    }
    CC_SAFE_DELETE(node);
    return node;
}
//----------------------------------------------------------------------
TestNode::TestNode()
{
}
//----------------------------------------------------------------------
TestNode::~TestNode()
{

}
//----------------------------------------------------------------------
bool TestNode::init()
{
    if (!Node::init())
    {
        return false;
    }
    
    auto buyWeaponPack = cocos2d::MenuItemLabel::create(getLabel("Buy Pack"),
                                               CC_CALLBACK_1(TestNode::Menu_buyWeaponPack, this));

    auto setBuyWeaponPack = cocos2d::MenuItemLabel::create(getLabel("Set Weapon Pack"),
                                                  CC_CALLBACK_1(TestNode::Menu_setBuyWeaponPack, this));
    
    auto setBuyTestCancel = cocos2d::MenuItemLabel::create(getLabel("Test Cancel"),
                                              CC_CALLBACK_1(TestNode::Menu_setBuyTestCancel, this));
    
    auto setBuyTestUnavailable = cocos2d::MenuItemLabel::create(getLabel("Test Unavailable"),
                                              CC_CALLBACK_1(TestNode::Menu_setBuyTestUnavailable, this));
    
    auto setBuyTestPurchased = cocos2d::MenuItemLabel::create(getLabel("Test Purchased"),
                                              CC_CALLBACK_1(TestNode::Menu_setBuyTestPurchased, this));
    
    auto setBuyTestRefunded = cocos2d::MenuItemLabel::create(getLabel("Test Refunded"),
                                              CC_CALLBACK_1(TestNode::Menu_setBuyTestRefunded, this));

    auto billLock = cocos2d::MenuItemLabel::create(getLabel("Bill lock"),
                                                    CC_CALLBACK_1(TestNode::Menu_billingLock, this));
    
    auto billUnlock = cocos2d::MenuItemLabel::create(getLabel("Bill unlock"),
                                                    CC_CALLBACK_1(TestNode::Menu_billingUnlock, this));

    _menu = cocos2d::Menu::create(buyWeaponPack,
                             setBuyWeaponPack,
                             setBuyTestCancel,
                             setBuyTestUnavailable,
                             setBuyTestPurchased,
                             setBuyTestRefunded,
                             billLock,
                             billUnlock,
                             NULL);
    _menu->alignItemsVerticallyWithPadding(10);
    _menu->setPosition(cocos2d::Vec2(VisibleRect::right().x-140, VisibleRect::center().y+0));
    addChild(_menu);
    
    _menu->setVisible(false);
    
    //----------
    auto debug = cocos2d::MenuItemLabel::create(getLabel("Debug toggle"),
                                            CC_CALLBACK_1(TestNode::Menu_debugToggle, this));
    
    auto menuDebug = cocos2d::Menu::create(debug,
                                  NULL);
//    menuDebug->alignItemsVerticallyWithPadding(10);
    menuDebug->setPosition(cocos2d::Vec2(VisibleRect::center().x, VisibleRect::top().y-60));
    addChild(menuDebug);

    return true;
}
//----------------------------------------------------------------------
void TestNode::Menu_buyWeaponPack(cocos2d::Ref* sender)
{
#ifdef BUILD_ANDROID
    billing_buyWeaponPackJNI();
#endif
}
//----------------------------------------------------------------------
void TestNode::Menu_setBuyWeaponPack(cocos2d::Ref* sender)
{
#ifdef BUILD_ANDROID
    billing_setBuyDefaultJNI();
#endif
    
}
//----------------------------------------------------------------------
void TestNode::Menu_setBuyTestCancel(cocos2d::Ref* sender)
{
#ifdef BUILD_ANDROID
    billing_setBuyTestCanceledJNI();
#endif
    
}
//----------------------------------------------------------------------
void TestNode::Menu_setBuyTestUnavailable(cocos2d::Ref* sender)
{
#ifdef BUILD_ANDROID
    billing_setBuyTestUnavailableJNI();
#endif
    
}
//----------------------------------------------------------------------
void TestNode::Menu_setBuyTestPurchased(cocos2d::Ref* sender)
{
#ifdef BUILD_ANDROID
    billing_setBuyTestPurchasedJNI();
#endif
    
}
//----------------------------------------------------------------------
void TestNode::Menu_setBuyTestRefunded(cocos2d::Ref* sender)
{
#ifdef BUILD_ANDROID
    billing_setBuyTestRefundedJNI();
#endif
}
//----------------------------------------------------------------------
void TestNode::Menu_billingLock(cocos2d::Ref* sender)
{
    Brain::Get()->WeaponPackDisable();
}
//----------------------------------------------------------------------
void TestNode::Menu_billingUnlock(cocos2d::Ref* sender)
{
    Brain::Get()->WeaponPackEnable();
}
//----------------------------------------------------------------------
void TestNode::Menu_debugToggle(cocos2d::Ref* sender)
{
    _menu->setVisible(!_menu->isVisible());
}
//----------------------------------------------------------------------

#endif