#ifdef BUILD_DEBUG
//----------------------------------------------------------------------
#ifndef __FotoShot__TestNode__
#define __FotoShot__TestNode__
//----------------------------------------------------------------------
#include "cocos2d.h"
//----------------------------------------------------------------------
class TestNode : public cocos2d::Node
{
public:
    static TestNode* create();

    ~TestNode();
    
    virtual bool init();

protected:
    TestNode();
    
    void Menu_buyWeaponPack(cocos2d::Ref* sender);
    void Menu_setBuyWeaponPack(cocos2d::Ref* sender);
    
    void Menu_setBuyTestCancel(cocos2d::Ref* sender);
    void Menu_setBuyTestUnavailable(cocos2d::Ref* sender);
    void Menu_setBuyTestPurchased(cocos2d::Ref* sender);
    void Menu_setBuyTestRefunded(cocos2d::Ref* sender);
    void Menu_billingLock(cocos2d::Ref* sender);
    void Menu_billingUnlock(cocos2d::Ref* sender);
    void Menu_debugToggle(cocos2d::Ref* sender);
    
private:
    cocos2d::Menu* _menu;
};
#endif

#endif